-- Create `o_pricing` table
DROP TABLE IF EXISTS `o_pricing`;
CREATE TABLE `o_pricing` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`duration` ENUM('month', 'term', 'annum') NOT NULL,
	`price` DECIMAL(10,2) NOT NULL,
	`payer` ENUM('I', 'S') NOT NULL,
	`max_students` INT(4) NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Insert into `o_pricing` table
INSERT INTO o_pricing (duration, price, payer, max_students) VALUES
  ('month', 1, 'I', NULL),
  ('term', 3.6, 'I', NULL),
  ('annum', 9.6, 'I', NULL),
  ('month', 0.9, 'S', 100),
  ('month', 0.8, 'S', 200),
  ('month', 0.7, 'S', 300),
  ('month', 0.6, 'S', 400),
  ('month', 0.5, 'S', NULL),
  ('term', 3.4, 'S', 100),
  ('term', 3.3, 'S', 200),
  ('term', 3.2, 'S', 300),
  ('term', 3.1, 'S', 400),
  ('term', 3, 'S', NULL),
  ('annum', 9.9, 'S', 100),
  ('annum', 9.8, 'S', 200),
  ('annum', 9.7, 'S', 300),
  ('annum', 9.6, 'S', 400),
  ('annum', 9.5, 'S', NULL);