-- Create `o_users` table
DROP TABLE IF EXISTS `o_users`;
CREATE TABLE `o_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(60) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `role` int(1) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `country_id` varchar(2) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `added_by` int(1) NOT NULL,
  `updated_by` int(1),
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1000;

-- Create `o_country` table
DROP TABLE IF EXISTS `o_country`;
CREATE TABLE `o_country` (
  `id` varchar(2) NOT NULL,
  `name` varchar(50) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `added_by` int(1) NOT NULL,
  `updated_by` int(1),
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Create `o_syllabi` table
DROP TABLE IF EXISTS `o_syllabi`;
CREATE TABLE `o_syllabi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `added_by` int(1) NOT NULL,
  `updated_by` int(1),
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1100;

-- Create `o_subject` table
DROP TABLE IF EXISTS `o_subject`;
CREATE TABLE `o_subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `added_by` int(1) NOT NULL,
  `updated_by` int(1),
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1200;

-- Create `o_topic` table
DROP TABLE IF EXISTS `o_topic`;
CREATE TABLE `o_topic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `is_paid` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '1',
  `added_by` int(1) NOT NULL,
  `updated_by` int(1),
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1300;

-- Create `o_question` table
DROP TABLE IF EXISTS `o_question`;
CREATE TABLE `o_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `explanation` text NOT NULL,
  `type` int(1) NOT NULL,
  `correct` int(1) NOT NULL,
  `is_paid` int(1) NOT NULL DEFAULT '0',
  `tags` text,
  `status` int(1) NOT NULL DEFAULT '1',
  `added_by` int(1) NOT NULL,
  `updated_by` int(1),
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1400;

-- Create `o_question_choices` table
DROP TABLE IF EXISTS `o_question_choices`;
CREATE TABLE `o_question_choices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `index` int(1) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `added_by` int(1) NOT NULL,
  `updated_by` int(1),
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `choice_index` (`question_id`,`index`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1500;

-- Create `o_school` table
DROP TABLE IF EXISTS `o_school`;
CREATE TABLE `o_school` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `num_students` int(4) NOT NULL,
  `account_type` int(1) NOT NULL,
  `reference` varchar(32) DEFAULT NULL,
  `verified` int(1) NOT NULL DEFAULT '0',
  `profile` text,
  `status` int(1) NOT NULL DEFAULT '1',
  `added_by` int(1) NOT NULL,
  `updated_by` int(1),
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1600;

-- Create `o_teacher` table
DROP TABLE IF EXISTS `o_teacher`;
CREATE TABLE `o_teacher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `profile` text,
  `status` int(1) NOT NULL DEFAULT '1',
  `added_by` int(1) NOT NULL,
  `updated_by` int(1),
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1700;

-- Create `o_class` table
DROP TABLE IF EXISTS `o_class`;
CREATE TABLE `o_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `school_id` int(11) NOT NULL,
  `syllabi_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `added_by` int(1) NOT NULL,
  `updated_by` int(1),
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1800;

-- Create `o_student` table
DROP TABLE IF EXISTS `o_student`;
CREATE TABLE `o_student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `syllabi_id` int(11),
  `class_id` int(11),
  `account_type` int(1) NOT NULL,
  `reference` varchar(32) DEFAULT NULL,
  `verified` int(1) NOT NULL DEFAULT '0',
  `profile` text,
  `status` int(1) NOT NULL DEFAULT '1',
  `added_by` int(1) NOT NULL,
  `updated_by` int(1),
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1900;

-- Create `o_pricing` table
DROP TABLE IF EXISTS `o_pricing`;
CREATE TABLE `o_pricing` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`duration` ENUM('month', 'term', 'annum') NOT NULL,
	`price` DECIMAL(10,2) NOT NULL,
	`payer` ENUM('I', 'S') NOT NULL,
	`max_students` INT(4) NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Create `m_syllabi_subject` table
DROP TABLE IF EXISTS `m_syllabi_subject`;
CREATE TABLE `m_syllabi_subject` (
  `syllabi_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `added_by` int(1) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `syllabi_subject` (`syllabi_id`,`subject_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Create `m_subject_topic` table
DROP TABLE IF EXISTS `m_subject_topic`;
CREATE TABLE `m_subject_topic` (
  `subject_id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `added_by` int(1) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `subject_topic` (`subject_id`,`topic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Create `m_topic_question` table
DROP TABLE IF EXISTS `m_topic_question`;
CREATE TABLE `m_topic_question` (
  `topic_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `added_by` int(1) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `topic_question` (`topic_id`,`question_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Create `m_school_syllabi` table
DROP TABLE IF EXISTS `m_school_syllabi`;
CREATE TABLE `m_school_syllabi` (
  `school_id` int(11) NOT NULL,
  `syllabi_id` int(11) NOT NULL,
  `added_by` int(1) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `school_syllabi` (`school_id`,`syllabi_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Create `o_test` table
DROP TABLE IF EXISTS `o_test`;
CREATE TABLE `o_test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `class_id` int(11),
  `details` text,
  `date_started` datetime,
  `date_ended` datetime,
  `added_by` int(1) NOT NULL,
  `updated_by` int(1),
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Create `o_test_questions` table
DROP TABLE IF EXISTS `o_test_questions`;
CREATE TABLE `o_test_questions` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`test_id` int(11) NOT NULL,
	`question_id` int(11) NOT NULL,
	`topic_id` int(11) NOT NULL,
  `is_correct` int(1) DEFAULT NULL,
	`details` text,
  `added_by` int(1) NOT NULL,
  `updated_by` int(1),
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;