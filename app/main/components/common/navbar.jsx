import React, {Component} from 'react';

class CmpNavBar extends Component {
  constructor(props) {
    super(props);
    this._handleToggleClick = this._handleToggleClick.bind(this);
    this.state = {
      toggle: false
    };
  }

  _handleToggleClick() {
    const toggle = this.state.toggle;
    this.setState({toggle: !toggle});
  }

  render() {
    const toggle = this.state.toggle;
    const isLoggedIn = __ejs_isLoggedIn; // eslint-disable-line
    return (
      <div className="wg_nav_wrapper">
        <nav className="navbar" role="navigation">
          <div className="container">
            <div className="navbar-header page-scroll">
              <button type="button" className="navbar-toggle collapsed bg-primary" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar" onClick={this._handleToggleClick}>
                <i className={`fa ${toggle ? 'fa-times' : 'fa-bars'} text-white`}></i>
              </button>
            </div>
            <a className="navbar-brand hidden-xs" href="/">
              <img src="/main/logo.gif" className="wg_navbar_logo" alt="Wagona Maths" />
            </a>
            <a className="wg_navbar_logo_mobile hidden-sm hidden-md hidden-lg" href="/">
              <img src="/main/logo.gif" className="wg_navbar_logo" alt="Wagona Maths" />
            </a>
            <div id="navbar" className="navbar-collapse collapse">
              <div className="row border-top-bottom m-t-sm hidden-sm hidden-md hidden-lg wg_navbar_collapse_mobile">
                <div className="col-xs-6">
                  <ul className="nav navbar-nav navbar">
                    <li><a href="/">Home</a></li>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">How it Works</a></li>
                    <li><a href="#">Schools</a></li>
                    <li><a href="#">Methodology</a></li>
                  </ul>
                </div>
                <div className="col-xs-6">
                  <ul className="nav navbar-nav navbar">
                    <li><a href="#">Features</a></li>
                    <li><a href="/pricing">Pricing</a></li>
                    <li><a href="#">Contact Us</a></li>
                    {!isLoggedIn && <li><a href="/login">Login</a></li>}
                    {!isLoggedIn && <li><a href="/register">Register</a></li>}
                    {isLoggedIn && <li><a href="/account">Account</a></li>}
                    {isLoggedIn && <li><a onClick={this.props._handleSignOutClick}>Logout</a></li>}
                  </ul>
                </div>
              </div>
              <ul className="nav navbar-nav navbar hidden-xs">
                <li><a href="/">Home</a></li>
                <li><a href="#">About Us</a></li>
                <li><a href="#">How it Works</a></li>
                <li><a href="#">Schools</a></li>
                <li><a href="#">Methodology</a></li>
              </ul>
              <ul className="nav navbar-nav navbar-right hidden-xs">
                <li><a href="#">Features</a></li>
                <li><a href="/pricing">Pricing</a></li>
                <li><a href="#">Contact Us</a></li>
                {!isLoggedIn && <li><a href="/login">Login</a></li>}
                {!isLoggedIn && <li><a href="/register">Register</a></li>}
                {isLoggedIn && <li><a href="/account">Account</a></li>}
                {isLoggedIn && <li><a onClick={this.props._handleSignOutClick}>Logout</a></li>}
              </ul>
            </div>
          </div>
        </nav>
      </div>
    );
  }
}

CmpNavBar.propTypes = {
  _handleSignOutClick: React.PropTypes.func
};

export default CmpNavBar;
