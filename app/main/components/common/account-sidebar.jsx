/* globals __ejs_page */
import React, {Component} from 'react';

class CmpAccountSideBar extends Component {
  render() {
    let page = __ejs_page; // eslint-disable-line
    return (
      <ul className="list-group wg_account_sidebar">
        <li className={`list-group-item ${page === 'account' ? 'active' : ''}`}><a href="/account">Account Dashboard</a></li>
        <li className={`list-group-item ${page === 'accountProfile' ? 'active' : ''}`}><a href="/account/profile">Profile</a></li>
      </ul>
    );
  }
}

export default CmpAccountSideBar;
