import React, {Component} from 'react';

class CmpLoader extends Component {
  render() {
    const loading = this.props.loading;
    return (
      <div className={`wg_loader ${loading ? '' : 'hide'}`}>
        <div className="sk-spinner sk-spinner-double-bounce">
          <div className="sk-double-bounce1"></div>
          <div className="sk-double-bounce2"></div>
        </div>
      </div>
    );
  }
}

CmpLoader.propTypes = {
  loading: React.PropTypes.bool.isRequired
};

export default CmpLoader;
