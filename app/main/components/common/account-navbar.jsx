/* globals __ejs_user, _ */
import React, {Component} from 'react';

class CmpAccountNavBar extends Component {
  render() {
    let accountTitle = __ejs_user.role === _.toUpper('individual') ? _.capitalize('student') : _.capitalize(__ejs_user.role); // eslint-disable-line
    let accountName = __ejs_user.first_name + ' ' + __ejs_user.last_name; // eslint-disable-line
    return (
      <div className="container">
        <nav className="navbar wg_account_navbar" role="navigation">
          <ul className="nav navbar-nav navbar">
            <li>
              <a href="/account" className="wg_account_title">{accountTitle} Dashboard</a>
            </li>
          </ul>
          <div className="btn-group navbar-right m-r-sm m-t-sm">
            <button className="btn btn btn-primary-color btn-normalize dropdown-toggle" data-toggle="dropdown">{accountName} <span className="caret"></span></button>
            <ul className="dropdown-menu">
                <li><a href="/account/profile"><i className="fa fa-user-circle"></i> Profile</a></li>
                <li className="divider"></li>
                <li><a onClick={this.props._handleSignOutClick}><i className="fa fa-sign-out"></i> Logout</a></li>
            </ul>
          </div>
        </nav>
      </div>
    );
  }
}

CmpAccountNavBar.propTypes = {
  _handleSignOutClick: React.PropTypes.func
};

export default CmpAccountNavBar;
