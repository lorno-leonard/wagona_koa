import React, {Component} from 'react';

class CmpSpinner extends Component {
  render() {
    return (
      <div className={this.props.hasMarginTop ? 'm-t-xl' : ''}>
        <div className="sk-spinner sk-spinner-cube-grid">
          <div className="sk-cube"></div>
          <div className="sk-cube"></div>
          <div className="sk-cube"></div>
          <div className="sk-cube"></div>
          <div className="sk-cube"></div>
          <div className="sk-cube"></div>
          <div className="sk-cube"></div>
          <div className="sk-cube"></div>
          <div className="sk-cube"></div>
        </div>
      </div>
    );
  }
}

CmpSpinner.propTypes = {
  hasMarginTop: React.PropTypes.bool
};

export default CmpSpinner;
