/* globals window, _, __ejs_page */
import React, {Component} from 'react';
import CmpNavBar from './../common/navbar';
import CmpAccountNavBar from './../common/account-navbar';
import CmpLoader from './../common/loader';
import ViewIndex from './../../views/index';
import ViewLogin from './../../views/login';
import ViewRegister from './../../views/register';
import ViewPricing from './../../views/pricing';
import ViewAccount from './../../views/account/account';
import ViewAccountProfile from './../../views/account/profile';
import toastr from 'toastr';
import request from './../../../_common/request';

class LayoutMain extends Component {
  constructor(props) {
    super(props);
    this._handleSignOutClick = this._handleSignOutClick.bind(this);
    this.childProps = {
      appThis: this,
      request: request,
      __toggleLoader: this.__toggleLoader,
      __toggleIboxLoader: this.__toggleIboxLoader,
      __toggleToastr: this.__toggleToastr,
      __ajaxErrorHandler__: this.__ajaxErrorHandler__
    };
    this.state = {
      loading: false
    };
  }

  _handleSignOutClick() {
    const self = this;

    // Show loader
    this.__toggleLoader(true);

    request.call('POST', '/api/logout', null, (err, res) => {
      if (err) {
        // Hide loader
        self.__toggleLoader(false);

        console.log(err);
        return;
      }

      // Reload
      window.location.reload();
    });
  }

  __toggleLoader(_bool) {
    const self = _.has(this, 'appThis') ? this.appThis : this;
    self.setState({loading: _bool});
    if (_bool) {
      $('body.landing-page').addClass('loading');
    } else {
      $('body.landing-page').removeClass('loading');
    }
  }

  __toggleIboxLoader(_bool) {
    if (_bool) {
      $('.wg_ibox > .ibox-content').addClass('sk-loading');
    } else {
      $('.wg_ibox > .ibox-content').removeClass('sk-loading');
    }
  }

  __toggleToastr(message, title, type) {
    title = _.isUndefined(title) ? 'Info' : title;
    if (_.includes(['info', 'success', 'warning', 'error'], type)) {
      toastr[type](message, title);
    } else {
      toastr.info(message, title);
    }
  }

  __ajaxErrorHandler__(self, res) {
    self.props.__toggleToastr(res.body.message, 'Error', 'error');
  }

  render() {
    let component = null;
    switch (__ejs_page) { // eslint-disable-line
      case 'login':
        component = <ViewLogin />;
        break;
      case 'register':
        component = <ViewRegister />;
        break;
      case 'pricing':
        component = <ViewPricing />;
        break;
      case 'account':
        component = <ViewAccount />;
        break;
      case 'accountProfile':
        component = <ViewAccountProfile />;
        break;
      default:
        component = <ViewIndex />;
        break;
    }
    return (
      <div className="wg_main_wrapper">
        <CmpLoader loading={this.state.loading} />
        <CmpNavBar _handleSignOutClick={this._handleSignOutClick} />
        {_.includes(['account', 'accountProfile'], __ejs_page) && // eslint-disable-line
        <CmpAccountNavBar _handleSignOutClick={this._handleSignOutClick} />
        }
        <div className="wg_main_container container m-b-lg">
          {React.cloneElement(component, this.childProps)}
        </div>
      </div>
    );
  }
}

export default LayoutMain;
