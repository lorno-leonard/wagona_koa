/* globals __ejs_page */
import React, {Component} from 'react';
import ViewSampleLanding from './../../views/sample-landing';

class LayoutEmpty extends Component {
  render() {
    let component = null;
    switch (__ejs_page) { // eslint-disable-line
      default:
        component = <ViewSampleLanding />;
        break;
    }
    return (
      <div>
        {component}
      </div>
    );
  }
}

export default LayoutEmpty;
