/* globals _, __ejs_page */
import React, {Component} from 'react';
import CmpLoader from './../common/loader';
import ViewTest from './../../views/test/test';
import ViewTestHistory from './../../views/test/history';
import toastr from 'toastr';
import request from './../../../_common/request';

class LayoutTest extends Component {
  constructor(props) {
    super(props);
    this.childProps = {
      appThis: this,
      request: request,
      __toggleLoader: this.__toggleLoader,
      __toggleToastr: this.__toggleToastr,
      __ajaxErrorHandler__: this.__ajaxErrorHandler__
    };
    this.state = {
      loading: false
    };
  }

  __toggleLoader(_bool) {
    const self = _.has(this, 'appThis') ? this.appThis : this;
    self.setState({loading: _bool});
    if (_bool) {
      $('body.landing-page').addClass('loading');
    } else {
      $('body.landing-page').removeClass('loading');
    }
  }

  __toggleToastr(message, title, type) {
    title = _.isUndefined(title) ? 'Info' : title;
    if (_.includes(['info', 'success', 'warning', 'error'], type)) {
      toastr[type](message, title);
    } else {
      toastr.info(message, title);
    }
  }

  __ajaxErrorHandler__(self, res) {
    self.props.__toggleToastr(res.body.message, 'Error', 'error');
  }

  render() {
    let component = null;
    switch (__ejs_page) { // eslint-disable-line
      case 'history':
        component = <ViewTestHistory />;
        break;
      default:
        component = <ViewTest />;
        break;
    }
    return (
      <div className="wg_main_wrapper">
        <CmpLoader loading={this.state.loading} />
        <div className="wg_main_container container m-b-lg">
          {React.cloneElement(component, this.childProps)}
        </div>
      </div>
    );
  }
}

export default LayoutTest;
