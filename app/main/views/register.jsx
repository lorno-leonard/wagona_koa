/* globals _ */
import React, {Component} from 'react';

class ViewRegister extends Component {
  constructor(props) {
    super(props);
    this._handleSubmitSchool = this._handleSubmitSchool.bind(this);
    this._handleSubmitHome = this._handleSubmitHome.bind(this);
    this.state = {
      type: null,
      dataCountry: [],
      errorMessage: '',
      buttonDisabled: false,
      done: false
    };
  }

  _handleChangeType(type) {
    this.setState({type});
    if (!_.isNull(type)) { // eslint-disable-line
      _.delay(() => {
        // Initialize form with Parsley
        $('#' + type + '_form').parsley();
      }, 100);
    }
  }

  _handleSubmitSchool(event) {
    event.preventDefault();

    const self = this;
    const username = this.refs.school_username;
    const email = this.refs.school_email;
    const password = this.refs.school_password;
    const name = this.refs.school_name;
    const num_students = this.refs.school_num_students; // eslint-disable-line camelcase
    const address1 = this.refs.school_address1;
    const address2 = this.refs.school_address2;
    const country = this.refs.school_country;
    const contact_firstname = this.refs.school_contact_firstname; // eslint-disable-line camelcase
    const contact_lastname = this.refs.school_contact_lastname; // eslint-disable-line camelcase
    const tel_no = this.refs.school_tel_no; // eslint-disable-line camelcase
    const cell_no = this.refs.school_cell_no; // eslint-disable-line camelcase
    const website = this.refs.school_website;

    // Trim
    $(username).val($(username).val().trim());
    $(email).val($(email).val().trim());
    $(name).val($(name).val().trim());
    $(address1).val($(address1).val().trim());
    $(address2).val($(address2).val().trim());
    $(contact_firstname).val($(contact_firstname).val().trim());
    $(contact_lastname).val($(contact_lastname).val().trim());
    $(tel_no).val($(tel_no).val().trim());
    $(cell_no).val($(cell_no).val().trim());
    $(website).val($(website).val().trim());

    // Validate
    if (!$('#school_form').parsley().isValid()) {
      return;
    }

    // Reset Error Message
    this.setState({errorMessage: ''});

    // Toggle Loader
    this.props.__toggleLoader(true);

    // Disable button
    this.setState({buttonDisabled: true});

    let data = {
      username: $(username).val(),
      email: $(email).val(),
      password: $(password).val(),
      name: $(name).val(),
      num_students: $(num_students).val(), // eslint-disable-line camelcase
      address1: $(address1).val(),
      address2: $(address2).val(),
      country_id: $(country).val(), // eslint-disable-line camelcase
      first_name: $(contact_firstname).val(), // eslint-disable-line camelcase
      last_name: $(contact_lastname).val(), // eslint-disable-line camelcase
      telephone_number: $(tel_no).val(), // eslint-disable-line camelcase
      cellphone_number: $(cell_no).val(), // eslint-disable-line camelcase
      website: $(website).val()
    };

    this.props.request.call('POST', '/api/signup/school', data, (err, res) => {
      // Toggle Loader
      this.props.__toggleLoader(false);

      // Sroll to top
      $('html, body').animate({scrollTop: 0}, 'fast');

      if (err) {
        // Enable button
        self.setState({buttonDisabled: false});

        self.setState({errorMessage: res.body.message});
        if (_.includes(res.body.message.toLowerCase(), 'username')) {
          $(username).addClass('parsley-error');
        }
        if (_.includes(res.body.message.toLowerCase(), 'email')) {
          $(email).addClass('parsley-error');
        }
        if (_.includes(res.body.message.toLowerCase(), 'school name')) {
          $(name).addClass('parsley-error');
        }
        return;
      }

      // Update done state
      self.setState({done: true});
    });
  }

  _handleSubmitHome(event) {
    event.preventDefault();

    const self = this;
    const username = this.refs.home_username;
    const email = this.refs.home_email;
    const password = this.refs.home_password;
    const firstname = this.refs.home_firstname;
    const lastname = this.refs.home_lastname;
    const address = this.refs.home_address;
    const country = this.refs.home_country;
    const tel_no = this.refs.home_tel_no; // eslint-disable-line camelcase
    const cell_no = this.refs.home_cell_no; // eslint-disable-line camelcase

    // Trim
    $(username).val($(username).val().trim());
    $(email).val($(email).val().trim());
    $(firstname).val($(firstname).val().trim());
    $(lastname).val($(lastname).val().trim());
    $(address).val($(address).val().trim());
    $(tel_no).val($(tel_no).val().trim());
    $(cell_no).val($(cell_no).val().trim());

    // Validate
    if (!$('#home_form').parsley().isValid()) {
      return;
    }

    // Reset Error Message
    this.setState({errorMessage: ''});

    // Toggle Loader
    this.props.__toggleLoader(true);

    // Disable button
    this.setState({buttonDisabled: true});

    let data = {
      username: $(username).val(),
      email: $(email).val(),
      password: $(password).val(),
      first_name: $(firstname).val(), // eslint-disable-line camelcase
      last_name: $(lastname).val(), // eslint-disable-line camelcase
      address: $(address).val(),
      country_id: $(country).val(), // eslint-disable-line camelcase
      telephone_number: $(tel_no).val(), // eslint-disable-line camelcase
      cellphone_number: $(cell_no).val() // eslint-disable-line camelcase
    };

    this.props.request.call('POST', '/api/signup/home', data, (err, res) => {
      // Toggle Loader
      this.props.__toggleLoader(false);

      // Sroll to top
      $('html, body').animate({scrollTop: 0}, 'fast');

      if (err) {
        // Enable button
        self.setState({buttonDisabled: false});

        self.setState({errorMessage: res.body.message});
        if (_.includes(res.body.message.toLowerCase(), 'username')) {
          $(username).addClass('parsley-error');
        }
        if (_.includes(res.body.message.toLowerCase(), 'email')) {
          $(email).addClass('parsley-error');
        }
        return;
      }

      // Update done state
      self.setState({done: true});
    });
  }

  __loadCountryData__() {
    const self = this;
    this.props.request.call('GET', '/api/signup/country?status=1', null, function(err, res) {
      if (err) {
        self.props.__ajaxErrorHandler__(self, res);
        return;
      }

      self.setState({dataCountry: _.sortBy(res.body.data, 'name')});
    });
  }

  componentWillMount() {
    this.__loadCountryData__();
  }

  render() {
    const type = this.state.type;
    const dataCountry = this.state.dataCountry;
    const errorMessage = this.state.errorMessage;
    const buttonDisabled = this.state.buttonDisabled;
    const done = this.state.done;
    return (
      <div className="row">
        <h1>Register</h1>
        <hr/>
        {_.isNull(type) && !done &&
        <div className="col-xs-6 col-xs-offset-3 col-md-4 col-md-offset-4 text-center">
          <h4>Choose your Subscription</h4>
          <div className="row">
            <div className="col-sm-6">
              <button className="btn btn-default btn-lg btn-primary-color btn-block text-uppercase" onClick={this._handleChangeType.bind(this, 'school')}>School</button>
            </div>
            <div className="col-sm-6">
              <button className="btn btn-default btn-lg btn-primary-color btn-block text-uppercase" onClick={this._handleChangeType.bind(this, 'home')}>Home</button>
            </div>
          </div>
        </div>
        }
        {type === 'school' && !done &&
        <div className="container">
          <h2>School Subscription</h2>
          <form id="school_form" onSubmit={this._handleSubmitSchool}>
            {!_.isEmpty(errorMessage) &&
            <div className="row">
              <div className="col-md-8 col-md-offset-2">
                <div className="alert alert-danger">{errorMessage}</div>
              </div>
            </div>
            }
            <div className="row">
              <h4>Login Details</h4>
              <div className="col-sm-6 col-md-4 col-md-offset-2">
                <div className="form-group">
                  <label htmlFor="school_username">Username <span className="text-danger">*</span></label>
                  <input type="text" id="school_username" ref="school_username" className="form-control" placeholder="Username" autoComplete="off" required maxLength="100" />
                </div>
                <div className="form-group">
                  <label htmlFor="school_email">Email Address <span className="text-danger">*</span></label>
                  <input type="email" id="school_email" ref="school_email" className="form-control" placeholder="Email Address" autoComplete="off" required maxLength="100" />
                </div>
              </div>
              <div className="col-sm-6 col-md-4">
                <div className="form-group">
                  <label htmlFor="school_password">Password <span className="text-danger">*</span></label>
                  <input type="password" id="school_password" ref="school_password" className="form-control" placeholder="Password" required data-parsley-equalto="#school_confirm_password" data-parsley-error-message="Passwords must be the same" />
                </div>
                <div className="form-group">
                  <label htmlFor="school_confirm_password">Confirm Password <span className="text-danger">*</span></label>
                  <input type="password" id="school_confirm_password" ref="school_confirm_password" className="form-control" placeholder="Confirm Password" required data-parsley-equalto="#school_password" data-parsley-error-message="Passwords must be the same" />
                </div>
              </div>
            </div>
            <div className="row">
              <h4>School Information</h4>
              <div className="col-sm-6 col-md-4 col-md-offset-2">
                <div className="form-group">
                  <label htmlFor="school_name">Name of School <span className="text-danger">*</span></label>
                  <input type="text" id="school_name" ref="school_name" className="form-control" placeholder="Name of School" autoComplete="off" required maxLength="100" />
                </div>
                <div className="form-group">
                  <label htmlFor="school_num_students">Number of Students <span className="text-danger">*</span></label>
                  <input type="number" id="school_num_students" ref="school_num_students" className="form-control" placeholder="Number of Students" autoComplete="off" required min="1" max="1000" />
                </div>
                <div className="form-group">
                  <label htmlFor="school_address1">Address 1 <span className="text-danger">*</span></label>
                  <input type="text" id="school_address1" ref="school_address1" className="form-control" placeholder="Address 1" autoComplete="off" required />
                </div>
                <div className="form-group">
                  <label htmlFor="school_address2">Address 2</label>
                  <input type="text" id="school_address2" ref="school_address2" className="form-control" placeholder="Address 2" autoComplete="off" />
                </div>
                <div className="form-group">
                  <label htmlFor="school_country">Country <span className="text-danger">*</span></label>
                  <select id="school_country" name="school_country" ref="school_country" className="form-control" required>
                    <option value="">-- Select Country --</option>
                    {dataCountry.map((value, index) => {
                      return <option key={index} value={value.id}>{value.name}</option>;
                    })}
                  </select>
                </div>
              </div>
              <div className="col-sm-6 col-md-4">
                <div className="form-group">
                  <label htmlFor="school_contact_firstname">Contact First Name <span className="text-danger">*</span></label>
                  <input type="text" id="school_contact_firstname" ref="school_contact_firstname" className="form-control" placeholder="Contact First Name" autoComplete="off" required maxLength="50" />
                </div>
                <div className="form-group">
                  <label htmlFor="school_contact_lastname">Contact Last Name <span className="text-danger">*</span></label>
                  <input type="text" id="school_contact_lastname" ref="school_contact_lastname" className="form-control" placeholder="Contact Last Name" autoComplete="off" required maxLength="50" />
                </div>
                <div className="form-group">
                  <label htmlFor="school_tel_no">Telephone Number</label>
                  <input type="text" id="school_tel_no" ref="school_tel_no" className="form-control" placeholder="Telephone Number" autoComplete="off" />
                </div>
                <div className="form-group">
                  <label htmlFor="school_cell_no">Cellphone Number</label>
                  <input type="text" id="school_cell_no" ref="school_cell_no" className="form-control" placeholder="Cellphone Number" autoComplete="off" />
                </div>
                <div className="form-group">
                  <label htmlFor="school_website">Website URL</label>
                  <input type="text" id="school_website" ref="school_website" className="form-control" placeholder="Website URL" autoComplete="off" />
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-3 col-sm-offset-3 col-md-2 col-md-offset-4">
                <a href="#" className="btn btn-block btn-info" onClick={this._handleChangeType.bind(this, null)}>Cancel</a>
              </div>
              <div className="col-sm-3 col-md-2">
                <button className="btn btn-block btn-normalize btn-primary" disabled={buttonDisabled}>Submit</button>
              </div>
            </div>
          </form>
        </div>
        }
        {type === 'home' && !done &&
        <div className="container">
          <h2>Home Subscription</h2>
          <form id="home_form" onSubmit={this._handleSubmitHome}>
            {!_.isEmpty(errorMessage) &&
            <div className="row">
              <div className="col-md-8 col-md-offset-2">
                <div className="alert alert-danger">{errorMessage}</div>
              </div>
            </div>
            }
            <div className="row">
              <h4>Login Details</h4>
              <div className="col-sm-6 col-md-4 col-md-offset-2">
                <div className="form-group">
                  <label htmlFor="home_username">Username <span className="text-danger">*</span></label>
                  <input type="text" id="home_username" ref="home_username" className="form-control" placeholder="Username" autoComplete="off" required maxLength="100" />
                </div>
                <div className="form-group">
                  <label htmlFor="home_email">Email Address <span className="text-danger">*</span></label>
                  <input type="email" id="home_email" ref="home_email" className="form-control" placeholder="Email Address" autoComplete="off" required maxLength="100" />
                </div>
              </div>
              <div className="col-sm-6 col-md-4">
                <div className="form-group">
                  <label htmlFor="home_password">Password <span className="text-danger">*</span></label>
                  <input type="password" id="home_password" ref="home_password" className="form-control" placeholder="Password" required data-parsley-equalto="#home_confirm_password" data-parsley-error-message="Passwords must be the same" />
                </div>
                <div className="form-group">
                  <label htmlFor="home_confirm_password">Confirm Password <span className="text-danger">*</span></label>
                  <input type="password" id="home_confirm_password" ref="home_confirm_password" className="form-control" placeholder="Confirm Password" required data-parsley-equalto="#home_password" data-parsley-error-message="Passwords must be the same" />
                </div>
              </div>
            </div>
            <div className="row">
              <h4>User Details</h4>
              <div className="col-md-12">
                <div className="row">
                  <div className="col-sm-6 col-md-4 col-md-offset-2">
                    <div className="form-group">
                      <label htmlFor="home_firstname">First Name <span className="text-danger">*</span></label>
                      <input type="text" id="home_firstname" ref="home_firstname" className="form-control" placeholder="First Name" autoComplete="off" required maxLength="50" />
                    </div>
                  </div>
                  <div className="col-sm-6 col-md-4">
                    <div className="form-group">
                      <label htmlFor="home_lastname">Last Name <span className="text-danger">*</span></label>
                      <input type="text" id="home_lastname" ref="home_lastname" className="form-control" placeholder="Last Name" autoComplete="off" required maxLength="50" />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6 col-md-4 col-md-offset-2">
                    <div className="form-group">
                      <label htmlFor="home_address">Address <span className="text-danger">*</span></label>
                      <input type="text" id="home_address" ref="home_address" className="form-control" placeholder="Address" autoComplete="off" required />
                    </div>
                  </div>
                  <div className="col-sm-6 col-md-4">
                    <div className="form-group">
                      <label htmlFor="home_country">Country <span className="text-danger">*</span></label>
                      <select id="home_country" name="home_country" ref="home_country" className="form-control" required>
                        <option value="">-- Select Country --</option>
                        {dataCountry.map((value, index) => {
                          return <option key={index} value={value.id}>{value.name}</option>;
                        })}
                      </select>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6 col-md-4 col-md-offset-2">
                    <div className="form-group">
                      <label htmlFor="home_tel_no">Telephone Number</label>
                      <input type="text" id="home_tel_no" ref="home_tel_no" className="form-control" placeholder="Telephone Number" autoComplete="off" />
                    </div>
                  </div>
                  <div className="col-sm-6 col-md-4">
                    <div className="form-group">
                      <label htmlFor="home_cell_no">Cellphone Number</label>
                      <input type="text" id="home_cell_no" ref="home_cell_no" className="form-control" placeholder="Cellphone Number" autoComplete="off" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-3 col-sm-offset-3 col-md-2 col-md-offset-4">
                <a href="#" className="btn btn-block btn-info" onClick={this._handleChangeType.bind(this, null)}>Cancel</a>
              </div>
              <div className="col-sm-3 col-md-2">
                <button className="btn btn-block btn-normalize btn-primary" disabled={buttonDisabled}>Submit</button>
              </div>
            </div>
          </form>
        </div>
        }
        {done &&
        <div className="col-md-8 col-md-offset-2">
          <div className="alert alert-success">Thank you for registering a <strong className="text-capitalize">{type}</strong> account. <a href="/login">Please login.</a></div>
        </div>
        }
      </div>
    );
  }
}

ViewRegister.propTypes = {
  request: React.PropTypes.object,
  __toggleLoader: React.PropTypes.func
};

export default ViewRegister;
