/* globals __ejs_user */
import React, {Component} from 'react';
import CmpAccountSideBar from './../../components/common/account-sidebar';

class ViewAccountProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      edit: {
        first_name: false, // eslint-disable-line
        last_name: false // eslint-disable-line
      },
      loading: {
        first_name: false, // eslint-disable-line
        last_name: false // eslint-disable-line
      }
    };
  }

  componentDidMount() {
    const self = this;
    $(self.refs.first_name).val(__ejs_user.first_name); // eslint-disable-line camelcase
    $(self.refs.last_name).val(__ejs_user.last_name); // eslint-disable-line camelcase
  }

  render() {
    return (
      <div className="ibox wg_ibox">
        <div className="ibox-content">
          <div className="row">
            <div className="col-md-3">
              <CmpAccountSideBar />
            </div>
            <div className="col-md-9">
              <h3 className="text-left text-primary-color">Account Profile</h3>
              <hr />
              <h4 className="text-left">Basic Information</h4>
              <form>
                <div className="row">
                  <div className="col-md-6">
                    <label htmlFor="first_name">First Name <span className="text-danger">*</span></label>
                    <input type="text" id="first_name" ref="first_name" className="form-control" placeholder="First Name" autoComplete="off" required maxLength="50" />
                  </div>
                  <div className="col-md-6">
                    <label htmlFor="last_name">Last Name <span className="text-danger">*</span></label>
                    <input type="text" id="last_name" ref="last_name" className="form-control" placeholder="Last Name" autoComplete="off" required maxLength="50" />
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ViewAccountProfile;
