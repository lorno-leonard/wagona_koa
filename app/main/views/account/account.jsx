/* globals _, __ejs_user */
import React, {Component} from 'react';
import SubViewIndividual from './individual';

class ViewAccount extends Component {
  render() {
    let component = null;
    switch (_.toLower(__ejs_user.role)) { // eslint-disable-line
      case 'individual':
        component = <SubViewIndividual />;
        break;
      default:
        component = <div></div>;
        break;
    }
    return (
      <div className="ibox wg_ibox">
        {React.cloneElement(component, this.props)}
      </div>
    );
  }
}

export default ViewAccount;
