/* globals __ejs_account */
import React, {Component} from 'react';
import SubViewIndividualSelectSyllabi from './individual-select-syllabi';
import SubViewIndividualSubjectList from './individual-subject-list';

class SubViewIndividual extends Component {
  render() {
    let syllabiId = __ejs_account.syllabi_id; // eslint-disable-line
    return (
      <div className="ibox-content">
        <div className="sk-spinner sk-spinner-double-bounce">
          <div className="sk-double-bounce1"></div>
          <div className="sk-double-bounce2"></div>
        </div>
        {!syllabiId && React.cloneElement(<SubViewIndividualSelectSyllabi />, this.props)}
        {syllabiId && React.cloneElement(<SubViewIndividualSubjectList />, this.props)}
      </div>
    );
  }
}

export default SubViewIndividual;
