/* globals window, _, __ejs_account */
import React, {Component} from 'react';

class SubViewIndividualSelectSyllabi extends Component {
  constructor(props) {
    super(props);
    this._handleSubmitSyllabi = this._handleSubmitSyllabi.bind(this);
    this.state = {
      dataSyllabi: [],
      buttonSyllabiDisabled: false,
      loadingSyllabi: false,
      errorMessageSyllabi: ''
    };
  }

  _handleSubmitSyllabi() {
    let syllabiId = $('input[name="wg_account_syllabi_choice"]:checked').val();

    // Reset error message on syllabi
    this.setState({errorMessageSyllabi: ''});

    if (!syllabiId) {
      this.setState({errorMessageSyllabi: 'Please select a Syllabi.'});
      return;
    }

    // Set loading and button disabled
    this.setState({
      buttonSyllabiDisabled: true,
      loadingSyllabi: true
    });

    let data = {
      syllabi_id: syllabiId // eslint-disable-line
    };

    this.props.request.call('PATCH', '/api/account/student/' + __ejs_account.student_id, data, function(err, res) { // eslint-disable-line
      _.delay(() => {
        if (err) {
          console.log(err);
          return;
        }

        // Reload
        window.location.reload();
      }, 1000);
    });
  }

  __loadSyllabiData__() {
    const self = this;

    // Set loading and button disabled
    this.setState({
      buttonSyllabiDisabled: true,
      loadingSyllabi: true
    });

    this.props.request.call('GET', '/api/account/syllabi?status=1', null, function(err, res) {
      _.delay(() => {
        self.setState({
          buttonSyllabiDisabled: false,
          loadingSyllabi: false
        });

        if (err) {
          self.props.__ajaxErrorHandler__(self, res);
          return;
        }

        self.setState({dataSyllabi: _.sortBy(res.body.data, 'name')});
      }, 1000);
    });
  }

  componentDidMount() {
    this.__loadSyllabiData__();
  }

  render() {
    let dataSyllabi = this.state.dataSyllabi;
    let buttonSyllabiDisabled = this.state.buttonSyllabiDisabled;
    let loadingSyllabi = this.state.loadingSyllabi;
    let errorMessageSyllabi = this.state.errorMessageSyllabi;
    return (
      <div>
        <div className="row">
          <div className="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
            <button className="btn btn-lg btn-block btn-primary-color" data-toggle="modal" data-target="#wg_choose_syllabi_modal">Choose a Syllabi now!</button>
          </div>
        </div>
        <div className="modal inmodal animated fadeIn" id="wg_choose_syllabi_modal" role="dialog" data-backdrop="static">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span className="sr-only">Close</span></button>
                <h4 className="modal-title">Syallabi List</h4>
                <small className="font-bold">Choose a Syllabi to start taking tests!</small>
              </div>
              <div className="modal-body">
                {loadingSyllabi &&
                <div className="sk-spinner sk-spinner-double-bounce">
                  <div className="sk-double-bounce1"></div>
                  <div className="sk-double-bounce2"></div>
                </div>
                }
                {!loadingSyllabi &&
                <div>
                  <button className="btn btn-sm btn-info m-b-xs" onClick={() => this.__loadSyllabiData__()}><i className="fa fa-refresh"></i> Reload</button>
                  {!_.isEmpty(errorMessageSyllabi) &&
                  <span className="text-danger m-l-sm">{errorMessageSyllabi}</span>
                  }
                  <table className="table table-bordered table-striped">
                    <tbody>
                      {dataSyllabi.length === 0 &&
                      <tr>
                        <td>No Syllabi available. Please contact the System Administrator.</td>
                      </tr>
                      }
                      {dataSyllabi.length > 0 && dataSyllabi.map((row, index) => {
                        return <tr key={index}>
                          <td>
                            <div className="radio radio-success m-t-none m-b-none">
                              <input type="radio" id={`wg_account_syllabi_choice${index}`} name="wg_account_syllabi_choice" value={row.id} />
                              <label className="block" htmlFor={`wg_account_syllabi_choice${index}`}>{row.name}</label>
                            </div>
                          </td>
                        </tr>;
                      })}
                    </tbody>
                  </table>
                </div>
                }
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-white" data-dismiss="modal" disabled={buttonSyllabiDisabled}>Close</button>
                <button type="button" className="btn btn-primary btn-normalize" disabled={buttonSyllabiDisabled} onClick={this._handleSubmitSyllabi}>Save Syllabi</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

SubViewIndividualSelectSyllabi.propTypes = {
  request: React.PropTypes.object
};

export default SubViewIndividualSelectSyllabi;
