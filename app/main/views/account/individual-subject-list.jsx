/* globals window, _, __ejs_account */
import React, {Component} from 'react';
import CmpSpinner from './../../components/common/spinner';
import asyncSeries from 'async/series';
import asyncEach from 'async/each';
import asyncParallel from 'async/parallel';
import moment from 'moment';

class SubViewIndividualSubjectList extends Component {
  constructor(props) {
    super(props);
    this.testDisplayLimit = 5;
    this.state = {
      dataSubject: [],
      currentSubject: null,
      selectedTopics: [],
      loadingStartTest: false
    };
  }

  _handleTakeTestClick(subject) {
    const self = this;
    self.setState({currentSubject: subject, selectedTopics: []});
    $('#wg_take_test_modal').modal('show');
  }

  _handleTopicChecked(event) {
    const selectedTopics = this.state.selectedTopics;
    const target = event.target;

    if (target.checked) {
      selectedTopics.push(target.value);
    } else {
      selectedTopics.splice(selectedTopics.indexOf(target.value), 1);
    }
    this.setState({selectedTopics});
  }

  _handleStartTestClick() {
    const self = this;
    const currentSubject = this.state.currentSubject;

    // Show loading
    this.setState({loadingStartTest: true});

    let data = {
      subject_id: currentSubject.id, // eslint-disable-line
      topics: this.state.selectedTopics
    };
    this.props.request.call('POST', '/api/test', data, (err, res) => {
      // Hide loading
      self.setState({loadingStartTest: false});

      if (err) {
        self.props.__ajaxErrorHandler__(self, res);
        return;
      }

      // Hide Modal
      $('#wg_take_test_modal').modal('hide');

      let hash = res.body.hash;
      let baseUrl = process.env.NODE_ENV === 'development' ? process.env.BASE_URL_DEV : process.env.BASE_URL;
      window.open(baseUrl + '/test/' + hash, '_blank');
    });
  }

  _handleReloadSubjectTests(index) {
    const self = this;
    const dataSubject = this.state.dataSubject;

    // Initialize loading
    dataSubject[index].loading = true;
    this.setState({dataSubject});

    // Reload Subject Tests
    const subject = dataSubject[index];
    this.props.request.call('GET', '/api/account/subject/' + subject.id + '/test?page=1&limit=' + self.testDisplayLimit, null, (err, res) => {
      if (err) {
        self.props.__ajaxErrorHandler__(self, res);
        return;
      }
      subject.tests = res.body.data;
      subject.loading = false;
      this.setState({dataSubject});
    });
  }

  _handleReloadSubjectTopics() {
    const self = this;
    const currentSubject = this.state.currentSubject;

    // Initialize loading
    currentSubject.loadingTopics = true;
    this.setState({currentSubject, selectedTopics: []});

    // Reload Subject Topics
    const subject = currentSubject;
    this.props.request.call('GET', '/api/account/subject/' + subject.id + '/topic', null, (err, res) => {
      if (err) {
        self.props.__ajaxErrorHandler__(self, res);
        return;
      }
      subject.topics = res.body.data;
      subject.loadingTopics = false;
      this.setState({currentSubject});
    });
  }

  __loadSubjectData__() {
    const self = this;
    const syllabiId = __ejs_account.syllabi_id; // eslint-disable-line

    // Toggle Ibox Loader
    this.props.__toggleIboxLoader(true);

    asyncSeries({
      subjects: callbackSeries => {
        self.props.request.call('GET', '/api/account/syllabi/' + syllabiId + '/subject?status=1', null, (err, res) => {
          // Toggle Ibox Loader
          self.props.__toggleIboxLoader(false);

          if (err) {
            self.props.__ajaxErrorHandler__(self, res);
            callbackSeries(err);
            return;
          }

          // Initialize slick
          $('.slick').slick({
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1
          });

          let dataSubject = _.cloneDeep(res.body.data);
          _.each(dataSubject, row => {
            row.loading = true;
            row.loadingTopics = false;
          });
          self.setState({dataSubject: _.sortBy(dataSubject, 'name')});
          callbackSeries();
        });
      },
      subjectDetails: callbackSeries => {
        const dataSubject = self.state.dataSubject;
        asyncEach(dataSubject, (row, callbackEach) => {
          asyncParallel({
            loadTests: callbackParallel => {
              self.props.request.call('GET', '/api/account/subject/' + row.id + '/test?page=1&limit=' + self.testDisplayLimit, null, (err, res) => {
                if (err) {
                  self.props.__ajaxErrorHandler__(self, res);
                  callbackParallel(err);
                  return;
                }
                row.tests = res.body.data;
                callbackParallel();
              });
            },
            loadTopics: callbackParallel => {
              self.props.request.call('GET', '/api/account/subject/' + row.id + '/topic', null, (err, res) => {
                if (err) {
                  self.props.__ajaxErrorHandler__(self, res);
                  callbackParallel(err);
                  return;
                }
                row.topics = res.body.data;
                callbackParallel();
              });
            }
          }, (err, results) => {
            if (err) {
              console.log(err);
              callbackEach('asyncParallel', err);
              return;
            }
            row.loading = false;
            self.setState({dataSubject});
            callbackEach();
          });
        }, err => {
          if (err) {
            console.log(err);
            callbackSeries(err);
          }
          callbackSeries();
        });
      }
    }, (err, results) => {
      if (err) {
        console.log(err);
        return;
      }
    });
  }

  componentDidMount() {
    const self = this;
    this.__loadSubjectData__();

    // On Modal Hiddden
    $('#wg_take_test_modal').on('hidden.bs.modal', () => {
      self.setState({currentSubject: null, selectedTopics: []});
    });
  }

  render() {
    let dataSubject = this.state.dataSubject;
    let currentSubject = this.state.currentSubject;
    let loadingStartTest = this.state.loadingStartTest;
    return (
      <div>
        <div className="row slick">
          {dataSubject.map((row, index) => {
            let tests = row.tests;
            let times = _.times(this.testDisplayLimit);
            return <div className="col-sm-6 col-md-3 wg_dashboard_subject_list" key={index}>
              <div className="panel panel-primary-color">
                <div className="panel-heading">
                  <h4 className="m-n">{row.name}</h4>
                </div>
                <div className="panel-body">
                  {row.loading && <CmpSpinner hasMarginTop={true} />}
                  {!row.loading &&
                  <div>
                    <button className="btn btn-xs btn-info wg_subject_test_reload_button" title="Reload Tests" onClick={this._handleReloadSubjectTests.bind(this, index)}>
                      <i className="fa fa-refresh"></i>
                    </button>
                    {times.map((valueTest, keyTest) => {
                      let content = null;
                      if (tests[Number(valueTest)]) {
                        let test = tests[valueTest];
                        let testDone = !_.isNull(test.date_ended);
                        let testStatusText = testDone ? moment(tests[valueTest].date_ended).format('DD/MM/YY') : 'Ongoing'; // eslint-disable-line
                        let testPercentage = 0;
                        let testBg = '';
                        if (testDone) {
                          testPercentage = Math.round((test.details.num_correct / test.details.num_questions) * 100);

                          if (testPercentage === 0) testBg = 'bg-black-color';
                          else if (testPercentage < 50) testBg = 'bg-red-color';
                          else if (testPercentage >= 50 && testPercentage < 75) testBg = 'bg-amber-color';
                          else testBg = 'bg-green-color';
                        }
                        content = <div className={`row m-b-sm ${keyTest === 0 ? 'm-t-sm' : ''}`} key={keyTest}>
                          <div className="col-sm-8">
                            <div className="wg_subject_test_text">Test {Number(valueTest) + 1}: {testStatusText}</div>
                          </div>
                          <div className="col-xs-4">
                            <a href={`/test/${test.hash}${_.isNull(test.date_ended) ? '' : '/history'}`} target="_blank">
                              {testDone && <div className={`wg_subject_test_score ${testBg}`}>{testPercentage}%</div>}
                              {!testDone && <div className="wg_subject_test_score black"><i className="fa fa-search"></i></div>}
                            </a>
                          </div>
                        </div>;
                      } else {
                        content = <div className={`row m-b-sm ${keyTest === 0 ? 'm-t-sm' : ''}`} key={keyTest}>
                          <div className="col-xs-8">
                            <div className="wg_subject_test_text">Test {Number(valueTest) + 1}: Pending</div>
                          </div>
                          <div className="col-xs-4">
                            <div className="wg_subject_test_score black">--</div>
                          </div>
                        </div>;
                      }
                      return content;
                    })}
                    <button className="btn btn-primary-color btn-block btn-normalize" onClick={this._handleTakeTestClick.bind(this, row)}>Take Test</button>
                  </div>
                  }
                </div>
              </div>
            </div>;
          })}
        </div>
        <div className="modal inmodal animated fadeIn" id="wg_take_test_modal" role="dialog" data-backdrop="static">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span className="sr-only">Close</span></button>
                <h4 className="modal-title">{_.isNull(currentSubject) ? '' : currentSubject.name}</h4>
                <span>Select Topic(s) to include in the Test</span>
              </div>
              <div className="modal-body no-padding">
                {!_.isNull(currentSubject) && currentSubject.loadingTopics &&
                <CmpSpinner hasMarginTop={true} />
                }
                {!_.isNull(currentSubject) && !currentSubject.loadingTopics &&
                <div>
                  <button className="btn btn-xs btn-info m-xs" onClick={this._handleReloadSubjectTopics.bind(this)}><i className="fa fa-refresh"></i> Reload Topics</button>
                  <table className="table table-striped">
                    <tbody>
                      {currentSubject.topics.map((row, index) => {
                        return <tr key={index}>
                          <td>
                            <div className="checkbox checkbox-success m-n">
                              <input id={`wg_topic_${row.id}`} type="checkbox" name="wg_topics_selected" value={row.id} onChange={this._handleTopicChecked.bind(this)} />
                              <label htmlFor={`wg_topic_${row.id}`}>{row.name}</label>
                            </div>
                          </td>
                        </tr>;
                      })}
                    </tbody>
                  </table>
                </div>
                }
              </div>
              <div className="modal-footer">
                <div className="row">
                  <div className="col-xs-4 col-xs-offset-4">
                    {loadingStartTest && <CmpSpinner hasMarginTop={false} />}
                    {!loadingStartTest && <button type="button" className="btn btn-primary-color btn-lg btn-block" onClick={this._handleStartTestClick.bind(this)} disabled={this.state.selectedTopics.length === 0}>Start the Test</button>}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

SubViewIndividualSubjectList.propTypes = {
  request: React.PropTypes.object,
  __toggleIboxLoader: React.PropTypes.func
};

export default SubViewIndividualSubjectList;
