/* globals _, __ejs_pricing */
import React, {Component} from 'react';

class ViewPricing extends Component {
  render() {
    let temp = null;
    return (
      <div className="row">
        <h1>Pricing</h1>
        <hr/>
        <h4>School Subscription</h4>
        <div className="row">
          <div className="col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-0">
            <ul className="pricing-plan list-unstyled border-left-right border-bottom">
              <li className="pricing-title">Month</li>
              {_.filter(__ejs_pricing, {duration: 'month', payer: 'S'}).map((row, index) => {
                temp = _.isNull(row.max_students) ? temp : row.max_students;
                return <li key={index} className="pricing-price">
                  {!_.isNull(row.max_students) &&
                    <div>
                      <span>${row.price.toFixed(2)}</span>/student - up to <span>{row.max_students}</span> students
                    </div>
                  }
                  {_.isNull(row.max_students) &&
                    <div>
                      <span>${row.price.toFixed(2)}</span>/student - <span>{temp}+</span> students
                    </div>
                  }
                </li>;
              })}
              <li><a className="btn btn-primary btn-xs" href="/register">Signup</a></li>
            </ul>
          </div>
          <div className="col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-0">
            <ul className="pricing-plan list-unstyled border-left-right border-bottom">
              <li className="pricing-title">Term</li>
              {_.filter(__ejs_pricing, {duration: 'term', payer: 'S'}).map((row, index) => {
                temp = _.isNull(row.max_students) ? temp : row.max_students;
                return <li key={index} className="pricing-price">
                  {!_.isNull(row.max_students) &&
                    <div>
                      <span>${row.price.toFixed(2)}</span>/student - up to <span>{row.max_students}</span> students
                    </div>
                  }
                  {_.isNull(row.max_students) &&
                    <div>
                      <span>${row.price.toFixed(2)}</span>/student - <span>{temp}+</span> students
                    </div>
                  }
                </li>;
              })}
              <li><a className="btn btn-primary btn-xs" href="/register">Signup</a></li>
            </ul>
          </div>
          <div className="col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-0">
            <ul className="pricing-plan list-unstyled border-left-right border-bottom">
              <li className="pricing-title">Annum</li>
              {_.filter(__ejs_pricing, {duration: 'annum', payer: 'S'}).map((row, index) => {
                temp = _.isNull(row.max_students) ? temp : row.max_students;
                return <li key={index} className="pricing-price">
                  {!_.isNull(row.max_students) &&
                    <div>
                      <span>${row.price.toFixed(2)}</span>/student - up to <span>{row.max_students}</span> students
                    </div>
                  }
                  {_.isNull(row.max_students) &&
                    <div>
                      <span>${row.price.toFixed(2)}</span>/student - <span>{temp}+</span> students
                    </div>
                  }
                </li>;
              })}
              <li><a className="btn btn-primary btn-xs" href="/register">Signup</a></li>
            </ul>
          </div>
        </div>
        <hr/>
        <h4>Home Subscription</h4>
        <div className="row">
          <div className="col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-0">
            <ul className="pricing-plan list-unstyled border-left-right border-bottom">
              <li className="pricing-title">Month</li>
              {_.filter(__ejs_pricing, {duration: 'month', payer: 'I'}).map((row, index) => {
                return <li key={index}>
                  <h1 className="text-success">${Math.round(Number(row.price)) === Number(row.price) ? row.price : row.price.toFixed(2)}</h1>
                </li>;
              })}
              <li><a className="btn btn-primary btn-xs" href="/register">Signup</a></li>
            </ul>
          </div>
          <div className="col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-0">
            <ul className="pricing-plan list-unstyled border-left-right border-bottom">
              <li className="pricing-title">Term</li>
              {_.filter(__ejs_pricing, {duration: 'term', payer: 'I'}).map((row, index) => {
                return <li key={index}>
                  <h1 className="text-success">${Math.round(Number(row.price)) === Number(row.price) ? row.price : row.price.toFixed(2)}</h1>
                </li>;
              })}
              <li><a className="btn btn-primary btn-xs" href="/register">Signup</a></li>
            </ul>
          </div>
          <div className="col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-0">
            <ul className="pricing-plan list-unstyled border-left-right border-bottom">
              <li className="pricing-title">Annum</li>
              {_.filter(__ejs_pricing, {duration: 'annum', payer: 'I'}).map((row, index) => {
                return <li key={index}>
                  <h1 className="text-success">${Math.round(Number(row.price)) === Number(row.price) ? row.price : row.price.toFixed(2)}</h1>
                </li>;
              })}
              <li><a className="btn btn-primary btn-xs" href="/register">Signup</a></li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default ViewPricing;
