/* globals window, _, __ejs_test */
import React, {Component} from 'react';

class ViewTest extends Component {
  constructor(props) {
    super(props);
    this._handleMarkTestConfirmClick = this._handleMarkTestConfirmClick.bind(this);
    this.state = {
      invalidMessage: '',
      valid: null,
      hash: null,
      currentQuestion: null,
      currentQuestionNum: 1,
      dataQuestions: [],
      testInitialized: false
    };
  }

  _handleChangeQuestionClick(questionNum) {
    this.setState({currentQuestionNum: questionNum});
  }

  _handleOptionChange(questionIndex, choiceIndex) {
    let dataQuestions = this.state.dataQuestions;
    dataQuestions[questionIndex].selected = choiceIndex;
    this.setState({dataQuestions});

    // Change page when an answer is selected
    if (dataQuestions.length >= (questionIndex + 2)) {
      _.delay(() => {
        this.setState({currentQuestionNum: (questionIndex + 2)});
      }, 300);
    }
  }

  _handleMarkTestConfirmClick() {
    const self = this;
    const hash = this.state.hash;
    const dataQuestions = this.state.dataQuestions;
    let questions = dataQuestions.map(question => {
      return _.pick(question, [
        'id',
        'selected',
        'type'
      ]);
    });

    // Show loader
    self.props.__toggleLoader(true);

    this.props.request.call('POST', '/api/test/' + hash + '/mark', {questions}, function(err, res) {
      // Hide loader
      self.props.__toggleLoader(false);

      if (err) {
        self.props.__ajaxErrorHandler__(self, res);
        return;
      }

      // Redirect to history
      window.location.href = '/test/' + hash + '/history';
    });
  }

  componentDidMount() {
    const self = this;
    const hash = __ejs_test.hash; // eslint-disable-line
    const valid = __ejs_test.valid; // eslint-disable-line
    this.setState({hash, valid});

    if (valid) {
      // Show loader
      this.props.__toggleLoader(true);
    } else {
      this.setState({invalidMessage: 'Invalid Test Id.'});
    }

    this.props.request.call('GET', '/api/test/' + hash + '/question?type=test', null, function(err, res) {
      // Hide loader
      self.props.__toggleLoader(false);

      if (err) {
        self.props.__ajaxErrorHandler__(self, res);
        return;
      }

      let dataQuestions = _.shuffle(res.body.data);
      self.setState({dataQuestions});
      self.setState({currentQuestion: dataQuestions[0]});
      self.setState({currentQuestionNum: 1});
      self.setState({testInitialized: true});
    });
  }

  render() {
    const valid = this.state.valid;
    const invalidMessage = this.state.invalidMessage;
    const currentQuestion = this.state.currentQuestion;
    const currentQuestionNum = this.state.currentQuestionNum;
    const dataQuestions = this.state.dataQuestions;
    const testInitialized = this.state.testInitialized;
    return (
      <div className="wg_test_proper">
        {!_.isNull(valid) && !valid &&
        <div className="col-md-6 col-md-offset-3">
          <div className="alert alert-danger">{invalidMessage} <a href="/account">Go back to Account dashboard.</a></div>
        </div>
        }
        {!_.isNull(valid) && valid &&
        <div>
          <div className="row">
            <div className="col-md-12">
              {!_.isNull(currentQuestion) && <h1 className="wg_topic_name">Topic: {currentQuestion.topic_name}</h1>}
            </div>
          </div>
          {dataQuestions.length > 0 &&
          <div>
            <div className="row wg_question_numbers">
              <div className="col-md-12 text-center">
                <button className="btn btn-default m-l-xs m-r-xs" disabled={currentQuestionNum <= 1} onClick={this._handleChangeQuestionClick.bind(this, (currentQuestionNum - 1))}><i className="fa fa-angle-left"></i></button>
                {_.times(dataQuestions.length, key => {
                  let question = dataQuestions[key];
                  let buttonClass = _.has(question, 'selected') ? 'btn-info' : 'btn-default';
                  return <button className={`btn m-l-xs m-r-xs ${currentQuestionNum === (key + 1) ? 'btn-danger' : buttonClass}`} key={key} onClick={this._handleChangeQuestionClick.bind(this, (key + 1))}>{key + 1}</button>;
                })}
                <button className="btn btn-default m-l-xs m-r-xs" disabled={currentQuestionNum >= dataQuestions.length} onClick={this._handleChangeQuestionClick.bind(this, (currentQuestionNum + 1))}><i className="fa fa-angle-right"></i></button>
              </div>
            </div>
            <div className="row m-t-sm">
              <div className="col-md-2 col-md-offset-5">
                <button className="btn btn-primary btn-normalize btn-block" data-toggle="modal" data-target="#wg_mark_test_modal"><i className="fa fa-check"></i> Mark Test</button>
              </div>
            </div>
          </div>
          }
          <hr/>
          {dataQuestions.length > 0 &&
          <div className={`m-t-xl ${testInitialized ? '' : 'hide'}`}>
            {dataQuestions.map((question, key) => {
              return <div className={`row animated fadeIn wg_question_row ${currentQuestionNum === (key + 1) ? '' : 'hide'}`} key={key}>
                <div className="col-xs-6">
                  <p dangerouslySetInnerHTML={{__html: question.description}}></p>
                </div>
                <div className="col-xs-6">
                  {question.choices.map((choice, keyChoice) => {
                    const choiceClass = 'border-top-bottom border-left-right border-size-sm';
                    return <label key={keyChoice} className={`wg_question_choice row p-xs b-r-xl m-b-xs m-l-none m-r-none full-width ${choiceClass}`} htmlFor={`wg_question_choice-${key}-${keyChoice}`}>
                      <div className="col-lg-10">
                        <div className="radio radio-success radio-inline m-t-none m-b-none">
                          <input type="radio" id={`wg_question_choice-${key}-${keyChoice}`} name={`wg_question_choice-${key}`} value={keyChoice}
                            checked={_.has(question, 'selected') && question.selected === keyChoice}
                            onChange={this._handleOptionChange.bind(this, key, keyChoice)} />
                          <label htmlFor={`wg_question_choice-${key}-${keyChoice}`} dangerouslySetInnerHTML={{__html: choice.description}}></label>
                        </div>
                      </div>
                    </label>;
                  })}
                </div>
              </div>;
            })}
          </div>
          }
        </div>
        }
        <div className="modal inmodal animated fadeIn" id="wg_mark_test_modal" role="dialog" data-backdrop="static">
          <div className="modal-dialog modal-sm">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span className="sr-only">Close</span></button>
                <h4 className="modal-title">Confirmation</h4>
              </div>
              <div className="modal-body">
                <p>Are you sure mark the current Test?</p>
              </div>
              <div className="modal-footer">
                <button className="btn btn-white" data-dismiss="modal">Cancel</button>
                <button className="btn btn-primary btn-normalize" data-dismiss="modal" onClick={this._handleMarkTestConfirmClick}><i className="fa fa-check"></i> Yes</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ViewTest.propTypes = {
  request: React.PropTypes.object,
  __toggleLoader: React.PropTypes.func
};

export default ViewTest;
