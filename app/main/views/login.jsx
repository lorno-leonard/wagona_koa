/* globals _, window */
import React, {Component} from 'react';
import request from './../../_common/request';

class ViewLogin extends Component {
  constructor(props) {
    super(props);
    this._handleSubmit = this._handleSubmit.bind(this);
    this.state = {
      errorMessage: '',
      buttonDisabled: false
    };
  }

  _handleSubmit(event) {
    event.preventDefault();

    // Toggle Loader
    this.props.__toggleLoader(true);

    // Disable button
    this.setState({buttonDisabled: true});

    const self = this;
    const username = this.refs.username;
    const password = this.refs.password;
    const rememberMe = this.refs.remember_me;

    // Reset errorMessage
    self.setState({errorMessage: ''});

    // Trim
    $(username).val($(username).val().trim());

    if ($(username).val() === '') {
      return;
    }

    let credentials = {
      username: $(username).val(),
      password: $(password).val(),
      remember_me: $(rememberMe).is(':checked') // eslint-disable-line camelcase
    };

    request.call('POST', '/api/login', credentials, (err, res) => {
      // Toggle Loader
      this.props.__toggleLoader(false);

      if (err) {
        // Enable button
        self.setState({buttonDisabled: false});

        self.setState({errorMessage: res.body.message});
        return;
      }

      // Reload
      window.location.href = '/account';
    });
  }

  render() {
    const errorMessage = this.state.errorMessage;
    const buttonDisabled = this.state.buttonDisabled;
    return (
      <div className="row">
        <div className="col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
          <div className="ibox">
            <div className="ibox-content">
              <h2>Login Here</h2>
              {!_.isEmpty(errorMessage) &&
              <div className="alert alert-danger" ref="error-message">{errorMessage}</div>
              }
              <form className="wg_login_form" role="form" onSubmit={this._handleSubmit}>
                <div className="form-group">
                  <input type="text" ref="username" className="form-control" placeholder="Username/Email Address" required autoFocus />
                </div>
                <div className="form-group">
                  <input type="password" ref="password" className="form-control" placeholder="Password" required />
                </div>
                <div className="row">
                  <div className="col-xs-6 col-md-5">
                    <div className="checkbox checkbox-success">
                      <input type="checkbox" id="remember_me" ref="remember_me" />
                      <label htmlFor="remember_me"><a>Remember me</a></label>
                    </div>
                  </div>
                  <div className="col-xs-6 col-md-5 col-md-offset-2">
                    <div className="m-t-sm text-right"><a>Forgot my details</a></div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6 col-md-5 m-b-xs">
                    <button type="submit" ref="button" className="btn btn-primary btn-normalize btn-block" disabled={buttonDisabled}>Log in</button>
                  </div>
                  <div className="col-sm-6 col-md-5 col-md-offset-2">
                    <a href="/register" className="btn btn-danger btn-block">Register</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ViewLogin.propTypes = {
  __toggleLoader: React.PropTypes.func
};

export default ViewLogin;
