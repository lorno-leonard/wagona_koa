/* globals document, __ejs_layout */
import React from 'react';
import ReactDOM from 'react-dom';
import LayoutEmpty from './components/layouts/empty';
import LayoutMain from './components/layouts/main';
import LayoutTest from './components/layouts/test';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import 'animate.css/animate.min.css';
import 'parsleyjs/src/parsley.css';
import 'toastr/toastr.scss';
import 'slick-carousel/slick/slick.scss';
import 'slick-carousel/slick/slick-theme.scss';
import './../_common/awesome-bootstrap-checkbox.css';
import './../_common/inspinia.css';
import './style.scss';
import './../_common/inspinia.js';

let component = null;
switch (__ejs_layout) { // eslint-disable-line
  case 'main':
    component = <LayoutMain />;
    break;
  case 'test':
    component = <LayoutTest />;
    break;
  default:
    component = <LayoutEmpty />;
    break;
}

ReactDOM.render(
  component,
  document.getElementById('root')
);
