import React, {Component} from 'react';
import CmpBreadcrumbs from './../components/common/breadcrumbs';
import CmpTable from './../components/common/table';

class ViewQuestion extends Component {
  constructor(props) {
    super(props);
    this.state = {
      breadcrumbsProps: {
        title: 'Question',
        links: [{
          name: 'Dashboard',
          href: '/'
        }, {
          name: 'Question'
        }]
      },
      tableProps: {
        request: props.request,
        _handleToggleToastr: props._handleToggleToastr,
        _handleSignOutClick: props._handleSignOutClick,
        __ajaxErrorHandler__: props.__ajaxErrorHandler__,
        canAdd: true,
        canEdit: true,
        canSearch: true,
        hasStatus: true,
        hasPager: true,
        addHref: '/question/add',
        editHref: '/question/edit',
        addText: 'Add Question',
        apiGET: '/api/question',
        apiPATCH: '/api/question',
        columns: [{
          field: 'description',
          title: 'Description',
          striptags: true
        }, {
          field: 'type',
          title: 'Question Type',
          tag: true,
          tagDetails: [{
            value: 'CHOICE',
            class: 'label label-info',
            title: 'Multiple Choice'
          }, {
            value: 'FILL_IN',
            class: 'label label-success',
            title: 'Fill-in'
          }]
        }, {
          field: 'is_paid',
          title: 'Paid/Free',
          tag: true,
          tagDetails: [{
            value: 0,
            class: 'label label-info',
            title: 'Free'
          }, {
            value: 1,
            class: 'label label-warning',
            title: 'Paid'
          }]
        }]
      }
    };
  }

  render() {
    return (
      <div>
        {React.createElement(CmpBreadcrumbs, this.state.breadcrumbsProps)}
        <div className="wrapper wrapper-content animated fadeIn">
          <div className="row">
            <div className="col-lg-12">
              {React.createElement(CmpTable, this.state.tableProps)}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ViewQuestion.propTypes = {
  request: React.PropTypes.object,
  _handleToggleToastr: React.PropTypes.func,
  _handleSignOutClick: React.PropTypes.func,
  __ajaxErrorHandler__: React.PropTypes.func
};

export default ViewQuestion;
