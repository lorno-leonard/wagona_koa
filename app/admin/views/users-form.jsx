/* globals _ */
import React, {Component} from 'react';
import CmpBreadcrumbs from './../components/common/breadcrumbs';
import Role from './../../../src/lib/role';

class ViewUsersForm extends Component {
  constructor(props) {
    super(props);
    this.action = _.includes(props.location.pathname, 'add') ? 'add' : 'edit';
    this.recordId = null;

    let title = this.action === 'add' ? 'Add User' : 'Edit User';
    let breadcrumbsProps = {
      title: 'Users',
      links: [{
        name: 'Dashboard',
        href: '/'
      }, {
        name: 'Users',
        href: '/users'
      }]
    };
    breadcrumbsProps.links.push({
      name: title
    });

    this.state = {
      title: title,
      dataCountry: [],
      breadcrumbsProps: breadcrumbsProps
    };
  }

  _handleSubmit(event) {
    event.preventDefault();

    const self = this;
    const first_name = this.refs.first_name; // eslint-disable-line camelcase
    const last_name = this.refs.last_name; // eslint-disable-line camelcase
    const email = this.refs.email;
    const username = this.refs.username;
    const password = this.refs.password;
    const country = this.refs.country;
    const role = this.refs.role;
    const button = this.refs.button;
    const errorMessage = this.refs['error-message'];

    // Reset Error Message
    $(errorMessage).addClass('hide').text('');

    // Trim
    $(first_name).val($(first_name).val().trim());
    $(last_name).val($(last_name).val().trim());
    $(email).val($(email).val().trim());
    $(username).val($(username).val().trim());

    // Validate
    if (!$('#form').parsley().isValid()) {
      return;
    }

    let data = {
      first_name: $(first_name).val(), // eslint-disable-line camelcase
      last_name: $(last_name).val(), // eslint-disable-line camelcase
      email: $(email).val(),
      username: $(username).val(),
      password: $(password).val(),
      country_id: $(country).val(), // eslint-disable-line camelcase
      role: Number($(role).val())
    };
    if (this.action === 'edit') {
      data.id = this.recordId;
    }
    if (data.password === '') {
      delete data.password;
    }

    // Disable button
    $(button).prop('disabled', true);

    // Set method and API
    const method = this.action === 'add' ? 'POST' : 'PATCH';
    const api = this.action === 'add' ? '/api/users' : '/api/users/' + this.recordId;

    this.props.request.call(method, api, data, function(err, res) {
      // Enable button
      $(button).prop('disabled', false);

      if (err) {
        $(errorMessage).text(res.body.message).removeClass('hide');
        if (_.includes(res.body.message.toLowerCase(), 'username')) {
          $(username).addClass('parsley-error');
        }
        if (_.includes(res.body.message.toLowerCase(), 'email')) {
          $(email).addClass('parsley-error');
        }
        return;
      }

      const form = $('#form');
      form.parsley().reset();
      if (self.action === 'add') {
        form[0].reset();
      }

      const message = self.action === 'add' ? 'Created new user.' : 'Updated user <strong>' + data.username + '<strong>.';
      self.props._handleToggleToastr('success', 'Success!', message);
    });
  }

  __loadCountryData__() {
    const self = this;
    this.props.request.call('GET', '/api/country?status=1', null, function(err, res) {
      if (err) {
        self.props.__ajaxErrorHandler__(self, res);
        return;
      }

      self.setState({dataCountry: _.sortBy(res.body.data, 'name')});
    });
  }

  __loadUserData__() {
    const self = this;
    this.props.request.call('GET', '/api/users/' + self.props.params.id, null, function(err, res) {
      if (err) {
        self.props.__ajaxErrorHandler__(self, res);
        return;
      }

      const data = res.body.data;
      $(self.refs.first_name).val(data.first_name); // eslint-disable-line camelcase
      $(self.refs.last_name).val(data.last_name); // eslint-disable-line camelcase
      $(self.refs.email).val(data.email);
      $(self.refs.username).val(data.username);
      $(self.refs.country).val(data.country_id);
      $(self.refs.role).val(Role[data.role]);
      self.recordId = data.id;
      self.setState({title: 'Edit User - ' + data.username});
    });
  }

  componentDidMount() {
    $('#form').parsley();
    if (this.action === 'edit') {
      this.__loadUserData__();
    }
  }

  componentWillMount() {
    this.__loadCountryData__();
  }

  render() {
    const requiredPassword = this.action === 'add';
    return (
      <div>
        {React.createElement(CmpBreadcrumbs, this.state.breadcrumbsProps)}
        <div className="wrapper wrapper-content animated fadeIn">
          <div className="row">
            <div className="col-lg-8">
              <a href="#/users" className="btn btn-outline btn-primary m-b-xs"><i className="fa fa-hand-o-left"></i> Back</a>
              <div className="ibox">
                <div className="ibox-title">
                  <h5>{this.state.title}</h5>
                </div>
                <div className="ibox-content">
                  <div className="alert alert-danger hide" ref="error-message"></div>
                  <form id="form" onSubmit={this._handleSubmit.bind(this)}>
                    <div className="row">
                      <div className="col-lg-6">
                        <div className="form-group">
                          <label>Firstname <span className="text-danger">*</span></label>
                          <input type="text" name="first_name" ref="first_name" className="form-control" placeholder="Enter Firstname" autoComplete="off" required maxLength="50" />
                        </div>
                      </div>
                      <div className="col-lg-6">
                        <div className="form-group">
                          <label>Surname <span className="text-danger">*</span></label>
                          <input type="text" name="last_name" ref="last_name" className="form-control" placeholder="Enter Surname" autoComplete="off" required maxLength="50" />
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-6">
                        <div className="form-group">
                          <label>Username <span className="text-danger">*</span></label>
                          <input type="text" name="username" ref="username" className="form-control" placeholder="Enter Username" autoComplete="off" required maxLength="100" />
                        </div>
                      </div>
                      <div className="col-lg-6">
                        <div className="form-group">
                          <label>Email</label>
                          <input type="email" name="email" ref="email" className="form-control" placeholder="Enter Email Address" autoComplete="off" maxLength="100" />
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-6">
                        <div className="form-group">
                          <label>Password {this.action === 'add' && <span className="text-danger">*</span>}</label>
                          <input type="password" name="password" ref="password" id="frm-password" className="form-control" placeholder="Enter Password" required={requiredPassword} data-parsley-equalto="#frm-confirm-password" data-parsley-error-message="Passwords must be the same" />
                        </div>
                      </div>
                      <div className="col-lg-6">
                        <div className="form-group">
                          <label>Confirm Password {this.action === 'add' && <span className="text-danger">*</span>}</label>
                          <input type="password" name="confirm_password" ref="confirm_password" id="frm-confirm-password" className="form-control" placeholder="Enter Password again" required={requiredPassword} data-parsley-equalto="#frm-password" data-parsley-error-message="Passwords must be the same" />
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-6">
                        <div className="form-group">
                          <label>Country <span className="text-danger">*</span></label>
                          <select name="country" ref="country" id="" className="form-control" required>
                            <option value="">-- Select Country --</option>
                            {this.state.dataCountry.map((value, index) => {
                              return <option key={index} value={value.id}>{value.name}</option>;
                            })}
                          </select>
                        </div>
                      </div>
                      <div className="col-lg-6">
                        <div className="form-group">
                          <label>User Role <span className="text-danger">*</span></label>
                          <select name="role" ref="role" id="" className="form-control" required>
                            <option value="">-- Select User Role --</option>
                            {Object.keys(Role).map((key, index) => {
                              return <option key={index} value={Role[key]}>{key}</option>;
                            })}
                          </select>
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12">
                        <button ref="button" className="btn btn-primary pull-right" type="submit"><i className="fa fa-check"></i> Submit</button>
                        <a href="#/users" className="btn btn-white pull-right m-r-xs"><i className="fa fa-ban"></i> Cancel</a>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ViewUsersForm.propTypes = {
  location: React.PropTypes.object.isRequired,
  request: React.PropTypes.object
};

export default ViewUsersForm;
