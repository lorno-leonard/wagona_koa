/* globals _, window, document, tinymce, com */
import React, {Component} from 'react';
import TinyMCE from 'react-tinymce';
import CmpBreadcrumbs from './../components/common/breadcrumbs';
import asyncSeries from 'async/series';

class ViewQuestionForm extends Component {
  constructor(props) {
    super(props);
    this._handleQuestionTypeChange = this._handleQuestionTypeChange.bind(this);
    this._handleQuestionPaidFreeChange = this._handleQuestionPaidFreeChange.bind(this);
    this._handleSaveChoice = this._handleSaveChoice.bind(this);
    this._handleCancelUpdateChoice = this._handleCancelUpdateChoice.bind(this);
    this._handleWirisEditorCopyTo = this._handleWirisEditorCopyTo.bind(this);
    this._handleUpdateQuestionState = this._handleUpdateQuestionState.bind(this);
    this._handleSubmit = this._handleSubmit.bind(this);
    this.action = _.includes(props.location.pathname, 'add') ? 'add' : 'edit';
    this.recordId = null;
    this.editorDescId = 'editor-tinymce-desc';
    this.editorAnsId = 'editor-tinymce-ans';
    this.editorExpId = 'editor-tinymce-exp';
    this.wEditorQuestion = null;
    this.wEditorQuestionCopy = null;
    this.wEditorPreview = null;
    this.minimumNumChoices = 3;
    this.errorMessage = '';
    this.emptyMathML = '<math xmlns="http://www.w3.org/1998/Math/MathML"/>';

    let title = this.action === 'add' ? 'Add Question' : 'Edit Question';
    let breadcrumbsProps = {
      title: 'Question',
      links: [{
        name: 'Dashboard',
        href: '/'
      }, {
        name: 'Question',
        href: '/question'
      }]
    };
    breadcrumbsProps.links.push({
      name: title
    });

    this.state = {
      title: title,
      breadcrumbsProps: breadcrumbsProps,
      question: {
        type: 'CHOICE',
        tags: [],
        is_paid: 0, // eslint-disable-line camelcase
        description: '',
        choices: [],
        actionChoice: 'add',
        currentEditIndex: -1,
        correctIndex: -1,
        fill_in: { // eslint-disable-line camelcase
          question: '',
          answer: ''
        },
        explanation: ''
      }
    };
  }

  _handleQuestionTypeChange(event) {
    const question = this.state.question;
    question.type = event.target.value;
    this.setState({question: question});
    this._handleUpdateQuestionState();
  }

  _handleQuestionPaidFreeChange(event) {
    const question = this.state.question;
    question.is_paid = Number(event.target.value); // eslint-disable-line camelcase
    this.setState({question: question});
    this._handleUpdateQuestionState();
  }

  _handleQuestionChoiceChange(index) {
    const question = this.state.question;
    question.correctIndex = index;
    _.each(question.choices, value => {
      value.correct = false;
    });
    question.choices[index].correct = true;
    this.setState({question: question});
  }

  _handleSaveChoice() {
    const editor = tinymce.get(this.editorAnsId);
    const question = this.state.question;

    if (question.actionChoice === 'add') {
      const obj = {
        description: editor.getContent(),
        body: editor.getBody().innerHTML.replace('<br data-mce-bogus="1">', ''),
        correct: false
      };
      question.choices.push(obj);
    } else if (question.actionChoice === 'edit') {
      const currentEditIndex = question.currentEditIndex;
      const obj = question.choices[currentEditIndex];
      obj.description = editor.getContent();
      obj.body = editor.getBody().innerHTML;
      question.choices[currentEditIndex] = obj;
      question.currentEditIndex = -1;
      question.actionChoice = 'add';
    }

    this.setState({question: question});

    // Clear editor
    editor.setContent('');
  }

  _handleCancelUpdateChoice() {
    const editor = tinymce.get(this.editorAnsId);
    const question = this.state.question;
    question.actionChoice = 'add';
    question.currentEditIndex = -1;
    this.setState({question: question});

    // Clear editor
    editor.setContent('');
  }

  _handleEditChoice(index) {
    const editor = tinymce.get(this.editorAnsId);
    const question = this.state.question;
    const obj = question.choices[index];
    question.actionChoice = 'edit';
    question.currentEditIndex = index;
    this.setState({question: question});

    // Update Editor
    editor.setContent(obj.description);
  }

  _handleRemoveChoice(index) {
    const question = this.state.question;
    question.choices.splice(index, 1);
    question.correctIndex = question.correctIndex >= index ? -1 : question.correctIndex;
    this.setState({question: question});
  }

  _handleWirisEditorCopyTo() {
    let questionMathML = this.wEditorQuestion.getMathML();
    questionMathML
      .replace('<math', '<math wrs:positionable="false"')
      .replace(/<mrow\/>/g, '<mrow wrs:positionable="true"/>');
    this.wEditorQuestionCopy.setMathML(questionMathML);
  }

  _handleUpdateQuestionState() {
    const question = this.state.question;
    const editorDesc = tinymce.get(this.editorDescId);
    const editorExp = tinymce.get(this.editorExpId);

    question.description = editorDesc.getBody().innerHTML.replace('<br data-mce-bogus="1">', '');
    question.explanation = editorExp.getBody().innerHTML.replace('<br data-mce-bogus="1">', '');

    // Check if Question type is FILL_IN, then update fill_in property
    if (question.type === 'FILL_IN') {
      question.fill_in.question = this.wEditorQuestion.getMathML();
      question.fill_in.answer = this.wEditorQuestionCopy.getMathML();
    }

    // Set tags
    question.tags = $('.tagsinput').tagsinput('items');

    this.setState({question: question});

    // Set MathML
    if (question.fill_in.question !== '') {
      this.wEditorQuestion.setMathML(question.fill_in.question);
      this.wEditorPreview.setMathML(question.fill_in.question);
    }
    if (question.fill_in.answer !== '') {
      this.wEditorQuestionCopy.setMathML(question.fill_in.answer);
    }
  }

  _handleSubmit() {
    this._handleUpdateQuestionState();
    const self = this;
    const valid = this.__validate__();
    let question = this.state.question;

    if (!valid) {
      return;
    }

    let data = _.merge({}, _.pick(question, [
      'description',
      'explanation',
      'type',
      'is_paid',
      'tags'
    ]), {
      correct: question.type === 'CHOICE' ? question.correctIndex : 1,
      choices: question.type === 'CHOICE' ? question.choices.map(value => {
        return value.body;
      }) : [
        question.fill_in.question,
        question.fill_in.answer
      ]
    });
    if (this.action === 'edit') {
      data.id = this.recordId;
    }

    // Set method and API
    const method = this.action === 'add' ? 'POST' : 'PATCH';
    const api = this.action === 'add' ? '/api/question' : '/api/question/' + this.recordId;

    this.props.request.call(method, api, data, function(err, res) {
      if (err) {
        this.errorMessage = res.body.message;
        return;
      }

      // Reset
      if (self.action === 'add') {
        question = {
          type: 'CHOICE',
          tags: [],
          is_paid: 0, // eslint-disable-line camelcase
          description: '',
          choices: [],
          actionChoice: 'add',
          currentEditIndex: -1,
          correctIndex: -1,
          fill_in: { // eslint-disable-line camelcase
            question: '',
            answer: ''
          },
          explanation: ''
        };
        self.setState({question: question});
        tinymce.get(self.editorDescId).setContent('');
        tinymce.get(self.editorAnsId).setContent('');
        tinymce.get(self.editorExpId).setContent('');
        self.wEditorQuestion.setMathML(self.emptyMathML);
        self.wEditorQuestionCopy.setMathML(self.emptyMathML);
        self.wEditorPreview.setMathML(self.emptyMathML);
        $('#tab-ans a[data-trigger="tab"]:first-child').trigger('click'); // Clicked previous button of Answer/Choice tab to redirect to Description tab
        $('.tagsinput').tagsinput('removeAll'); // Clear tags input
      } else if (self.action === 'edit') {
        $('#tab-ans a[data-trigger="tab"]:first-child').trigger('click'); // Clicked previous button of Answer/Choice tab to redirect to Description tab
      }

      const message = self.action === 'add' ? 'Created new question.' : 'Updated question';
      self.props._handleToggleToastr('success', 'Success!', message);
    });
  }

  __validate__() {
    const question = this.state.question;
    let valid = true;
    let errors = [];
    this.errorMessage = '';

    // Check if Description is Empty
    if (question.description === '') {
      errors.push('Description is required.');
      valid = false;
    }

    if (question.type === 'CHOICE') {
      // Check if Choices are empty
      if (question.choices.length === 0) {
        errors.push('Choices are required.');
        valid = false;
      }

      // Check if number of Choices is less than minimumNumChoices
      if (question.choices.length > 0 && question.choices.length < 3) {
        errors.push('Please add more choices. Minimum of ' + this.minimumNumChoices + '.');
        valid = false;
      }

      // Check if there is no chosen correct answer
      if (question.choices.length > 0 && question.correctIndex === -1) {
        errors.push('Please select a correct answer choice.');
        valid = false;
      }
    }

    if (question.type === 'FILL_IN') {
      // Check if Question is empty
      if (_.isEqual(question.fill_in.question, this.emptyMathML)) {
        errors.push('Fill in formula is required.');
        valid = false;
      }

      // Check if Correct answer is empty
      if (_.isEqual(question.fill_in.answer, this.emptyMathML)) {
        errors.push('Correct fill in formula is required.');
        valid = false;
      }
    }

    // Check if Explanation is Empty
    if (question.explanation === '') {
      errors.push('Explanation is required.');
      valid = false;
    }

    let errorMessage = '';
    if (errors.length > 0) {
      errorMessage += '<ul>';
      errors.map(value => {
        errorMessage += '<li>' + value + '</li>';
        return value;
      });
      errorMessage += '</ul>';
      this.errorMessage = errorMessage;
    }

    return valid;
  }

  __loadQuestionData__() {
    const self = this;
    asyncSeries([
      callback => {
        self.props.request.call('GET', '/api/question/' + self.props.params.id, null, function(err, res) {
          if (err) {
            self.props.__ajaxErrorHandler__(self, res);
            callback(res);
            return;
          }

          const data = res.body.data;
          self.recordId = data.id;
          callback(null, data);
        });
      },
      callback => {
        self.props.request.call('GET', '/api/question/' + self.props.params.id + '/choice', null, function(err, res) {
          if (err) {
            self.props.__ajaxErrorHandler__(self, res);
            callback(res);
            return;
          }

          const data = res.body.data;
          callback(null, data);
        });
      }
    ],
    function(err, results) {
      if (err) {
        console.log(err);
        return;
      }

      let questionDetails = results[0];
      let choiceDetails = results[1];
      const question = self.state.question;
      question.type = questionDetails.type;
      question.tags = questionDetails.tags;
      question.is_paid = questionDetails.is_paid; // eslint-disable-line camelcase
      question.description = questionDetails.description;
      question.explanation = questionDetails.explanation;
      question.correctIndex = questionDetails.correct;
      if (question.type === 'CHOICE') {
        question.choices = choiceDetails.map(value => {
          return {
            description: value.description,
            index: value.index,
            body: value.description
          };
        });
      } else if (question.type === 'FILL_IN') {
        question.fill_in.question = choiceDetails[0].description;
        question.fill_in.answer = choiceDetails[1].description;
      }
      self.setState({question: question});

      _.delay(() => {
        const question = self.state.question;
        _.each(question.tags, value => {
          $('.tagsinput').tagsinput('add', value);
        });
        const editorDesc = tinymce.get(self.editorDescId);
        const editorExp = tinymce.get(self.editorExpId);
        editorDesc.setContent(question.description);
        editorExp.setContent(question.explanation);
        if (question.type === 'FILL_IN') {
          self.wEditorQuestion.setMathML(question.fill_in.question);
          self.wEditorPreview.setMathML(question.fill_in.question);
          self.wEditorQuestionCopy.setMathML(question.fill_in.answer);
        }
      }, 2000);
    });
  }

  componentDidMount() {
    // Load question data
    if (this.action === 'edit') {
      this.__loadQuestionData__();
    }

    // Tab trigger
    $('[data-trigger="tab"]').click(function(e) {
      var href = $(this).attr('href');
      e.preventDefault();
      $('[data-toggle="tab"][href="' + href + '"]').trigger('click');
    });

    // Initialize Tags input
    $('.tagsinput').tagsinput({
      maxTags: 10,
      trimValue: true
    });

    // Initialize Wiris Editor
    const wEditorOpts = {
      language: 'en',
      toolbar: `<toolbar ref="chemistry" removeLinks="true">
        <removeTab ref="chemistry" />
        <removeTab ref="greek" />
        <removeTab ref="contextual" />
        <tab name="scriptsAndLayout">
          <section position="1">
            <item ref="mixedNumber"/>
          </section>
        </tab>
        <tab name="Custom">
          <section>
            <createButton icon="${process.env.BASE_URL}/admin/custom_empty.png" title="Empty" >
              <content>
                <maction><mrow/></maction>
              </content>
            </createButton>
          </section>
          <section>
            <item ref="bold"/>
            <item ref="italic"/>
            <item ref="autoItalic"/>
            <item ref="setColor"/>
            <item ref="mtext"/>
          </section>
          <section>
            <item ref="rtl"/>
            <item ref="forceLigature"/>
          </section>
          <section>
            <item ref="setFontFamily"/>
            <item ref="setFontSize"/>
          </section>
        </tab>
      </toolbar>`
    };
    const wEditorOptsHidden = {
      language: 'en',
      toolbarHidden: true
    };
    this.wEditorQuestion = com.wiris.jsEditor.JsEditor.newInstance(wEditorOpts);
    this.wEditorQuestionCopy = com.wiris.jsEditor.JsEditor.newInstance(wEditorOptsHidden);
    this.wEditorPreview = com.wiris.jsEditor.JsEditor.newInstance(wEditorOptsHidden);
    this.wEditorQuestion.insertInto(document.getElementById('editor_question'));
    this.wEditorQuestionCopy.insertInto(document.getElementById('editor_question_copy'));
    this.wEditorPreview.insertInto(document.getElementById('editor_preview'));
  }

  componentWillMount() {
    window.addEventListener('message', function(event) {
      let data = event.data;
      if (_.includes(data, 'wagona_upload|')) {
        let dataArr = data.split('|');
        tinymce.activeEditor.insertContent('<img src="' + dataArr[1] + '">');
      }
    }, false);
  }

  render() {
    const question = this.state.question;
    return (
      <div>
        {React.createElement(CmpBreadcrumbs, this.state.breadcrumbsProps)}
        <div className="wrapper wrapper-content animated fadeIn">
          <div className="row">
            <div className="col-lg-12">
              <a href="#/question" className="btn btn-outline btn-primary m-b-xs"><i className="fa fa-hand-o-left"></i> Back</a>
              <div className="ibox border-bottom">
                <div className="ibox-content">
                  {this.errorMessage !== '' &&
                    <div className="alert alert-danger" ref="error-message" dangerouslySetInnerHTML={{__html: this.errorMessage}}></div>
                  }
                  <div className="row">
                    <div className="col-lg-4">
                      <label className="m-r-xs">Question Type</label>
                      <div className="radio radio-info radio-inline">
                        <input type="radio" id="radio-qt-choice" ref="question_type" name="question_type" value="CHOICE" checked={question.type === 'CHOICE'} disabled={this.action === 'edit'} onChange={this._handleQuestionTypeChange} />
                        <label htmlFor="radio-qt-choice"> Multiple Choice </label>
                      </div>
                      <div className="radio radio-primary radio-inline">
                        <input type="radio" id="radio-qt-fillin" ref="question_type" name="question_type" value="FILL_IN" checked={question.type === 'FILL_IN'} disabled={this.action === 'edit'} onChange={this._handleQuestionTypeChange} />
                        <label htmlFor="radio-qt-fillin"> Fill-in </label>
                      </div>
                    </div>
                    <div className="col-lg-5">
                      <label className="m-r-xs">Tags</label>
                      <input type="text" className="form-control tagsinput" ref="tags" />
                    </div>
                    <div className="col-lg-3">
                      <label className="m-r-xs">Free/Paid</label>
                      <div className="radio radio-info radio-inline">
                        <input type="radio" id="radio-pf-free" ref="question_paid_free" name="question_paid_free" value="0" checked={question.is_paid === 0} onChange={this._handleQuestionPaidFreeChange} />
                        <label htmlFor="radio-pf-free"> Free </label>
                      </div>
                      <div className="radio radio-warning radio-inline">
                        <input type="radio" id="radio-pf-paid" ref="question_paid_free" name="question_paid_free" value="1" checked={question.is_paid === 1} onChange={this._handleQuestionPaidFreeChange} />
                        <label htmlFor="radio-pf-paid"> Paid </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="tabs-container animated fadeIn">
                <ul className="nav nav-tabs">
                  <li className="active"><a data-toggle="tab" href="#tab-desc" onClick={this._handleUpdateQuestionState}><i className="fa fa-align-left"></i> Description</a></li>
                  <li><a data-toggle="tab" href="#tab-ans" onClick={this._handleUpdateQuestionState}><i className="fa fa-check-circle-o"></i> Answer/Choice</a></li>
                  <li><a data-toggle="tab" href="#tab-exp" onClick={this._handleUpdateQuestionState}><i className="fa fa-comment-o"></i> Explanation</a></li>
                  <li><a data-toggle="tab" href="#tab-view" onClick={this._handleUpdateQuestionState}><i className="fa fa-file-text-o"></i> Preview</a></li>
                </ul>
                <div className="tab-content">
                  <div id="tab-desc" className="tab-pane active">
                    <div className="panel-body animated fadeIn">
                      <div className="row m-b-xs">
                        <div className="col-lg-12">
                          <a data-trigger="tab" href="#tab-ans" className="btn btn-sm btn-outline btn-primary btn-rounded pull-right" onClick={this._handleUpdateQuestionState}>Next <i className="fa fa-long-arrow-right"></i></a>
                        </div>
                      </div>
                      <TinyMCE
                        id={this.editorDescId}
                        content=""
                        config={{
                          forced_root_block: '', // eslint-disable-line camelcase
                          plugins: 'link image code',
                          toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code | jbimages | tiny_mce_wiris_formulaEditor',
                          min_height: 200, // eslint-disable-line camelcase
                          external_plugins: { // eslint-disable-line camelcase
                            tiny_mce_wiris: process.env.WIRIS_PLUGIN_URL, // eslint-disable-line camelcase
                            jbimages: process.env.JBIMAGES_PLUGIN_URL
                          }
                        }}
                      />
                    </div>
                  </div>
                  <div id="tab-ans" className="tab-pane">
                    <div className="panel-body animated fadeIn">
                      <div className="row m-b-xs">
                        <div className="col-lg-12">
                          <a data-trigger="tab" href="#tab-desc" className="btn btn-sm btn-outline btn-primary btn-rounded"><i className="fa fa-long-arrow-left" onClick={this._handleUpdateQuestionState}></i> Prev</a>
                          <a data-trigger="tab" href="#tab-exp" className="btn btn-sm btn-outline btn-primary btn-rounded pull-right" onClick={this._handleUpdateQuestionState}>Next <i className="fa fa-long-arrow-right"></i></a>
                        </div>
                      </div>
                      <div className={`row ${question.type === 'CHOICE' ? '' : 'hide'}`}>
                        <div className="col-lg-6">
                          <TinyMCE
                            id={this.editorAnsId}
                            content=""
                            config={{
                              forced_root_block: '', // eslint-disable-line camelcase
                              plugins: 'link image code',
                              toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code | jbimages | tiny_mce_wiris_formulaEditor',
                              min_height: 200, // eslint-disable-line camelcase
                              external_plugins: { // eslint-disable-line camelcase
                                tiny_mce_wiris: process.env.WIRIS_PLUGIN_URL, // eslint-disable-line camelcase
                                jbimages: process.env.JBIMAGES_PLUGIN_URL
                              }
                            }}
                          />
                          {question.actionChoice === 'edit' && <button className="btn btn-sm btn-default m-t-xs" onClick={this._handleCancelUpdateChoice}>Cancel</button>}
                          <button className="btn btn-sm btn-primary m-t-xs pull-right" onClick={this._handleSaveChoice}>{question.actionChoice === 'add' ? 'Add' : 'Update'} Choice</button>
                        </div>
                        <div className="col-lg-6">
                          {question.choices.length < this.minimumNumChoices &&
                          <p>Please add choices. Minimum of {this.minimumNumChoices}.</p>
                          }
                          {question.choices.length > 0 &&
                            question.choices.map((value, index) => {
                              const choiceClass = question.correctIndex === index ? 'bg-success' : 'border-top-bottom border-left-right border-size-sm';
                              const choiceEditClass = question.currentEditIndex === index ? 'bg-info' : '';
                              return <div key={index} className={`row p-xs b-r-xl m-b-xs m-l-none m-r-none ${choiceClass} ${choiceEditClass}`}>
                                <div className="col-lg-9">
                                  <div className="radio radio-success radio-inline m-t-none m-b-none">
                                    <input type="radio" id={`question_choice-${index}`} ref="question_choice" name="question_choice" value={index} checked={question.correctIndex === index} onChange={this._handleQuestionChoiceChange.bind(this, index)} />
                                    <label htmlFor={`question_choice-${index}`} dangerouslySetInnerHTML={{__html: value.body}}></label>
                                  </div>
                                </div>
                                <div className="col-lg-3 text-right">
                                  <button className="btn btn-info btn-circle" onClick={this._handleEditChoice.bind(this, index)}><i className="fa fa-pencil"></i></button>
                                  <button className="btn btn-danger btn-circle m-l-xs" onClick={this._handleRemoveChoice.bind(this, index)}><i className="fa fa-times"></i></button>
                                </div>
                              </div>;
                            })
                          }
                        </div>
                      </div>
                      <div className={`row ${question.type === 'FILL_IN' ? '' : 'hide'}`}>
                        <div className="col-lg-6">
                          <h3>Fill-in</h3>
                          <div id="editor_question" style={{height: 250}}></div>
                          <button className="btn btn-sm btn-primary m-t-xs" onClick={this._handleWirisEditorCopyTo}>Copy to Correct</button>
                        </div>
                        <div className="col-lg-6">
                          <h3>Correct answer</h3>
                          <div id="editor_question_copy" style={{height: 250}}></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="tab-exp" className="tab-pane">
                    <div className="panel-body animated fadeIn">
                      <div className="row m-b-xs">
                        <div className="col-lg-12">
                          <a data-trigger="tab" href="#tab-ans" className="btn btn-sm btn-outline btn-primary btn-rounded"><i className="fa fa-long-arrow-left" onClick={this._handleUpdateQuestionState}></i> Prev</a>
                          <a data-trigger="tab" href="#tab-view" className="btn btn-sm btn-outline btn-primary btn-rounded pull-right" onClick={this._handleUpdateQuestionState}>Next <i className="fa fa-long-arrow-right"></i></a>
                        </div>
                      </div>
                      <TinyMCE
                        id={this.editorExpId}
                        content=""
                        config={{
                          forced_root_block: '', // eslint-disable-line camelcase
                          plugins: 'link image code',
                          toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code | jbimages | tiny_mce_wiris_formulaEditor',
                          min_height: 200, // eslint-disable-line camelcase
                          external_plugins: { // eslint-disable-line camelcase
                            tiny_mce_wiris: process.env.WIRIS_PLUGIN_URL, // eslint-disable-line camelcase
                            jbimages: process.env.JBIMAGES_PLUGIN_URL
                          }
                        }}
                      />
                    </div>
                  </div>
                  <div id="tab-view" className="tab-pane">
                    <div className="panel-body animated fadeIn">
                      <div className="row m-b-xs">
                        <div className="col-lg-12">
                          <a data-trigger="tab" href="#tab-exp" className="btn btn-sm btn-outline btn-primary btn-rounded"><i className="fa fa-long-arrow-left" onClick={this._handleUpdateQuestionState}></i> Prev</a>
                          <a className="btn btn-sm btn-outline btn-primary btn-rounded pull-right" onClick={this._handleSubmit}>Submit <i className="fa fa-check-circle"></i></a>
                        </div>
                      </div>
                      <div className="row m-t-md">
                        <div className="col-lg-6">
                          {question.description === '' &&
                            <p>Please <a data-trigger="tab" href="#tab-desc">add a question description</a>.</p>
                          }
                          {question.description !== '' &&
                            <p dangerouslySetInnerHTML={{__html: question.description}}></p>
                          }
                        </div>
                        <div className={`col-lg-6 ${question.type === 'CHOICE' ? '' : 'hide'}`}>
                          {question.choices.length < this.minimumNumChoices &&
                          <p>Please <a data-trigger="tab" href="#tab-ans">add more choices</a>. Minimum of {this.minimumNumChoices}.</p>
                          }
                          {question.choices.length > 0 &&
                            question.choices.map((value, index) => {
                              // const choiceClass = choiceSelectedValue === index ? 'bg-success' : 'border-top-bottom border-left-right border-size-sm';
                              const choiceClass = 'border-top-bottom border-left-right border-size-sm';
                              return <label key={index} className={`row p-xs b-r-xl m-b-xs m-l-none m-r-none full-width ${choiceClass}`} htmlFor={`question_choice-preview${index}`}>
                                <div className="col-lg-12">
                                  <div className="radio radio-success radio-inline m-t-none m-b-none">
                                    <input type="radio" id={`question_choice-preview${index}`} name="question_choice_preview" value={index} />
                                    <label htmlFor={`question_choice-preview${index}`} dangerouslySetInnerHTML={{__html: value.body}}></label>
                                  </div>
                                </div>
                              </label>;
                            })
                          }
                        </div>
                        <div className={`col-lg-6 ${question.type === 'FILL_IN' ? '' : 'hide'}`}>
                          <div id="editor_preview" style={{height: 250}}></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ViewQuestionForm.propTypes = {
  location: React.PropTypes.object.isRequired,
  request: React.PropTypes.object
};

export default ViewQuestionForm;
