import React, {Component} from 'react';
import CmpBreadcrumbs from './../components/common/breadcrumbs';
import CmpTable from './../components/common/table';

class ViewUsers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      breadcrumbsProps: {
        title: 'Users',
        links: [{
          name: 'Dashboard',
          href: '/'
        }, {
          name: 'Users'
        }]
      },
      tableProps: {
        request: props.request,
        _handleToggleToastr: props._handleToggleToastr,
        _handleSignOutClick: props._handleSignOutClick,
        __ajaxErrorHandler__: props.__ajaxErrorHandler__,
        canAdd: true,
        canEdit: true,
        canSearch: true,
        hasStatus: true,
        hasPager: true,
        addHref: '/users/add',
        editHref: '/users/edit',
        addText: 'Add User',
        apiGET: '/api/users',
        apiPATCH: '/api/users',
        columns: [{
          field: 'username',
          title: 'Username'
        }, {
          field: 'first_name',
          title: 'Firstname'
        }, {
          field: 'last_name',
          title: 'Surname'
        }, {
          field: 'role',
          title: 'User Role'
        }]
      }
    };
  }

  render() {
    return (
      <div>
        {React.createElement(CmpBreadcrumbs, this.state.breadcrumbsProps)}
        <div className="wrapper wrapper-content animated fadeIn">
          <div className="row">
            <div className="col-lg-12">
              {React.createElement(CmpTable, this.state.tableProps)}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ViewUsers.propTypes = {
  request: React.PropTypes.object,
  _handleToggleToastr: React.PropTypes.func,
  _handleSignOutClick: React.PropTypes.func,
  __ajaxErrorHandler__: React.PropTypes.func
};

export default ViewUsers;
