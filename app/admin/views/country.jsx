import React, {Component} from 'react';
import CmpBreadcrumbs from './../components/common/breadcrumbs';
import CmpTable from './../components/common/table';

class ViewCountry extends Component {
  constructor(props) {
    super(props);
    this.state = {
      breadcrumbsProps: {
        title: 'Country',
        links: [{
          name: 'Dashboard',
          href: '/'
        }, {
          name: 'Country'
        }]
      },
      tableProps: {
        request: props.request,
        _handleToggleToastr: props._handleToggleToastr,
        _handleSignOutClick: props._handleSignOutClick,
        __ajaxErrorHandler__: props.__ajaxErrorHandler__,
        canSearch: true,
        hasStatus: true,
        hasPager: true,
        apiGET: '/api/country',
        apiPATCH: '/api/country',
        columns: [{
          field: 'id',
          title: 'Country ID'
        }, {
          field: 'name',
          title: 'Name'
        }]
      }
    };
  }

  render() {
    return (
      <div>
        {React.createElement(CmpBreadcrumbs, this.state.breadcrumbsProps)}
        <div className="wrapper wrapper-content animated fadeIn">
          <div className="row">
            <div className="col-lg-12">
              {React.createElement(CmpTable, this.state.tableProps)}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ViewCountry.propTypes = {
  request: React.PropTypes.object,
  _handleToggleToastr: React.PropTypes.func,
  _handleSignOutClick: React.PropTypes.func,
  __ajaxErrorHandler__: React.PropTypes.func
};

export default ViewCountry;
