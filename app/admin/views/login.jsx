import React, {Component} from 'react';
import auth from './../../_common/auth';

class ViewLogin extends Component {
  _handleSignIn(event) {
    event.preventDefault();

    const self = this;
    const username = this.refs.username;
    const password = this.refs.password;
    const button = this.refs.button;
    const errorMessage = this.refs['error-message'];

    // Reset Error Message
    $(errorMessage).addClass('hide').text('');

    // Trim
    $(username).val($(username).val().trim());

    if ($(username).val() === '') {
      return;
    }

    // Disable button
    $(button).prop('disabled', true);

    let credentials = {
      username: $(username).val(),
      password: $(password).val()
    };

    auth.signIn(credentials, self, (err, res) => {
      // Enable button
      $(button).prop('disabled', false);

      if (err) {
        $(errorMessage).text(res.body.message).removeClass('hide');
      }
    });
  }

  render() {
    return (
      <div className="middle-box text-center loginscreen animated fadeIn">
        <h2 className="text-white">Wagona Admin</h2>
        <p className="text-white">Please Sign in to proceed.</p>
        <div className="alert alert-danger hide" ref="error-message"></div>
        <form className="m-t" role="form" onSubmit={this._handleSignIn.bind(this)}>
          <div className="form-group">
            <input type="text" ref="username" className="form-control" placeholder="Username/Email Address" required autoFocus />
          </div>
          <div className="form-group">
            <input type="password" ref="password" className="form-control" placeholder="Password" required />
          </div>
          <button type="submit" ref="button" className="btn btn-primary block full-width m-b">Sign in</button>
          <p className="text-white">Go back to <a href="/" className="text-navy">Wagona Maths</a>.</p>
        </form>
      </div>
    );
  }
}

export default ViewLogin;
