import React, {Component} from 'react';
import CmpBreadcrumbs from './../components/common/breadcrumbs';
import CmpTable from './../components/common/table';

class ViewSyllabi extends Component {
  constructor(props) {
    super(props);
    this.state = {
      breadcrumbsProps: {
        title: 'Syllabi',
        links: [{
          name: 'Dashboard',
          href: '/'
        }, {
          name: 'Syllabi'
        }]
      },
      tableProps: {
        request: props.request,
        _handleToggleToastr: props._handleToggleToastr,
        _handleSignOutClick: props._handleSignOutClick,
        __ajaxErrorHandler__: props.__ajaxErrorHandler__,
        canAdd: true,
        canEdit: true,
        canSearch: true,
        hasStatus: true,
        hasPager: true,
        addHref: '/syllabi/add',
        editHref: '/syllabi/edit',
        addText: 'Add Syllabi',
        editText: 'Edit Syllabi',
        apiGET: '/api/syllabi',
        apiPATCH: '/api/syllabi',
        columns: [{
          field: 'name',
          title: 'Name'
        }]
      }
    };
  }

  render() {
    return (
      <div>
        {React.createElement(CmpBreadcrumbs, this.state.breadcrumbsProps)}
        <div className="wrapper wrapper-content animated fadeIn">
          <div className="row">
            <div className="col-lg-12">
              {React.createElement(CmpTable, this.state.tableProps)}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ViewSyllabi.propTypes = {
  request: React.PropTypes.object,
  _handleToggleToastr: React.PropTypes.func,
  _handleSignOutClick: React.PropTypes.func,
  __ajaxErrorHandler__: React.PropTypes.func
};

export default ViewSyllabi;
