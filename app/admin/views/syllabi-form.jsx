/* globals _ */
import React, {Component} from 'react';
import CmpBreadcrumbs from './../components/common/breadcrumbs';
import DualListBox from 'react-dual-listbox';
import asyncSeries from 'async/series';
import asyncAuto from 'async/auto';

class ViewSyllabiForm extends Component {
  constructor(props) {
    super(props);
    this.action = _.includes(props.location.pathname, 'add') ? 'add' : 'edit';
    this.recordId = null;

    let title = this.action === 'add' ? 'Add Syllabi' : 'Edit Syllabi';
    let breadcrumbsProps = {
      title: 'Syllabi',
      links: [{
        name: 'Dashboard',
        href: '/'
      }, {
        name: 'Syllabi',
        href: '/syllabi'
      }]
    };
    breadcrumbsProps.links.push({
      name: title
    });

    this.state = {
      title: title,
      breadcrumbsProps: breadcrumbsProps,
      subjectOptions: [],
      selectedSubjects: []
    };
  }

  _handleDualListBoxChange(selected) {
    this.setState({selectedSubjects: selected});
  }

  _handleSubmit(event) {
    event.preventDefault();

    const self = this;
    const name = this.refs.name;
    const button = this.refs.button;
    const errorMessage = this.refs['error-message'];

    // Reset Error Message
    $(errorMessage).addClass('hide').text('');

    // Trim
    $(name).val($(name).val().trim());

    // Validate
    if (!$('#form').parsley().isValid()) {
      return;
    }

    let data = {
      name: $(name).val()
    };
    if (this.action === 'edit') {
      data.id = this.recordId;
    }

    // Disable button
    $(button).prop('disabled', true);

    // Set method and API
    const method = this.action === 'add' ? 'POST' : 'PATCH';
    const api = this.action === 'add' ? '/api/syllabi' : '/api/syllabi/' + this.recordId;

    asyncAuto({
      syllabi: callback => {
        self.props.request.call(method, api, data, (err, res) => {
          if (err) {
            $(errorMessage).text(res.body.message).removeClass('hide');
            if (_.includes(res.body.message.toLowerCase(), 'syllabi name')) {
              $(name).addClass('parsley-error');
            }
            callback(res);
            return;
          }

          const id = self.action === 'add' ? res.body.insertId : self.recordId;
          callback(null, {id});
        });
      },
      addSubjects: ['syllabi', (results, callback) => {
        const subjectData = {
          subjects: self.state.selectedSubjects
        };
        self.props.request.call('POST', '/api/syllabi/' + results.syllabi.id + '/subject', subjectData, (err, res) => {
          if (err) {
            self.props.__ajaxErrorHandler__(self, res);
            callback(res);
            return;
          }
          callback(null);
        });
      }]
    }, (err, results) => {
      // Enable button
      $(button).prop('disabled', false);

      if (err) {
        console.log(err);
        return;
      }

      const form = $('#form');
      form.parsley().reset();
      if (self.action === 'add') {
        form[0].reset();
      }

      const message = self.action === 'add' ? 'Created new syllabi.' : 'Updated syllabi <strong>' + data.name + '<strong>.';
      self.props._handleToggleToastr('success', 'Success!', message);
      self.__loadSubjectData__();
    });
  }

  __loadSyllabiData__() {
    const self = this;
    this.props.request.call('GET', '/api/syllabi/' + self.props.params.id, null, (err, res) => {
      if (err) {
        self.props.__ajaxErrorHandler__(self, res);
        return;
      }

      const data = res.body.data;
      $(self.refs.name).val(data.name);
      self.recordId = data.id;
      self.setState({title: 'Edit Syllabi - ' + data.name});
    });
  }

  __loadSubjectData__() {
    const self = this;
    asyncSeries([
      callback => {
        self.props.request.call('GET', '/api/subject?status=1', null, (err, res) => {
          if (err) {
            self.props.__ajaxErrorHandler__(self, res);
            callback(res);
            return;
          }

          const data = res.body.data;
          callback(null, data);
        });
      },
      callback => {
        if (self.action === 'edit') {
          self.props.request.call('GET', '/api/syllabi/' + self.props.params.id + '/subject?status=1', null, (err, res) => {
            if (err) {
              self.props.__ajaxErrorHandler__(self, res);
              callback(res);
              return;
            }

            const data = res.body.data;
            callback(null, data);
          });
        } else {
          callback(null, []);
        }
      }
    ], (err, results) => {
      if (err) {
        console.log(err);
        return;
      }

      let options = _.map(results[0], obj => {
        return {
          value: obj.id,
          label: obj.name
        };
      });
      let selected = _.map(results[1], obj => {
        return obj.id;
      });
      self.setState({
        subjectOptions: options,
        selectedSubjects: selected
      });
    });
  }

  componentDidMount() {
    $('#form').parsley();
    this.__loadSubjectData__();
    if (this.action === 'edit') {
      this.__loadSyllabiData__();
    }
  }

  render() {
    const {subjectOptions, selectedSubjects} = this.state;
    return (
      <div>
        {React.createElement(CmpBreadcrumbs, this.state.breadcrumbsProps)}
        <div className="wrapper wrapper-content animated fadeIn">
          <div className="row">
            <div className="col-lg-12">
              <a href="#/syllabi" className="btn btn-outline btn-primary m-b-xs"><i className="fa fa-hand-o-left"></i> Back</a>
              <div className="ibox">
                <div className="ibox-title">
                  <h5>{this.state.title}</h5>
                </div>
                <div className="ibox-content">
                  <div className="alert alert-danger hide" ref="error-message"></div>
                  <div className="row">
                    <div className="col-lg-4">
                      <form id="form" onSubmit={this._handleSubmit.bind(this)}>
                        <div className="form-group">
                          <label>Syllabi Name <span className="text-danger">*</span></label>
                          <input type="text" name="name" ref="name" className="form-control" placeholder="Enter Syllabi Name" autoComplete="off" required maxLength="100" />
                        </div>
                        <button ref="button" className="btn btn-primary pull-right" type="submit"><i className="fa fa-check"></i> Submit</button>
                        <a href="#/syllabi" className="btn btn-white pull-right m-r-xs"><i className="fa fa-ban"></i> Cancel</a>
                      </form>
                    </div>
                    <div className="col-lg-8">
                      <div className="form-group">
                        <label>Subjects</label>
                        <DualListBox options={subjectOptions} selected={selectedSubjects} onChange={this._handleDualListBoxChange.bind(this)} />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ViewSyllabiForm.propTypes = {
  location: React.PropTypes.object.isRequired,
  request: React.PropTypes.object
};

export default ViewSyllabiForm;
