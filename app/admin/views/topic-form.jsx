/* globals _ */
import React, {Component} from 'react';
import CmpBreadcrumbs from './../components/common/breadcrumbs';
import DualListBox from 'react-dual-listbox';
import asyncSeries from 'async/series';
import asyncAuto from 'async/auto';
import striptags from 'striptags';

class ViewTopicForm extends Component {
  constructor(props) {
    super(props);
    this.action = _.includes(props.location.pathname, 'add') ? 'add' : 'edit';
    this.recordId = null;

    let title = this.action === 'add' ? 'Add Topic' : 'Edit Topic';
    let breadcrumbsProps = {
      title: 'Topic',
      links: [{
        name: 'Dashboard',
        href: '/'
      }, {
        name: 'Topic',
        href: '/topic'
      }]
    };
    breadcrumbsProps.links.push({
      name: title
    });

    this.state = {
      title: title,
      breadcrumbsProps: breadcrumbsProps,
      questionOptions: [],
      selectedQuestions: []
    };
  }

  _handleDualListBoxChange(selected) {
    this.setState({selectedQuestions: selected});
  }

  _handleSubmit(event) {
    event.preventDefault();

    const self = this;
    const name = this.refs.name;
    const is_paid = this.refs.is_paid; // eslint-disable-line camelcase
    const button = this.refs.button;
    const errorMessage = this.refs['error-message'];

    // Reset Error Message
    $(errorMessage).addClass('hide').text('');

    // Trim
    $(name).val($(name).val().trim());

    // Validate
    if (!$('#form').parsley().isValid()) {
      return;
    }

    let data = {
      name: $(name).val(),
      is_paid: $(is_paid).val() // eslint-disable-line camelcase
    };
    if (this.action === 'edit') {
      data.id = this.recordId;
    }

    // Disable button
    $(button).prop('disabled', true);

    // Set method and API
    const method = this.action === 'add' ? 'POST' : 'PATCH';
    const api = this.action === 'add' ? '/api/topic' : '/api/topic/' + this.recordId;

    asyncAuto({
      topic: callback => {
        self.props.request.call(method, api, data, (err, res) => {
          if (err) {
            $(errorMessage).text(res.body.message).removeClass('hide');
            if (_.includes(res.body.message.toLowerCase(), 'topic name')) {
              $(name).addClass('parsley-error');
            }
            callback(res);
            return;
          }

          const id = self.action === 'add' ? res.body.insertId : self.recordId;
          callback(null, {id});
        });
      },
      addQuestions: ['topic', (results, callback) => {
        const topicData = {
          questions: self.state.selectedQuestions
        };
        self.props.request.call('POST', '/api/topic/' + results.topic.id + '/question', topicData, (err, res) => {
          if (err) {
            self.props.__ajaxErrorHandler__(self, res);
            callback(res);
            return;
          }
          callback(null);
        });
      }]
    }, (err, results) => {
      // Enable button
      $(button).prop('disabled', false);

      if (err) {
        console.log(err);
        return;
      }

      const form = $('#form');
      form.parsley().reset();
      if (self.action === 'add') {
        form[0].reset();
      }

      const message = self.action === 'add' ? 'Created new topic.' : 'Updated topic <strong>' + data.name + '<strong>.';
      self.props._handleToggleToastr('success', 'Success!', message);
      self.__loadQuestionData__();
    });
  }

  __loadTopicData__() {
    const self = this;
    this.props.request.call('GET', '/api/topic/' + self.props.params.id, null, function(err, res) {
      if (err) {
        self.props.__ajaxErrorHandler__(self, res);
        return;
      }

      const data = res.body.data;
      $(self.refs.name).val(data.name);
      $(self.refs.is_paid).val(data.is_paid);
      self.recordId = data.id;
      self.setState({title: 'Edit Topic - ' + data.name});
    });
  }

  __loadQuestionData__() {
    const self = this;
    asyncSeries([
      callback => {
        self.props.request.call('GET', '/api/question?status=1', null, (err, res) => {
          if (err) {
            self.props.__ajaxErrorHandler__(self, res);
            callback(res);
            return;
          }

          const data = res.body.data;
          callback(null, data);
        });
      },
      callback => {
        if (self.action === 'edit') {
          self.props.request.call('GET', '/api/topic/' + self.props.params.id + '/question?status=1', null, (err, res) => {
            if (err) {
              self.props.__ajaxErrorHandler__(self, res);
              callback(res);
              return;
            }

            const data = res.body.data;
            callback(null, data);
          });
        } else {
          callback(null, []);
        }
      }
    ], (err, results) => {
      if (err) {
        console.log(err);
        return;
      }

      let options = _.map(results[0], obj => {
        return {
          value: obj.id,
          label: striptags(obj.description)
        };
      });
      let selected = _.map(results[1], obj => {
        return obj.id;
      });
      self.setState({
        questionOptions: options,
        selectedQuestions: selected
      });
    });
  }

  componentDidMount() {
    $('#form').parsley();
    this.__loadQuestionData__();
    if (this.action === 'edit') {
      this.__loadTopicData__();
    }
  }

  render() {
    const {questionOptions, selectedQuestions} = this.state;
    return (
      <div>
        {React.createElement(CmpBreadcrumbs, this.state.breadcrumbsProps)}
        <div className="wrapper wrapper-content animated fadeIn">
          <div className="row">
            <div className="col-lg-12">
              <a href="#/topic" className="btn btn-outline btn-primary m-b-xs"><i className="fa fa-hand-o-left"></i> Back</a>
              <div className="ibox">
                <div className="ibox-title">
                  <h5>{this.state.title}</h5>
                </div>
                <div className="ibox-content">
                  <div className="alert alert-danger hide" ref="error-message"></div>
                  <div className="row">
                    <div className="col-lg-4">
                      <form id="form" onSubmit={this._handleSubmit.bind(this)}>
                        <div className="row">
                          <div className="col-lg-12">
                            <div className="form-group">
                              <label>Topic Name <span className="text-danger">*</span></label>
                              <input type="text" name="name" ref="name" className="form-control" placeholder="Enter Topic Name" autoComplete="off" required maxLength="100" />
                            </div>
                          </div>
                          <div className="col-lg-12">
                            <div className="form-group">
                              <label>Type <span className="text-danger">*</span></label>
                              <select name="is_paid" ref="is_paid" id="" className="form-control" required>
                                <option value="">-- Select Type --</option>
                                <option value={1}>Paid</option>
                                <option value={0}>Free</option>
                              </select>
                            </div>
                          </div>
                        </div>
                        <button ref="button" className="btn btn-primary pull-right" type="submit"><i className="fa fa-check"></i> Submit</button>
                        <a href="#/topic" className="btn btn-white pull-right m-r-xs"><i className="fa fa-ban"></i> Cancel</a>
                      </form>
                    </div>
                    <div className="col-lg-8">
                      <div className="form-group">
                        <label>Questions</label>
                        <DualListBox options={questionOptions} selected={selectedQuestions} onChange={this._handleDualListBoxChange.bind(this)} />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ViewTopicForm.propTypes = {
  location: React.PropTypes.object.isRequired,
  request: React.PropTypes.object
};

export default ViewTopicForm;
