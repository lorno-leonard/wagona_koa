/* globals window, _ */
import React, {Component} from 'react';
import CmpProgress from './../common/progress';
import CmpNavigation from './../common/navigation';
import CmpHeader from './../common/header';
import CmpFooter from './../common/footer';
import {correctHeight, detectBody} from './_helpers';
import toastr from 'toastr';
import request from './../../../_common/request';
import auth from './../../../_common/auth';

const appProps = {
  auth,
  request
};

class LayoutMain extends Component {
  constructor(props) {
    super(props);
    this.state = {
      childrenProps: Object.assign({}, appProps, {
        _handleToggleToastr: this._handleToggleToastr,
        _handleSignOutClick: this._handleSignOutClick.bind(this),
        __ajaxErrorHandler__: this.__ajaxErrorHandler__
      })
    };
  }

  _handleToggleToastr(type, title, message) {
    const self = this;
    if (type === 'signOut') {
      toastr.error('Your session has expired. Logging out in a few seconds.', 'Access Denied', {
        progressBar: true,
        timeOut: 5000,
        onHidden: () => {
          self._handleSignOutClick();
        }
      });
    } else if (_.includes(['info', 'success', 'warning', 'error'], type)) {
      toastr[type](message, title);
    } else {
      toastr.info(message, title);
    }
  }

  _handleSignOutClick() {
    const router = this.router || this.props.router;
    auth.signOut(router);
  }

  __ajaxErrorHandler__(self, res) {
    if (res.body.code === 'UNAUTHORIZED') {
      self.props._handleToggleToastr('signOut');
    } else {
      self.props._handleToggleToastr('error', 'Error', res.body.message);
    }
  }

  componentDidMount() {
    // Run correctHeight function on load and resize window event
    $(window).bind('load resize', function() {
      correctHeight();
      detectBody();
    });

    // Correct height of wrapper after metisMenu animation.
    $('.metismenu a').click(() => {
      setTimeout(() => {
        correctHeight();
      }, 300);
    });
  }

  render() {
    const wrapperClass = 'gray-bg ' + this.props.location.pathname;
    return (
      <div id="wrapper">
        <CmpProgress />
        <CmpNavigation
          location={this.props.location}
          router={this.props.router}
          _handleSignOutClick={this._handleSignOutClick}
        />
        <div id="page-wrapper" className={wrapperClass}>
          <CmpHeader
            router={this.props.router}
            _handleSignOutClick={this._handleSignOutClick}
          />
          {React.cloneElement(this.props.children, this.state.childrenProps)}
          <CmpFooter />
        </div>
      </div>
    );
  }
}

LayoutMain.propTypes = {
  location: React.PropTypes.object.isRequired,
  children: React.PropTypes.node.isRequired,
  router: React.PropTypes.object.isRequired
};

export default LayoutMain;
