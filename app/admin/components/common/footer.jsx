import React, {Component} from 'react';

class CmpFooter extends Component {
  render() {
    return (
      <div className="footer">
        <div>
          <strong>Copyright</strong> &copy; 2016. Wagona Maths. All Rights Reserved.
        </div>
      </div>
    );
  }
}

export default CmpFooter;
