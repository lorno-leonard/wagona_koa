/* globals _ */
import React, {Component} from 'react';
import {Link} from 'react-router';

class CmpBreadcrumbs extends Component {
  render() {
    return (
      <div className="row white-bg border-bottom p-h-xs">
        <div className="col-lg-12 animated fadeIn">
          <ol className="breadcrumb">
            {this.props.links.map(function(obj, index) {
              return _.has(obj, 'href') ?
                <li key={index}>
                  <Link to={obj.href}>{obj.name}</Link>
                </li> :
                <li key={index} className="active">
                  <strong>{obj.name}</strong>
                </li>;
            })}
          </ol>
        </div>
      </div>
    );
  }
}

CmpBreadcrumbs.propTypes = {
  title: React.PropTypes.string.isRequired,
  links: React.PropTypes.array.isRequired
};

export default CmpBreadcrumbs;
