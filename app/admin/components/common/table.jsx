/* globals _ */
import React, {Component} from 'react';
import striptags from 'striptags';

class CmpTable extends Component {
  constructor(props) {
    super(props);
    this._handleSearchSubmit = this._handleSearchSubmit.bind(this);
    this.state = {
      source: [],
      pager: {},
      loading: false
    };
    this.currentPage = 1;
    this.maxPageNumbers = 5;
  }

  _handleChangeStatus(status, id) {
    const self = this;
    const data = {
      status: status
    };
    let source = _.cloneDeep(self.state.source);
    let obj = _.find(source, {id: id});

    // Set `status` to 'loading'
    obj.status = 'loading';
    source = _.merge([], source, {obj});
    self.setState({
      source: source
    });

    this.props.request.call('PATCH', this.props.apiPATCH + '/' + id, data, function(err, res) {
      if (err) {
        self.props.__ajaxErrorHandler__(self, res);
        return;
      }

      // Set `status` on local source
      let source = _.cloneDeep(self.state.source);
      let obj = _.find(source, {id: id});
      obj.status = status;
      source = _.merge([], source, {obj});
      self.setState({
        source: source
      });
    });
  }

  _handleSearchSubmit(event) {
    event.preventDefault();
    this.currentPage = 1;
    this.__loadData__();
  }

  _handleChangePage(page, event) {
    if ($(event.target).attr('disabled')) {
      return;
    }
    this.currentPage = Number(page);
    this.__loadData__();
  }

  _handleChangeLimit() {
    this.currentPage = 1;
    this.__loadData__();
  }

  __loadData__() {
    const self = this;
    let apiGET = this.props.apiGET;
    let queries = [];

    // Clear data
    self.setState({source: []});

    // Check if canSearch
    if (_.has(this.props, 'canSearch') && this.props.canSearch === true) {
      const search = this.refs.search;
      $(search).val($(search).val().trim());
      queries.push('search=' + encodeURIComponent($(search).val()));
    }

    // Check if hasPager
    if (_.has(this.props, 'hasPager') && this.props.hasPager === true) {
      const limit = this.refs.limit;
      queries.push('limit=' + $(limit).val());
      queries.push('page=' + this.currentPage);
    }

    self.setState({loading: true});
    apiGET += '?' + queries.join('&');
    this.props.request.call('GET', apiGET, null, function(err, res) {
      self.setState({loading: false});

      if (err) {
        self.props.__ajaxErrorHandler__(self, res);
        return;
      }

      self.setState({
        source: _.cloneDeep(res.body.data)
      });

      // Check if response has `pager`
      if (_.has(res.body, 'pager')) {
        self.setState({
          pager: res.body.pager
        });
      }
    });
  }

  componentDidMount() {
    this.__loadData__();
  }

  render() {
    const addHref = _.has(this.props, 'addHref') ? '#' + _.replace(this.props.addHref, '#', '') : '#';
    const editHref = _.has(this.props, 'editHref') ? '#' + _.replace(this.props.editHref, '#', '') : '#';
    const addText = _.has(this.props, 'addText') ? this.props.addText : 'Add';
    const editText = _.has(this.props, 'editText') ? this.props.editText : 'Edit';
    const canAdd = _.has(this.props, 'canAdd') && this.props.canAdd === true;
    const canEdit = _.has(this.props, 'canEdit') && this.props.canEdit === true;
    const canSearch = _.has(this.props, 'canSearch') && this.props.canSearch === true;
    const hasStatus = _.has(this.props, 'hasStatus') && this.props.hasStatus === true;
    const hasPager = _.has(this.props, 'hasPager') && this.props.hasPager === true;
    const columns = this.props.columns;
    const colSpan = columns.length + (hasStatus ? 1 : 0) + (canEdit ? 1 : 0);

    // Pagination details
    let elPageNumbers = [];
    let detailsEntryCount = '';
    let detailsPageCount = '';
    let pager = {};
    if (hasPager) {
      pager = this.state.pager;

      // Page number element
      let startPage;
      let endPage;
      if (pager.numPages <= this.maxPageNumbers) {
        startPage = 1;
        endPage = pager.numPages;
      } else {
        if (pager.currentPage <= Math.ceil(this.maxPageNumbers / 2)) {  // eslint-disable-line no-lonely-if
          startPage = 1;
          endPage = this.maxPageNumbers;
        } else if (pager.currentPage >= pager.numPages - Math.ceil(this.maxPageNumbers / 2)) {
          startPage = pager.numPages - Math.ceil(this.maxPageNumbers / 2);
          endPage = pager.numPages;
        } else {
          startPage = pager.currentPage - Math.floor(this.maxPageNumbers / 2);
          endPage = pager.currentPage + Math.floor(this.maxPageNumbers / 2);
        }
      }
      const pageNumbers = _.range(startPage, endPage + 1);
      _.each(pageNumbers, (value, key) => {
        elPageNumbers.push(
          <li key={key} className={pager.currentPage === value ? 'active' : ''}>
            <a disabled={pager.currentPage === value} onClick={this._handleChangePage.bind(this, value)}>{value}</a>
          </li>
        );
      });

      // Entry count details
      const limit = Number($(this.refs.limit).val());
      let entryCountPoint = ((pager.currentPage - 1) * limit);
      let startEntryCount = entryCountPoint + 1;
      let endEntryCount = entryCountPoint + limit > pager.total ? pager.total : entryCountPoint + limit;
      detailsEntryCount = `Showing entry ${startEntryCount} to ${endEntryCount} of ${pager.total}`;

      // Page count details
      detailsPageCount = `Page ${pager.currentPage} of ${pager.numPages}`;
    }

    return (
      <div className="ibox">
        <div className="ibox-title">
          <div className="row">
            <div className="col-lg-8">
              {canAdd && <a href={addHref} className="btn btn-primary btn-sm"><i className="fa fa-plus"></i> {addText}</a>}
            </div>
            <div className="col-lg-4">
              {canSearch &&
                <form onSubmit={this._handleSearchSubmit}>
                  <div className="input-group">
                    <input type="text" ref="search" className="form-control" placeholder="Search" />
                    <span className="input-group-btn">
                      <button type="button" className="btn btn-default"><i className="fa fa-search"></i></button>
                    </span>
                  </div>
                </form>
              }
            </div>
          </div>
          {hasPager &&
          <div>
            <div className="row">
              <hr className="line-dashed m-t-xs m-b-xs"/>
              <div className="col-lg-2">
                <select ref="limit" className="form-control input-sm" onChange={this._handleChangeLimit.bind(this)}>
                  <option value="10">10</option>
                  <option value="25">25</option>
                  <option value="50">50</option>
                </select>
              </div>
              <div className="col-lg-6 col-lg-offset-4">
                <ul className="pagination pagination-sm m-n pull-right">
                  {pager.numPages > this.maxPageNumbers &&
                  <li className={pager.currentPage === 1 ? 'disabled' : ''}>
                    <a disabled={pager.currentPage === 1} onClick={this._handleChangePage.bind(this, 1)}>First</a>
                  </li>
                  }
                  <li className={pager.currentPage === 1 ? 'disabled' : ''}>
                    <a disabled={pager.currentPage === 1} onClick={this._handleChangePage.bind(this, pager.currentPage - 1)}>Prev</a>
                  </li>
                  {elPageNumbers}
                  <li className={pager.currentPage === pager.numPages ? 'disabled' : ''}>
                    <a disabled={pager.currentPage === pager.numPages} onClick={this._handleChangePage.bind(this, pager.currentPage + 1)}>Next</a>
                  </li>
                  {pager.numPages > this.maxPageNumbers &&
                  <li className={pager.currentPage === pager.numPages ? 'disabled' : ''}>
                    <a disabled={pager.currentPage === pager.numPages} onClick={this._handleChangePage.bind(this, pager.numPages)}>Last</a>
                  </li>
                  }
                </ul>
              </div>
            </div>
            <div className="row m-t-xs">
              <div className="col-lg-6">
                <span>{this.state.loading === false && detailsEntryCount}</span>
              </div>
              <div className="col-lg-6">
                <span className="text-right block">{this.state.loading === false && detailsPageCount}</span>
              </div>
            </div>
          </div>
          }
        </div>
        <div className="ibox-content">
          <div className="table-responsive">
            <table className="table table-striped table-hover" ref="table">
              <thead>
                <tr>
                  {hasStatus && <th width={'5%'}></th>}
                  {columns.map((col, index) => {
                    return <th key={index}>{col.title}</th>;
                  })}
                  {canEdit && <th width={'5%'}></th>}
                </tr>
              </thead>
              <tbody>
                {this.state.loading === true && <tr><td colSpan={colSpan}>Loading records...</td></tr>}
                {this.state.loading === false && this.state.source.length === 0 && <tr><td colSpan={colSpan}>No records found.</td></tr>}
                {this.state.source.length > 0 && this.state.source.map((obj, index) => {
                  return <tr key={index}>
                    {hasStatus &&
                    <td className="text-center">
                      {!_.isNumber(obj.status) && obj.status === 'loading' &&
                        <i className="fa fa-lg fa-spinner fa-pulse"></i>
                      }
                      {_.isNumber(obj.status) && obj.status === 1 &&
                        <a title="Deactivate User" onClick={this._handleChangeStatus.bind(this, 0, obj.id)}>
                          <i className="text-navy fa fa-lg fa-check-circle"></i>
                        </a>
                      }
                      {_.isNumber(obj.status) && obj.status === 0 &&
                        <a title="Activate User" onClick={this._handleChangeStatus.bind(this, 1, obj.id)}>
                          <i className="text-danger fa fa-lg fa-times-circle"></i>
                        </a>
                      }
                    </td>
                    }
                    {columns.map((col, indexCol) => {
                      if (_.has(col, 'html') && col.html === true) {
                        return <td key={`${index} - ${indexCol}`} dangerouslySetInnerHTML={{__html: obj[col.field]}}></td>;
                      } else if (_.has(col, 'striptags') && col.striptags === true) {
                        let value = striptags(obj[col.field]);
                        return <td key={`${index} - ${indexCol}`}>{value}</td>;
                      } else {  // eslint-disable-line no-else-return
                        return <td key={`${index} - ${indexCol}`}>
                          {_.has(col, 'tag') && col.tag === true &&
                            col.tagDetails.map((tag, indexTag) => {
                              if (obj[col.field] === tag.value) {
                                return <span key={`${index} - ${indexCol} - ${indexTag}`} className={tag.class}>{tag.title}</span>;
                              }
                              return '';
                            })
                          }
                          {!_.has(col, 'tag') && obj[col.field]}
                        </td>;
                      }
                    })}
                    {canEdit &&
                      <td>
                        <a href={editHref + '/' + obj.id} className="btn btn-white btn-xs">
                          <i className="fa fa-pencil"></i>&nbsp;
                          <span>{editText}</span>
                        </a>
                      </td>
                    }
                  </tr>;
                })
                }
              </tbody>
            </table>
          </div>
          {hasPager &&
          <div>
            <div className="row">
              <div className="col-lg-6 col-lg-offset-6">
                <ul className="pagination pagination-sm m-n pull-right">
                  {pager.numPages > this.maxPageNumbers &&
                  <li className={pager.currentPage === 1 ? 'disabled' : ''}>
                    <a disabled={pager.currentPage === 1} onClick={this._handleChangePage.bind(this, 1)}>First</a>
                  </li>
                  }
                  <li className={pager.currentPage === 1 ? 'disabled' : ''}>
                    <a disabled={pager.currentPage === 1} onClick={this._handleChangePage.bind(this, pager.currentPage - 1)}>Prev</a>
                  </li>
                  {elPageNumbers}
                  <li className={pager.currentPage === pager.numPages ? 'disabled' : ''}>
                    <a disabled={pager.currentPage === pager.numPages} onClick={this._handleChangePage.bind(this, pager.currentPage + 1)}>Next</a>
                  </li>
                  {pager.numPages > this.maxPageNumbers &&
                  <li className={pager.currentPage === pager.numPages ? 'disabled' : ''}>
                    <a disabled={pager.currentPage === pager.numPages} onClick={this._handleChangePage.bind(this, pager.numPages)}>Last</a>
                  </li>
                  }
                </ul>
              </div>
            </div>
          </div>
          }
        </div>
      </div>
    );
  }
}

CmpTable.propTypes = {
  request: React.PropTypes.object,
  columns: React.PropTypes.array,
  apiChangeStatus: React.PropTypes.string,
  apiLoadData: React.PropTypes.string,
  canAdd: React.PropTypes.bool,
  canEdit: React.PropTypes.bool,
  canSearch: React.PropTypes.bool,
  hasStatus: React.PropTypes.bool,
  hasPager: React.PropTypes.bool,
  addHref: React.PropTypes.string,
  editHref: React.PropTypes.string,
  addText: React.PropTypes.string,
  editText: React.PropTypes.string,
  apiGET: React.PropTypes.string,
  apiPATCH: React.PropTypes.string
};

export default CmpTable;
