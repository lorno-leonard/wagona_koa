import React, {Component} from 'react';
import {Link} from 'react-router';

class CmpNavigation extends Component {
  _activeRoute(routeName) {
    if (routeName === '/') {
      return this.props.location.pathname === routeName ? 'active' : '';
    }
    return this.props.location.pathname.indexOf(routeName) > -1 ? 'active' : '';
  }

  _secondLevelActive(routeName) {
    return this.props.location.pathname.indexOf(routeName) > -1 ? 'nav nav-second-level collapse in' : 'nav nav-second-level collapse';
  }

  componentDidMount() {
    const {menu} = this.refs;
    $(menu).metisMenu();
  }

  render() {
    return (
      <nav className="navbar-default navbar-static-side" role="navigation">
        <ul className="nav metismenu" id="side-menu" ref="menu">
          <li className="nav-header">
            <div className="dropdown profile-element">
              <span></span>
              <a data-toggle="dropdown" className="dropdown-toggle">
                <span className="clear">
                  <span className="block m-t-xs">
                    <strong className="font-bold">Wagona Admin</strong>
                  </span>
                  <span className="text-muted text-xs block">Admin<b className="caret"></b></span>
                </span>
              </a>
              <ul className="dropdown-menu animated fadeInUp m-t-xs">
                <li><a onClick={this.props._handleSignOutClick.bind(this)}>Logout</a></li>
              </ul>
            </div>
            <div className="logo-element">WG</div>
          </li>
          <li className={this._activeRoute('/')}>
            <Link to="/"><i className="fa fa-th"></i> <span className="nav-label">Dashboard</span></Link>
          </li>
          <li className={this._activeRoute('/users')}>
            <Link to="/users"><i className="fa fa-users"></i> <span className="nav-label">Users</span></Link>
          </li>
          <li className={this._activeRoute('/country')}>
            <Link to="/country"><i className="fa fa-flag"></i> <span className="nav-label">Countries</span></Link>
          </li>
          <li className={this._activeRoute('/syllabi')}>
            <Link to="/syllabi"><i className="fa fa-book"></i> <span className="nav-label">Syllabus</span></Link>
          </li>
          <li className={this._activeRoute('/subject')}>
            <Link to="/subject"><i className="fa fa-bookmark"></i> <span className="nav-label">Subjects</span></Link>
          </li>
          <li className={this._activeRoute('/topic')}>
            <Link to="/topic"><i className="fa fa-certificate"></i> <span className="nav-label">Topics</span></Link>
          </li>
          <li className={this._activeRoute('/question')}>
            <Link to="/question"><i className="fa fa-question"></i> <span className="nav-label">Questions</span></Link>
          </li>
        </ul>
      </nav>
    );
  }
}

CmpNavigation.propTypes = {
  location: React.PropTypes.object.isRequired,
  _handleSignOutClick: React.PropTypes.func.isRequired
};

export default CmpNavigation;
