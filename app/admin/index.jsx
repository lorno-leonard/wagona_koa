/* globals document */
import React from 'react';
import ReactDOM from 'react-dom';
import Router from 'react-router/lib/Router';
import hashHistory from 'react-router/lib/hashHistory';
import routes from './config/routes';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import 'animate.css/animate.min.css';
import 'pace-progress/themes/green/pace-theme-minimal.css';
import 'sweetalert/dist/sweetalert.css';
import 'parsleyjs/src/parsley.css';
import 'toastr/toastr.scss';
import './../_common/awesome-bootstrap-checkbox.css';
import 'bootstrap-tagsinput/dist/bootstrap-tagsinput.css';
import 'react-dual-listbox/lib/react-dual-listbox.css';
import './../_common/inspinia.css';
import './style.scss';
import './../_common/inspinia.js';

ReactDOM.render(
  <Router history={hashHistory}>{routes}</Router>,
  document.getElementById('root')
);
