import React from 'react';
import Router from 'react-router/lib/Router';
import Route from 'react-router/lib/Route';
import IndexRoute from 'react-router/lib/IndexRoute';
import Redirect from 'react-router/lib/Redirect';
import hashHistory from 'react-router/lib/hashHistory';
import auth from './../../_common/auth';

// Layouts
import LayoutMain from './../components/layouts/main';

// Views
import ViewDashboard from './../views/dashboard';
import ViewUsers from './../views/users';
import ViewUsersForm from './../views/users-form';
import ViewCountry from './../views/country';
import ViewSyllabi from './../views/syllabi';
import ViewSyllabiForm from './../views/syllabi-form';
import ViewSubject from './../views/subject';
import ViewSubjectForm from './../views/subject-form';
import ViewTopic from './../views/topic';
import ViewTopicForm from './../views/topic-form';
import ViewQuestion from './../views/question';
import ViewQuestionForm from './../views/question-form';
import ViewLogin from './../views/login';
import View404 from './../views/_404';

export default (
  <Router history={hashHistory}>
    <Route path="/" component={LayoutMain} onEnter={auth.checkAuth}>
      <IndexRoute component={ViewDashboard} onEnter={auth.checkAuth} />
      <Route path="/users" component={ViewUsers} onEnter={auth.checkAuth} />
      <Route path="/users/add" component={ViewUsersForm} onEnter={auth.checkAuth} />
      <Route path="/users/edit/:id" component={ViewUsersForm} onEnter={auth.checkAuth} />
      <Route path="/country" component={ViewCountry} onEnter={auth.checkAuth} />
      <Route path="/syllabi" component={ViewSyllabi} onEnter={auth.checkAuth} />
      <Route path="/syllabi/add" component={ViewSyllabiForm} onEnter={auth.checkAuth} />
      <Route path="/syllabi/edit/:id" component={ViewSyllabiForm} onEnter={auth.checkAuth} />
      <Route path="/subject" component={ViewSubject} onEnter={auth.checkAuth} />
      <Route path="/subject/add" component={ViewSubjectForm} onEnter={auth.checkAuth} />
      <Route path="/subject/edit/:id" component={ViewSubjectForm} onEnter={auth.checkAuth} />
      <Route path="/topic" component={ViewTopic} onEnter={auth.checkAuth} />
      <Route path="/topic/add" component={ViewTopicForm} onEnter={auth.checkAuth} />
      <Route path="/topic/edit/:id" component={ViewTopicForm} onEnter={auth.checkAuth} />
      <Route path="/question" component={ViewQuestion} onEnter={auth.checkAuth} />
      <Route path="/question/add" component={ViewQuestionForm} onEnter={auth.checkAuth} />
      <Route path="/question/edit/:id" component={ViewQuestionForm} onEnter={auth.checkAuth} />
    </Route>
    <Route path="/login" component={ViewLogin} onEnter={auth.checkAuth} />
    <Route path="/404" component={View404} />
    <Redirect from="*" to="/404" />
  </Router>
);
