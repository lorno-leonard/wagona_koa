import request from 'superagent';

export default {
  call: function(method, api, data, callback) {
    method = method.toLowerCase();

    let req = method === 'get' ?
      request[method](api) :
      request[method](api).send(data);
    req.end(callback);
  }
};
