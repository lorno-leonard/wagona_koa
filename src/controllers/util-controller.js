/* globals Util */

export let UtilController = {
  bcrypt: function * () {
    let result = yield Util.bcryptHash(this.params.text);
    this.body = result;
  },
  encrypt: function * () {
    let result = yield Util.encrypt(this.params.num, this.params.length);
    this.body = result;
  },
  decrypt: function * () {
    let result = yield Util.decrypt(this.params.text, this.params.length);
    this.body = result;
  }
};
