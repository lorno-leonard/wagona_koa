/* globals CountryModel, SignupModel */
import _ from 'lodash';

export let SignupController = {
  findCountry: function * () {
    let query = this.query;
    let countries = yield CountryModel.find(query);
    this.body = {
      data: countries
    };
  },
  createSchool: function * () {
    let params = _.pick(this.request.body, [
      'username',
      'email',
      'password',
      'name',
      'num_students',
      'address1',
      'address2',
      'country_id',
      'first_name',
      'last_name',
      'telephone_number',
      'phone_number',
      'website'
    ]);
    yield SignupModel.createSchool(params);

    this.status = 201;
  },
  createUser: function * () {
    let params = _.pick(this.request.body, [
      'username',
      'email',
      'password',
      'first_name',
      'last_name',
      'address',
      'country_id',
      'telephone_number',
      'phone_number'
    ]);
    yield SignupModel.createUser(params);

    this.status = 201;
  }
};
