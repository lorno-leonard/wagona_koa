/* globals PaymentModel */
// import _ from 'lodash';

export let PaymentController = {
  findPricing: function * () {
    let query = this.query;
    let pricings = yield PaymentModel.findPricing(query);
    this.body = {
      data: pricings
    };
  }
};
