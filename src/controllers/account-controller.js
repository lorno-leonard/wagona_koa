/* globals SyllabiModel, SubjectModel, StudentModel, AccountModel, Util */
import _ from 'lodash';
import moment from 'moment';

export let AccountController = {
  findSyllabi: function * () {
    let query = this.query;
    let syllabus = yield SyllabiModel.find(query);
    yield _.map(syllabus, function(syllabi) {
      return function * () {
        if (syllabi.id) {
          // Encrypt Syllabi Id
          syllabi.id = yield Util.encrypt(syllabi.id.toString());
        }

        return syllabi;
      };
    });
    this.body = {
      data: syllabus
    };
  },
  findSyllabiSubjects: function * () {
    let query = this.query;
    let id = yield Util.decrypt(this.params.id.toString());
    let subjects = yield SyllabiModel.findSubjects(id, query);
    yield _.map(subjects, function(subject) {
      return function * () {
        if (subject.id) {
          // Encrypt Subject Id
          subject.id = yield Util.encrypt(subject.id.toString());
        }

        return subject;
      };
    });
    this.body = {
      data: subjects
    };
  },
  findSubjectTests: function * () {
    let query = this.query;
    let id = yield Util.decrypt(this.params.id.toString());
    let tests = yield AccountModel.findSubjectTests(id, query);
    yield _.map(tests, function(test) {
      return function * () {
        let clonedTestId = _.clone(test.id);
        let clonedSubjectId = _.clone(test.subject_id);
        let clonedUserId = _.clone(test.user_id);

        // Encrypt Ids
        test.id = yield Util.encrypt(test.id.toString());
        test.user_id = yield Util.encrypt(test.user_id.toString()); // eslint-disable-line camelcase
        test.subject_id = yield Util.encrypt(test.subject_id.toString()); // eslint-disable-line camelcase
        if (test.class_id) {
          test.class_id = yield Util.encrypt(test.class_id.toString()); // eslint-disable-line camelcase
        }

        if (test.details) {
          test.details = JSON.parse(test.details);
        }

        // Insert hash
        let testAdded = moment(test.date_added).unix() * 1000;
        test.hash = yield Util.encryptAES(_.join([clonedTestId, clonedSubjectId, clonedUserId, testAdded], '|'));

        return test;
      };
    });
    this.body = {
      data: tests
    };
  },
  findSubjectTopics: function * () {
    let query = this.query;
    let id = yield Util.decrypt(this.params.id.toString());
    let topics = yield SubjectModel.findTopics(id, query);
    yield _.map(topics, function(topic) {
      return function * () {
        if (topic.id) {
          // Encrypt Topic Id
          topic.id = yield Util.encrypt(topic.id.toString());
        }

        return topic;
      };
    });
    this.body = {
      data: topics
    };
  },
  updateStudent: function * () {
    let id = yield Util.decrypt(this.params.id.toString());
    yield StudentModel.update(this.request.body, id);
    this.status = 201;
  }
};
