/* globals StudentModel, Util */
import _ from 'lodash';
import AccountType from '../lib/account-type';

export let StudentController = {
  find: function * () {
    let query = this.query;
    let students = yield StudentModel.find(query);
    yield _.map(students, function(student) {
      return function * () {
        if (student.id) {
          // Encrypt Student Id
          student.id = yield Util.encrypt(student.id.toString());
        }

        if (student.user_id) {
          // Encrypt Syllabi Id
          student.user_id = yield Util.encrypt(student.user_id.toString()); // eslint-disable-line camelcase
        }

        if (student.school_id) {
          // Encrypt Syllabi Id
          student.school_id = yield Util.encrypt(student.school_id.toString()); // eslint-disable-line camelcase
        }

        if (student.syllabi_id) {
          // Encrypt Syllabi Id
          student.syllabi_id = yield Util.encrypt( // eslint-disable-line camelcase
            student.syllabi_id.toString()
          );
        }

        if (student.class_id) {
          // Encrypt Class Id
          student.class_id = yield Util.encrypt(student.class_id.toString()); // eslint-disable-line camelcase
        }

        if (_.has(student, 'account_type')) {
          // Show Account Type key
          student.account_type = yield Util.findKey( // eslint-disable-line camelcase
            student.account_type, AccountType
          );
        }

        if (student.profile) {
          // Parse JSON
          student.profile = JSON.parse(student.profile);
        }

        return student;
      };
    });
    this.body = {
      data: students
    };
  },
  findOne: function * () {
    let query = this.query;
    let id = yield Util.decrypt(this.params.id.toString());
    let student = yield StudentModel.findOne(id, query);

    if (student.id) {
      // Encrypt Student Id
      student.id = yield Util.encrypt(student.id.toString());
    }

    if (student.user_id) {
      // Encrypt Syllabi Id
      student.user_id = yield Util.encrypt(student.user_id.toString()); // eslint-disable-line camelcase
    }

    if (student.school_id) {
      // Encrypt Syllabi Id
      student.school_id = yield Util.encrypt(student.school_id.toString()); // eslint-disable-line camelcase
    }

    if (student.syllabi_id) {
      // Encrypt Syllabi Id
      student.syllabi_id = yield Util.encrypt( // eslint-disable-line camelcase
        student.syllabi_id.toString()
      );
    }

    if (student.class_id) {
      // Encrypt Class Id
      student.class_id = yield Util.encrypt(student.class_id.toString()); // eslint-disable-line camelcase
    }

    if (_.has(student, 'account_type')) {
      // Show Account Type key
      student.account_type = yield Util.findKey( // eslint-disable-line camelcase
        student.account_type, AccountType
      );
    }

    if (student.profile) {
      // Parse JSON
      student.profile = JSON.parse(student.profile);
    }

    this.body = {
      data: student
    };
  },
  create: function * () {
    let pickFields = [
      'user_id',
      'school_id',
      'syllabi_id',
      'class_id',
      'account_type',
      'verified'
    ];
    let params = _.pick(this.request.body, pickFields);
    let otherDetails = _.omit(this.request.body, pickFields);
    if (!_.isEmpty(otherDetails)) {
      params.profile = JSON.stringify(otherDetails);
    }
    let result = yield StudentModel.create(params);
    this.body = {
      insertId: yield Util.encrypt(result.insertId)
    };
    this.status = 201;
  },
  update: function * () {
    let id = yield Util.decrypt(this.params.id.toString());
    yield StudentModel.update(this.request.body, id);
    this.status = 201;
  }
};
