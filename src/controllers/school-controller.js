/* globals SchoolModel, Util */
import _ from 'lodash';
import AccountType from '../lib/account-type';

export let SchoolController = {
  find: function * () {
    let query = this.query;
    let schools = yield SchoolModel.find(query);
    yield _.map(schools, function(school) {
      return function * () {
        if (school.id) {
          // Encrypt School Id
          school.id = yield Util.encrypt(school.id.toString());
        }

        if (school.user_id) {
          // Encrypt User Id
          school.user_id = yield Util.encrypt(school.user_id.toString()); // eslint-disable-line camelcase
        }

        if (_.has(school, 'account_type')) {
          // Show Account Type key
          school.account_type = yield Util.findKey( // eslint-disable-line camelcase
            school.account_type, AccountType
          );
        }

        if (school.profile) {
          // Parse JSON
          school.profile = JSON.parse(school.profile);
        }

        return school;
      };
    });
    this.body = {
      data: schools
    };
  },
  findOne: function * () {
    let query = this.query;
    let id = yield Util.decrypt(this.params.id.toString());
    let school = yield SchoolModel.findOne(id, query);

    if (school.id) {
      // Encrypt School Id
      school.id = yield Util.encrypt(school.id.toString());
    }

    if (school.user_id) {
      // Encrypt User Id
      school.user_id = yield Util.encrypt(school.user_id.toString()); // eslint-disable-line camelcase
    }

    if (_.has(school, 'account_type')) {
      // Show Account Type key
      school.account_type = yield Util.findKey( // eslint-disable-line camelcase
        school.account_type, AccountType
      );
    }

    if (school.profile) {
      // Parse JSON
      school.profile = JSON.parse(school.profile);
    }

    this.body = {
      data: school
    };
  },
  findSyllabus: function * () {
    let query = this.query;
    let id = yield Util.decrypt(this.params.id.toString());
    let syllabus = yield SchoolModel.findSyllabus(id, query);
    yield _.map(syllabus, function(syllabi) {
      return function * () {
        if (syllabi.id) {
          // Encrypt Question Id
          syllabi.id = yield Util.encrypt(syllabi.id.toString());
        }

        return syllabi;
      };
    });
    this.body = {
      data: syllabus
    };
  },
  create: function * () {
    let pickFields = [
      'name',
      'user_id',
      'num_students',
      'account_type',
      'verified'
    ];
    let params = _.pick(this.request.body, pickFields);
    let otherDetails = _.omit(this.request.body, pickFields);
    if (!_.isEmpty(otherDetails)) {
      params.profile = JSON.stringify(otherDetails);
    }
    let result = yield SchoolModel.create(params);
    this.body = {
      insertId: yield Util.encrypt(result.insertId)
    };
    this.status = 201;
  },
  createSyllabus: function * () {
    let id = yield Util.decrypt(this.params.id.toString());
    let params = _.pick(this.request.body, ['syllabus']);
    yield SchoolModel.createSyllabus(params, id);
    this.status = 201;
  },
  update: function * () {
    let id = yield Util.decrypt(this.params.id.toString());
    yield SchoolModel.update(this.request.body, id);
    this.status = 201;
  }
};
