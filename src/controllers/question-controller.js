/* globals QuestionModel, Util */
import _ from 'lodash';
import QuestionType from '../lib/question-type';

export let QuestionController = {
  find: function * () {
    let query = this.query;
    let questions = yield QuestionModel.find(query);
    questions = yield _.map(questions, function(question) {
      return function * () {
        if (question.id) {
          // Encrypt Question Id
          question.id = yield Util.encrypt(question.id.toString());
        }

        if (question.choices) {
          question.choices = yield _.map(question.choices, function(choice) {
            return function * () {
              if (choice.id) {
                // Encrypt Choice Id
                choice.id = yield Util.encrypt(choice.id.toString());
              }

              if (choice.question_id) {
                // Encrypt Question Id
                choice.question_id = yield Util.encrypt( // eslint-disable-line camelcase
                  choice.question_id.toString()
                );
              }

              return choice;
            };
          });
        }

        if (_.has(question, 'type')) {
          // Show Question Type key
          question.type = yield Util.findKey(
            question.type, QuestionType
          );
        }

        if (question.tags) {
          // Show Tags as array
          question.tags = _.split(question.tags, ',');
        }

        return question;
      };
    });
    this.body = {
      data: questions
    };

    // `page` query
    if (!_.isEmpty(query) && _.has(query, 'page') && _.has(query, 'limit') && _.isNumber(Number(query.page)) && _.isNumber(Number(query.limit))) {
      this.body.pager = QuestionModel.pager;
    }
  },
  findOne: function * () {
    let query = this.query;
    let id = yield Util.decrypt(this.params.id.toString());
    let question = yield QuestionModel.findOne(id, query);

    if (question.id) {
      // Encrypt Question Id
      question.id = yield Util.encrypt(question.id.toString());
    }

    if (question.choices) {
      question.choices = yield _.map(question.choices, function(choice) {
        return function * () {
          if (choice.id) {
            // Encrypt Choice Id
            choice.id = yield Util.encrypt(choice.id.toString());
          }

          if (choice.question_id) {
            // Encrypt Question Id
            choice.question_id = yield Util.encrypt( // eslint-disable-line camelcase
              choice.question_id.toString()
            );
          }

          return choice;
        };
      });
    }

    if (_.has(question, 'type')) {
      // Show Question Type key
      question.type = yield Util.findKey( // eslint-disable-line camelcase
        question.type, QuestionType
      );
    }

    if (question.tags) {
      // Show Tags as array
      question.tags = _.split(question.tags, ',');
    }

    this.body = {
      data: question
    };
  },
  findChoices: function * () {
    let query = this.query;
    let id = yield Util.decrypt(this.params.id.toString());
    let choices = yield QuestionModel.findChoices(id, query);

    choices = yield _.map(choices, function(choice) {
      return function * () {
        if (choice.id) {
          // Encrypt Choice Id
          choice.id = yield Util.encrypt(choice.id.toString());
        }

        if (choice.question_id) {
          // Encrypt Question Id
          choice.question_id = yield Util.encrypt( // eslint-disable-line camelcase
            choice.question_id.toString()
          );
        }

        return choice;
      };
    });
    this.body = {
      data: choices
    };
  },
  findOneChoice: function * () {
    let query = this.query;
    let id = yield Util.decrypt(this.params.id.toString());
    let choice = yield QuestionModel.findOneChoice(id, query);

    if (choice.id) {
      // Encrypt Choice Id
      choice.id = yield Util.encrypt(choice.id.toString());
    }

    if (choice.question_id) {
      // Encrypt Question Id
      choice.question_id = yield Util.encrypt(choice.question_id.toString()); // eslint-disable-line camelcase
    }

    this.body = {
      data: choice
    };
  },
  create: function * () {
    let params = _.pick(this.request.body, [
      'description',
      'explanation',
      'type',
      'correct',
      'is_paid',
      'tags',
      'choices'
    ]);
    let result = yield QuestionModel.create(params);
    this.body = {
      insertId: yield Util.encrypt(result.insertId)
    };
    this.status = 201;
  },
  createChoice: function * () {
    let params = _.pick(this.request.body, ['description']);
    params.question_id = this.params.id; // eslint-disable-line camelcase
    let result = yield QuestionModel.createChoice(params);
    this.body = {
      insertId: yield Util.encrypt(result[1].insertId)
    };
    this.status = 201;
  },
  update: function * () {
    let id = yield Util.decrypt(this.params.id.toString());
    yield QuestionModel.update(this.request.body, id);
    this.status = 201;
  },
  updateChoice: function * () {
    let id = yield Util.decrypt(this.params.id.toString());
    yield QuestionModel.updateChoice(this.request.body, id);
    this.status = 201;
  }
};
