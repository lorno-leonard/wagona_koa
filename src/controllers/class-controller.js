/* globals ClassModel, Util */
import _ from 'lodash';

export let ClassController = {
  find: function * () {
    let query = this.query;
    let classes = yield ClassModel.find(query);
    yield _.map(classes, function(_class) {
      return function * () {
        if (_class.id) {
          // Encrypt Class Id
          _class.id = yield Util.encrypt(_class.id.toString());
        }

        if (_class.school_id) {
          // Encrypt School Id
          _class.school_id = yield Util.encrypt(_class.school_id.toString()); // eslint-disable-line camelcase
        }

        if (_class.syllabi_id) {
          // Encrypt Syllabi Id
          _class.syllabi_id = yield Util.encrypt(_class.syllabi_id.toString()); // eslint-disable-line camelcase
        }

        if (_class.subject_id) {
          // Encrypt Subject Id
          _class.subject_id = yield Util.encrypt(_class.subject_id.toString()); // eslint-disable-line camelcase
        }

        return _class;
      };
    });
    this.body = {
      data: classes
    };
  },
  findOne: function * () {
    let query = this.query;
    let id = yield Util.decrypt(this.params.id.toString());
    let _class = yield ClassModel.findOne(id, query);

    if (_class.id) {
      // Encrypt Class Id
      _class.id = yield Util.encrypt(_class.id.toString());
    }

    if (_class.school_id) {
      // Encrypt School Id
      _class.school_id = yield Util.encrypt(_class.school_id.toString()); // eslint-disable-line camelcase
    }

    if (_class.syllabi_id) {
      // Encrypt Syllabi Id
      _class.syllabi_id = yield Util.encrypt(_class.syllabi_id.toString()); // eslint-disable-line camelcase
    }

    if (_class.subject_id) {
      // Encrypt Subject Id
      _class.subject_id = yield Util.encrypt(_class.subject_id.toString()); // eslint-disable-line camelcase
    }

    this.body = {
      data: _class
    };
  },
  create: function * () {
    let params = _.pick(this.request.body, [
      'name',
      'school_id',
      'syllabi_id',
      'subject_id'
    ]);
    let result = yield ClassModel.create(params);
    this.body = {
      insertId: yield Util.encrypt(result.insertId)
    };
    this.status = 201;
  },
  update: function * () {
    let id = yield Util.decrypt(this.params.id.toString());
    yield ClassModel.update(this.request.body, id);
    this.status = 201;
  }
};
