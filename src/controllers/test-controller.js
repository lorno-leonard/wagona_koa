/* globals TestModel, Util */
import _ from 'lodash';
import moment from 'moment';

export let TestController = {
  find: function * () {
    let query = this.query;
    let tests = yield TestModel.find(query);
    yield _.map(tests, function(test) {
      return function * () {
        let clonedTestId = _.clone(test.id);
        let clonedSubjectId = _.clone(test.subject_id);
        let clonedUserId = _.clone(test.user_id);

        // Encrypt Ids
        test.id = yield Util.encrypt(test.id.toString());
        test.user_id = yield Util.encrypt(test.user_id.toString()); // eslint-disable-line camelcase
        test.subject_id = yield Util.encrypt(test.subject_id.toString()); // eslint-disable-line camelcase
        if (test.class_id) {
          test.class_id = yield Util.encrypt(test.class_id.toString()); // eslint-disable-line camelcase
        }

        if (test.details) {
          test.details = JSON.parse(test.details);
        }

        // Insert hash
        let testAdded = moment(test.date_added).unix() * 1000;
        test.hash = yield Util.encryptAES(_.join([clonedTestId, clonedSubjectId, clonedUserId, testAdded], '|'));

        return test;
      };
    });
    this.body = {
      data: tests
    };

    // `page` query
    if (!_.isEmpty(query) && _.has(query, 'page') && _.has(query, 'limit') && _.isNumber(Number(query.page)) && _.isNumber(Number(query.limit))) {
      this.body.pager = TestModel.pager;
    }
  },
  findOne: function * () {
    let query = this.query;
    let id = yield Util.decrypt(this.params.id.toString());
    let test = yield TestModel.findOne(id, query);

    let clonedTestId = _.clone(test.id);
    let clonedSubjectId = _.clone(test.subject_id);
    let clonedUserId = _.clone(test.user_id);

    // Encrypt Ids
    test.id = yield Util.encrypt(test.id.toString());
    test.user_id = yield Util.encrypt(test.user_id.toString()); // eslint-disable-line camelcase
    test.subject_id = yield Util.encrypt(test.subject_id.toString()); // eslint-disable-line camelcase
    if (test.class_id) {
      test.class_id = yield Util.encrypt(test.class_id.toString()); // eslint-disable-line camelcase
    }

    if (test.details) {
      test.details = JSON.parse(test.details);
    }

    // Insert hash
    let testAdded = moment(test.date_added).unix() * 1000;
    test.hash = yield Util.encryptAES(_.join([clonedTestId, clonedSubjectId, clonedUserId, testAdded], '|'));

    this.body = {
      data: test
    };
  },
  findTestQuestions: function * () {
    let query = this.query;
    let decrypt = yield Util.decryptAES(this.params.hash);
    let decryptArr = decrypt.split('|');
    let testId = Number(decryptArr[0]); // First item on array is test_id
    let questions = yield TestModel.findTestQuestions(testId);
    yield _.map(questions, function(question) {
      return function * () {
        // Parse JSON
        let clonedDetails = question.details;
        delete question.details;
        let parsedDetails = JSON.parse(clonedDetails);

        // Encrypt Ids
        question.id = yield Util.encrypt(question.id.toString());
        parsedDetails.question_id = yield Util.encrypt(parsedDetails.question_id.toString()); // eslint-disable-line
        parsedDetails.topic_id = yield Util.encrypt(parsedDetails.topic_id.toString()); // eslint-disable-line

        // Delete properties not required in test proper
        if (_.has(query, 'type') && query.type === 'test') {
          delete parsedDetails.correct;
          delete parsedDetails.explanation;
        }

        return _.merge(question, parsedDetails);
      };
    });

    this.body = {
      data: questions
    };
  },
  create: function * () {
    let params = _.pick(this.request.body, [
      'subject_id',
      'class_id',
      'topics'
    ]);
    params.subject_id = yield Util.decrypt(params.subject_id.toString()); // eslint-disable-line
    params.topics = yield _.map(params.topics, function(topicId) {
      return function * () {
        return yield Util.decrypt(topicId.toString());
      };
    });
    let result = yield TestModel.create(params);
    this.body = {
      hash: result
    };
    this.status = 201;
  },
  mark: function * () {
    let questions = this.request.body.questions;

    // Decrypt hash
    let decrypt = yield Util.decryptAES(this.params.hash);
    let decryptArr = decrypt.split('|');
    let testId = Number(decryptArr[0]); // First item on array is test_id

    // Decrypt Test Question Ids
    yield _.map(questions, function(question) {
      return function * () {
        question.id = yield Util.decrypt(question.id.toString());
        return question;
      };
    });

    // Mark Test
    yield TestModel.mark(testId, questions);
    this.status = 201;
  }
};
