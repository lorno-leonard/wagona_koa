/* globals PaymentModel, StudentModel, SchoolModel, SyllabiModel, ClassModel, TestModel, Util, User */
import _ from 'lodash';
import path from 'path';
import sendfile from 'koa-sendfile';
import Role from '../lib/role';
import AccountType from '../lib/account-type';

export let PageController = {
  index: function * () {
    let page = 'index';
    yield this.render(
      'main',
      _.merge({
        page,
        title: 'Home',
        pageLayout: 'main'
      }, yield PageController._otherPageFields(page, this))
    );
  },
  login: function * () {
    let page = 'login';
    yield PageController._validatePage(page, this);
    yield this.render(
      'main',
      _.merge({
        page,
        title: 'Login',
        pageLayout: 'main'
      }, yield PageController._otherPageFields(page, this))
    );
  },
  register: function * () {
    let page = 'register';
    yield PageController._validatePage(page, this);
    yield this.render(
      'main',
      _.merge({
        page,
        title: 'Register',
        pageLayout: 'main'
      }, yield PageController._otherPageFields(page, this))
    );
  },
  pricing: function * () {
    let page = 'pricing';
    yield this.render(
      'main',
      _.merge({
        page,
        title: 'Pricing',
        pageLayout: 'main'
      }, yield PageController._otherPageFields(page, this))
    );
  },
  account: function * () {
    let page = 'account';
    yield PageController._validatePage(page, this);
    yield this.render(
      'main',
      _.merge({
        page,
        title: 'Account Dashboard',
        pageLayout: 'main'
      }, yield PageController._otherPageFields(page, this))
    );
  },
  accountProfile: function * () {
    let page = 'accountProfile';
    yield PageController._validatePage(page, this);
    yield this.render(
      'main',
      _.merge({
        page,
        title: 'Account Profile',
        pageLayout: 'main'
      }, yield PageController._otherPageFields(page, this))
    );
  },
  test: function * () {
    let page = 'test';
    yield PageController._validatePage(page, this);

    let hash = this.params.hash;
    let decrypt = yield Util.decryptAES(hash);
    let valid = !_.isEmpty(decrypt);
    let testEnded = false;

    if (valid) {
      let decryptArr = decrypt.split('|');
      let testId = Number(decryptArr[0]); // First item on array is test_id
      let test = yield TestModel.findOne(testId);
      testEnded = !_.isNull(test.date_ended);
    }

    if (testEnded) {
      this.redirect('/test/' + hash + '/history');
    } else {
      yield this.render(
        'main',
        _.merge({
          page,
          title: 'Test Proper',
          pageLayout: 'test',
          test: {
            valid,
            hash
          }
        }, yield PageController._otherPageFields(page, this))
      );
    }
  },
  testHistory: function * () {
    let page = 'history';
    yield PageController._validatePage(page, this);

    let hash = this.params.hash;
    let decrypt = yield Util.decryptAES(hash);
    let valid = !_.isEmpty(decrypt);

    yield this.render(
      'main',
      _.merge({
        page,
        title: 'Test History',
        pageLayout: 'test',
        test: {
          valid,
          hash
        }
      }, yield PageController._otherPageFields(page, this))
    );
  },
  admin: function * () {
    yield this.render('admin');
  },
  sampleLanding: function * () {
    let page = 'sample-landing';
    yield this.render(
      'main',
      _.merge({
        page,
        title: 'Sample Landing Page'
      }, yield PageController._otherPageFields(page, this))
    );
  },
  uploads: function * () {
    yield sendfile(this, path.join(__dirname, '../../uploads/', this.params.file));
  },
  _validatePage: function * (page, self) {
    let isLoggedIn = User ? (User.role === Role.ADMIN ? false : true) : false // eslint-disable-line
    let pagesArr = [
      'login',
      'register'
    ];

    if (_.includes(pagesArr, page) && isLoggedIn) {
      self.redirect('/');
    } else if (!_.includes(pagesArr, page) && !isLoggedIn) {
      self.redirect('/login');
    }
  },
  _otherPageFields: function * (page, self) {
    let obj = {
      isLoggedIn: User ? (User.role === Role.ADMIN ? false : true) : false // eslint-disable-line
    };
    if (obj.isLoggedIn) {
      let user = _.clone(User);

      user.role = yield Util.findKey(
        User.role, Role
      );

      // Delete fields
      delete user.password;
      delete user.id;

      obj.user = user;
    }

    // PRICING page fields
    if (page === 'pricing') {
      obj.pricing = yield PaymentModel.findPricing();
    }

    // ACCOUNT page fields
    if (_.includes(['account', 'accountProfile'], page) && obj.isLoggedIn) {
      // Role = INDIVIDUAL
      if (User.role === Role.INDIVIDUAL) {
        let account = yield StudentModel.findByUserId(User.id, {
          fields: ['id', 'school_id', 'syllabi_id', 'class_id', 'account_type', 'verified', 'profile'].join(',')
        });
        account.account_type = yield Util.findKey( // eslint-disable-line
          account.account_type, AccountType
        );
        account.student_id = yield Util.encrypt(account.id.toString()); // eslint-disable-line
        account.profile = JSON.parse(account.profile);

        // Get school details
        if (account.school_id) {
          let school = yield SchoolModel.findOne(account.school_id, {
            fields: ['name'].join(',')
          });
          account.school = school.name;
          account.school_id = yield Util.encrypt(account.school_id.toString()); // eslint-disable-line
        } else {
          account.school = null;
        }

        // Get syllabi details
        if (account.syllabi_id) {
          let syllabi = yield SyllabiModel.findOne(account.syllabi_id, {
            fields: ['name'].join(',')
          });
          account.syllabi = syllabi.name;
          account.syllabi_id = yield Util.encrypt(account.syllabi_id.toString()); // eslint-disable-line
        } else {
          account.syllabi = null;
        }

        // Get class details
        if (account.class_id) {
          let _class = yield ClassModel.findOne(account.class_id, {
            fields: ['name'].join(',')
          });
          account.class = _class.name;
          account.class_id = yield Util.encrypt(account.class_id.toString()); // eslint-disable-line
        } else {
          account.class = null;
        }

        // Delete fields
        delete account.id;

        obj.account = account;
      }
    }

    return obj;
  }
};
