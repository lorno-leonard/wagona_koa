/* globals SubjectModel, Util */
import _ from 'lodash';

export let SubjectController = {
  find: function * () {
    let query = this.query;
    let subjects = yield SubjectModel.find(query);
    yield _.map(subjects, function(subject) {
      return function * () {
        if (subject.id) {
          // Encrypt Subject Id
          subject.id = yield Util.encrypt(subject.id.toString());
        }

        return subject;
      };
    });
    this.body = {
      data: subjects
    };

    // `page` query
    if (!_.isEmpty(query) && _.has(query, 'page') && _.has(query, 'limit') && _.isNumber(Number(query.page)) && _.isNumber(Number(query.limit))) {
      this.body.pager = SubjectModel.pager;
    }
  },
  findOne: function * () {
    let query = this.query;
    let id = yield Util.decrypt(this.params.id.toString());
    let subject = yield SubjectModel.findOne(id, query);

    if (subject.id) {
      // Encrypt Subject Id
      subject.id = yield Util.encrypt(subject.id.toString());
    }

    this.body = {
      data: subject
    };
  },
  findTopics: function * () {
    let query = this.query;
    let id = yield Util.decrypt(this.params.id.toString());
    let topics = yield SubjectModel.findTopics(id, query);
    yield _.map(topics, function(topic) {
      return function * () {
        if (topic.id) {
          // Encrypt Topic Id
          topic.id = yield Util.encrypt(topic.id.toString());
        }

        return topic;
      };
    });
    this.body = {
      data: topics
    };
  },
  create: function * () {
    let params = _.pick(this.request.body, ['name']);
    let results = yield SubjectModel.create(params);
    this.body = {
      insertId: yield Util.encrypt(results.insertId)
    };
    this.status = 201;
  },
  createTopics: function * () {
    let id = yield Util.decrypt(this.params.id.toString());
    let params = _.pick(this.request.body, ['topics']);
    yield SubjectModel.createTopics(params, id);
    this.status = 201;
  },
  update: function * () {
    let id = yield Util.decrypt(this.params.id.toString());
    yield SubjectModel.update(this.request.body, id);
    this.status = 201;
  }
};
