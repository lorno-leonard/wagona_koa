/* globals UserModel, AppError, Util */
import _ from 'lodash';
import Token from '../lib/token';
import Role from '../lib/role';
import moment from 'moment';

export let AuthController = {
  login: function * () {
    let params = _.pick(this.request.body, [
      'username',
      'password',
      'remember_me'
    ]);

    // Check if there is a username and a password
    if (!_.has(params, 'username') || !_.has(params, 'password')) {
      throw new AppError('FORBIDDEN', 'Username and Password required.');
    }

    // Check if username exists
    let userByUsername = yield UserModel.findByUsername(params.username);
    let userByEmail = yield UserModel.findByEmail(params.username);
    if (!userByUsername && !userByEmail) {
      throw new AppError('INVALID_USERNAME', 'Username/Email does not exist.');
    }
    let user = userByUsername || userByEmail;

    // Check password validity
    let compare = yield Util.bcryptCompare(params.password, user.password);
    if (!compare) {
      throw new AppError('INVALID_PASSWORD', 'Invalid password.');
    }

    // Check if user is loggin in to proper page
    let referer = this.request.header.referer;
    let isAdminPage = _.isUndefined(referer) ? true : _.includes(referer, 'admin'); // if referer is undefined, then it's accessed in a test
    let roleName = yield Util.findKey(user.role, Role);
    if ((isAdminPage && roleName !== 'ADMIN') || (!isAdminPage && roleName === 'ADMIN')) {
      throw new AppError('FORBIDDEN', 'Account forbidden to access this page.');
    }

    // Generate token
    let payload = _.pick(user, [
      'id',
      'username',
      'role'
    ]);
    let expireDateObj = params.remember_me ? {years: 1, minutes: 1} : {days: 1, minutes: 1};
    payload.expire = moment().add(expireDateObj).unix();
    let token = yield Token.create(payload);
    this.cookies.set('token', token);
    this.body = {token};
    this.status = 200;
  },
  logout: function * () {
    // Remove token
    this.cookies.set('token', '');
    this.status = 200;
  }
};
