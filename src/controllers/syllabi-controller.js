/* globals SyllabiModel, Util */
import _ from 'lodash';

export let SyllabiController = {
  find: function * () {
    let query = this.query;
    let syllabus = yield SyllabiModel.find(query);
    yield _.map(syllabus, function(syllabi) {
      return function * () {
        if (syllabi.id) {
          // Encrypt Syllabi Id
          syllabi.id = yield Util.encrypt(syllabi.id.toString());
        }

        return syllabi;
      };
    });
    this.body = {
      data: syllabus
    };

    // `page` query
    if (!_.isEmpty(query) && _.has(query, 'page') && _.has(query, 'limit') && _.isNumber(Number(query.page)) && _.isNumber(Number(query.limit))) {
      this.body.pager = SyllabiModel.pager;
    }
  },
  findOne: function * () {
    let query = this.query;
    let id = yield Util.decrypt(this.params.id.toString());
    let syllabi = yield SyllabiModel.findOne(id, query);

    if (syllabi.id) {
      // Encrypt Syllabi Id
      syllabi.id = yield Util.encrypt(syllabi.id.toString());
    }

    this.body = {
      data: syllabi
    };
  },
  findSubjects: function * () {
    let query = this.query;
    let id = yield Util.decrypt(this.params.id.toString());
    let subjects = yield SyllabiModel.findSubjects(id, query);
    yield _.map(subjects, function(subject) {
      return function * () {
        if (subject.id) {
          // Encrypt Subject Id
          subject.id = yield Util.encrypt(subject.id.toString());
        }

        return subject;
      };
    });
    this.body = {
      data: subjects
    };
  },
  create: function * () {
    let params = _.pick(this.request.body, ['name']);
    let result = yield SyllabiModel.create(params);
    this.body = {
      insertId: yield Util.encrypt(result.insertId)
    };
    this.status = 201;
  },
  createSubjects: function * () {
    let id = yield Util.decrypt(this.params.id.toString());
    let params = _.pick(this.request.body, ['subjects']);
    yield SyllabiModel.createSubjects(params, id);
    this.status = 201;
  },
  update: function * () {
    let id = yield Util.decrypt(this.params.id.toString());
    yield SyllabiModel.update(this.request.body, id);
    this.status = 201;
  }
};
