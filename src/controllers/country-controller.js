/* globals CountryModel */
import _ from 'lodash';

export let CountryController = {
  find: function * () {
    let query = this.query;
    let countries = yield CountryModel.find(query);
    this.body = {
      data: countries
    };

    // `page` query
    if (!_.isEmpty(query) && _.has(query, 'page') && _.has(query, 'limit') && _.isNumber(Number(query.page)) && _.isNumber(Number(query.limit))) {
      this.body.pager = CountryModel.pager;
    }
  },
  findOne: function * () {
    let query = this.query;
    let id = this.params.id;
    let country = yield CountryModel.findOne(id, query);
    this.body = {
      data: country
    };
  },
  create: function * () {
    let params = _.pick(this.request.body, [
      'id',
      'name'
    ]);
    yield CountryModel.create(params);
    this.status = 201;
  },
  update: function * () {
    let id = this.params.id;
    yield CountryModel.update(this.request.body, id);
    this.status = 201;
  }
};
