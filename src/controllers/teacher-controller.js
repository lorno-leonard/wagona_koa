/* globals TeacherModel, Util */
import _ from 'lodash';

export let TeacherController = {
  find: function * () {
    let query = this.query;
    let teachers = yield TeacherModel.find(query);
    yield _.map(teachers, function(teacher) {
      return function * () {
        if (teacher.id) {
          // Encrypt Teacher Id
          teacher.id = yield Util.encrypt(teacher.id.toString());
        }

        if (teacher.user_id) {
          // Encrypt User Id
          teacher.user_id = yield Util.encrypt(teacher.user_id.toString()); // eslint-disable-line camelcase
        }

        if (teacher.profile) {
          // Parse JSON
          teacher.profile = JSON.parse(teacher.profile);
        }

        return teacher;
      };
    });
    this.body = {
      data: teachers
    };
  },
  findOne: function * () {
    let query = this.query;
    let id = yield Util.decrypt(this.params.id.toString());
    let teacher = yield TeacherModel.findOne(id, query);

    if (teacher.id) {
      // Encrypt Teacher Id
      teacher.id = yield Util.encrypt(teacher.id.toString());
    }

    if (teacher.user_id) {
      // Encrypt User Id
      teacher.user_id = yield Util.encrypt(teacher.user_id.toString()); // eslint-disable-line camelcase
    }

    if (teacher.profile) {
      // Parse JSON
      teacher.profile = JSON.parse(teacher.profile);
    }

    this.body = {
      data: teacher
    };
  },
  create: function * () {
    let pickFields = [
      'user_id'
    ];
    let params = _.pick(this.request.body, pickFields);
    let otherDetails = _.omit(this.request.body, pickFields);
    if (!_.isEmpty(otherDetails)) {
      params.profile = JSON.stringify(otherDetails);
    }
    let result = yield TeacherModel.create(params);
    this.body = {
      insertId: yield Util.encrypt(result.insertId)
    };
    this.status = 201;
  },
  update: function * () {
    let id = yield Util.decrypt(this.params.id.toString());
    yield TeacherModel.update(this.request.body, id);
    this.status = 201;
  }
};
