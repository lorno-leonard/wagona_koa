/* globals UserModel, Util */
import _ from 'lodash';
import Role from '../lib/role';

export let UserController = {
  find: function * () {
    let query = this.query;
    let users = yield UserModel.find(query);
    yield _.map(users, function(user) {
      return function * () {
        if (user.id) {
          // Encrypt User Id
          user.id = yield Util.encrypt(user.id.toString());
        }

        if (_.has(user, 'role')) {
          // Show Role key
          user.role = yield Util.findKey(
            user.role, Role
          );
        }

        // Delete Password field
        delete user.password;

        return user;
      };
    });
    this.body = {
      data: users
    };

    // `page` query
    if (!_.isEmpty(query) && _.has(query, 'page') && _.has(query, 'limit') && _.isNumber(Number(query.page)) && _.isNumber(Number(query.limit))) {
      this.body.pager = UserModel.pager;
    }
  },
  findOne: function * () {
    let query = this.query;
    let id = yield Util.decrypt(this.params.id.toString());
    let user = yield UserModel.findOne(id, query);

    if (user.id) {
      // Encrypt User Id
      user.id = yield Util.encrypt(user.id.toString());
    }

    if (_.has(user, 'role')) {
      // Show Role key
      user.role = yield Util.findKey( // eslint-disable-line camelcase
        user.role, Role
      );
    }

    // Delete Password field
    delete user.password;

    this.body = {
      data: user
    };
  },
  create: function * () {
    let params = _.pick(this.request.body, [
      'username',
      'password',
      'email',
      'role',
      'first_name',
      'last_name',
      'country_id'
    ]);
    let result = yield UserModel.create(params);
    this.body = {
      insertId: yield Util.encrypt(result.insertId)
    };
    this.status = 201;
  },
  update: function * () {
    let id = yield Util.decrypt(this.params.id.toString());
    yield UserModel.update(this.request.body, id);
    this.status = 201;
  }
};
