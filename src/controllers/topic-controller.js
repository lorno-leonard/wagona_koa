/* globals TopicModel, Util */
import _ from 'lodash';
import QuestionType from '../lib/question-type';

export let TopicController = {
  find: function * () {
    let query = this.query;
    let topics = yield TopicModel.find(query);
    yield _.map(topics, function(topic) {
      return function * () {
        if (topic.id) {
          // Encrypt Topic Id
          topic.id = yield Util.encrypt(topic.id.toString());
        }

        return topic;
      };
    });
    this.body = {
      data: topics
    };

    // `page` query
    if (!_.isEmpty(query) && _.has(query, 'page') && _.has(query, 'limit') && _.isNumber(Number(query.page)) && _.isNumber(Number(query.limit))) {
      this.body.pager = TopicModel.pager;
    }
  },
  findOne: function * () {
    let query = this.query;
    let id = yield Util.decrypt(this.params.id.toString());
    let topic = yield TopicModel.findOne(id, query);

    if (topic.id) {
      // Encrypt Topic Id
      topic.id = yield Util.encrypt(topic.id.toString());
    }

    this.body = {
      data: topic
    };
  },
  findQuestions: function * () {
    let query = this.query;
    let id = yield Util.decrypt(this.params.id.toString());
    let questions = yield TopicModel.findQuestions(id, query);
    questions = yield _.map(questions, function(question) {
      return function * () {
        if (question.id) {
          // Encrypt Question Id
          question.id = yield Util.encrypt(question.id.toString());
        }

        if (question.choices) {
          question.choices = yield _.map(question.choices, function(choice) {
            return function * () {
              if (choice.id) {
                // Encrypt Choice Id
                choice.id = yield Util.encrypt(choice.id.toString());
              }

              if (choice.question_id) {
                // Encrypt Question Id
                choice.question_id = yield Util.encrypt( // eslint-disable-line camelcase
                  choice.question_id.toString()
                );
              }

              return choice;
            };
          });
        }

        if (_.has(question, 'type')) {
          // Show Question Type key
          question.type = yield Util.findKey( // eslint-disable-line camelcase
            question.type, QuestionType
          );
        }

        if (question.tags) {
          // Show Tags as array
          question.tags = _.split(question.tags, ',');
        }

        return question;
      };
    });
    this.body = {
      data: questions
    };
  },
  create: function * () {
    let params = _.pick(this.request.body, [
      'name',
      'is_paid'
    ]);
    let result = yield TopicModel.create(params);
    this.body = {
      insertId: yield Util.encrypt(result.insertId)
    };
    this.status = 201;
  },
  createQuestions: function * () {
    let id = yield Util.decrypt(this.params.id.toString());
    let params = _.pick(this.request.body, ['questions']);
    yield TopicModel.createQuestions(params, id);
    this.status = 201;
  },
  update: function * () {
    let id = yield Util.decrypt(this.params.id.toString());
    yield TopicModel.update(this.request.body, id);
    this.status = 201;
  }
};
