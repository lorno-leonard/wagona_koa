/* globals User, Util */
import _ from 'lodash';
import db from '../lib/mysql';

let getFields = [
  'id',
  'name',
  'school_id',
  'syllabi_id',
  'subject_id',
  'status'
];

export let ClassModel = {
  find: function * (query) {
    let sqlArr = [];
    let params = [];
    let filterArr = [];
    sqlArr.push(`SELECT ${getFields.join(', ')} FROM o_class`);

    if (!_.isEmpty(query)) {
      let filterFields = _.pick(query, getFields);

      // `fields` query
      if (!_.isEmpty(query.fields)) {
        let queryFieldsArr = query.fields.split(',');
        let queryFields = _.intersection(queryFieldsArr, getFields);

        // Replace fields to display
        if (!_.isEmpty(queryFields)) {
          sqlArr[0] = `SELECT ${queryFields.join(', ')} FROM o_class`;
        }
      }

      // Set filters
      yield _.each(filterFields, function(value, field) {
        if (!_.isEmpty(value)) {
          filterArr.push(`${field} = ?`);
          params.push(value);
        }
      });
    }

    if (!_.isEmpty(filterArr)) {
      sqlArr.push('WHERE ' + filterArr.join(' AND '));
    }

    let sql = sqlArr.join(' ');
    return yield db.query(sql, params);
  },
  findOne: function * (id, query) {
    let sqlArr = [];
    let params = [];
    let filterArr = [];
    sqlArr.push(`SELECT ${getFields.join(', ')} FROM o_class`);
    filterArr.push('id = ?');
    params.push(Number(id));

    if (!_.isEmpty(query)) {
      let filterFields = _.pick(query, getFields);

      // `fields` query
      if (!_.isEmpty(query.fields)) {
        let queryFieldsArr = query.fields.split(',');
        let queryFields = _.intersection(queryFieldsArr, getFields);

        // Replace fields to display
        if (!_.isEmpty(queryFields)) {
          sqlArr[0] = `SELECT ${queryFields.join(', ')} FROM o_class`;
        }
      }

      // Set filters
      yield _.each(filterFields, function(value, field) {
        if (!_.isEmpty(value)) {
          filterArr.push(`${field} = ?`);
          params.push(value);
        }
      });
    }

    if (!_.isEmpty(filterArr)) {
      sqlArr.push('WHERE ' + filterArr.join(' AND '));
    }

    let sql = sqlArr.join(' ');
    let results = yield db.query(sql, params);
    return results.length > 0 ? _.first(results) : false;
  },
  create: function * (params) {
    let createFields = [
      'name',
      'school_id',
      'syllabi_id',
      'subject_id',
      'added_by'
    ];
    return yield db.query(`
      INSERT INTO o_class(${createFields.join(', ')})
      VALUES(${_.times(createFields.length, _.constant('?')).join(', ')})
    `, [
      params.name,
      yield Util.decrypt(params.school_id),
      yield Util.decrypt(params.syllabi_id),
      yield Util.decrypt(params.subject_id),
      User.id
    ]);
  },
  update: function * (params, id) {
    let updateFields = [];
    let updateValues = [];
    let fields = _.pick(params, [
      'name',
      'status'
    ]);

    yield _.each(fields, function(field, key) {
      updateFields.push(`${key} = ?`);
      updateValues.push(field);
    });

    // Add `updated_by` field
    updateFields.push('updated_by = ?');
    updateValues.push(User.id);

    // Add Id
    updateValues.push(id);

    return yield db.query(`
      UPDATE o_class SET ${updateFields.join(', ')}
      WHERE id = ?
    `, updateValues);
  }
};
