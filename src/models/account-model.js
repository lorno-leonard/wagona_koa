/* globals User */
import _ from 'lodash';
import db from '../lib/mysql';

let getSubjectTestFields = [
  'id',
  'user_id',
  'subject_id',
  'details',
  'date_ended'
];

export let AccountModel = {
  pager: {
    total: 0,
    numPages: 0,
    currentPage: 0
  },
  findSubjectTests: function * (subjectId, query) {
    let sqlArr = [];
    let params = [];
    let filterArr = [];
    sqlArr.push(`SELECT ${getSubjectTestFields.join(', ')} FROM o_test`);
    filterArr.push('subject_id = ?');
    params.push(Number(subjectId));
    if (!_.isUndefined(User)) {
      filterArr.push('user_id = ?');
      params.push(Number(User.id));
    }

    if (!_.isEmpty(query)) {
      let filterFields = _.pick(query, getSubjectTestFields);

      // `fields` query
      if (!_.isEmpty(query.fields)) {
        let queryFieldsArr = query.fields.split(',');
        let queryFields = _.intersection(queryFieldsArr, getSubjectTestFields);

        // Replace fields to display
        if (!_.isEmpty(queryFields)) {
          sqlArr[0] = `SELECT ${queryFields.join(', ')} FROM o_test`;
        }
      }

      // Set filters
      yield _.each(filterFields, function(value, field) {
        if (!_.isEmpty(value)) {
          filterArr.push(`${field} = ?`);
          params.push(value);
        }
      });
    }

    if (!_.isEmpty(filterArr)) {
      sqlArr.push('WHERE ' + filterArr.join(' AND '));
    }

    // `page` query
    if (!_.isEmpty(query) && _.has(query, 'page') && _.has(query, 'limit') && _.isNumber(Number(query.page)) && _.isNumber(Number(query.limit))) {
      let page = Number(query.page);
      let limit = Number(query.limit);
      let offset = limit * (page - 1);
      this.pager.currentPage = page;

      // Get total rows and number of pages
      let clonedSqlArr = _.clone(sqlArr);
      clonedSqlArr[0] = `SELECT COUNT(id) AS num_rows FROM o_test`;
      let sqlCount = clonedSqlArr.join(' ');
      let resultCount = yield db.query(sqlCount, params);
      let total = Number(_.first(resultCount).num_rows);
      this.pager.total = total;
      this.pager.numPages = Math.ceil(total / limit);

      // Get records
      sqlArr.push(`LIMIT ${offset}, ${limit}`);
      let sql = sqlArr.join(' ');
      return yield db.query(sql, params);
    }

    let sql = sqlArr.join(' ');
    return yield db.query(sql, params);
  }
};
