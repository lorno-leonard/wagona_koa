/* globals User, Util */
import _ from 'lodash';
import db from '../lib/mysql';

let getFields = [
  'id',
  'user_id',
  'profile',
  'status'
];

export let TeacherModel = {
  find: function * (query) {
    let sqlArr = [];
    let params = [];
    let filterArr = [];
    sqlArr.push(`SELECT ${getFields.join(', ')} FROM o_teacher`);

    if (!_.isEmpty(query)) {
      let filterFields = _.pick(query, getFields);

      // `fields` query
      if (!_.isEmpty(query.fields)) {
        let queryFieldsArr = query.fields.split(',');
        let queryFields = _.intersection(queryFieldsArr, getFields);

        // Replace fields to display
        if (!_.isEmpty(queryFields)) {
          sqlArr[0] = `SELECT ${queryFields.join(', ')} FROM o_teacher`;
        }
      }

      // Set filters
      yield _.each(filterFields, function(value, field) {
        if (!_.isEmpty(value)) {
          filterArr.push(`${field} = ?`);
          params.push(value);
        }
      });
    }

    if (!_.isEmpty(filterArr)) {
      sqlArr.push('WHERE ' + filterArr.join(' AND '));
    }

    let sql = sqlArr.join(' ');
    return yield db.query(sql, params);
  },
  findOne: function * (id, query) {
    let sqlArr = [];
    let params = [];
    let filterArr = [];
    sqlArr.push(`SELECT ${getFields.join(', ')} FROM o_teacher`);
    filterArr.push('id = ?');
    params.push(Number(id));

    if (!_.isEmpty(query)) {
      let filterFields = _.pick(query, getFields);

      // `fields` query
      if (!_.isEmpty(query.fields)) {
        let queryFieldsArr = query.fields.split(',');
        let queryFields = _.intersection(queryFieldsArr, getFields);

        // Replace fields to display
        if (!_.isEmpty(queryFields)) {
          sqlArr[0] = `SELECT ${queryFields.join(', ')} FROM o_teacher`;
        }
      }

      // Set filters
      yield _.each(filterFields, function(value, field) {
        if (!_.isEmpty(value)) {
          filterArr.push(`${field} = ?`);
          params.push(value);
        }
      });
    }

    if (!_.isEmpty(filterArr)) {
      sqlArr.push('WHERE ' + filterArr.join(' AND '));
    }

    let sql = sqlArr.join(' ');
    let results = yield db.query(sql, params);
    return results.length > 0 ? _.first(results) : false;
  },
  create: function * (params) {
    let createFields = [
      'user_id',
      'added_by'
    ];
    let paramFields = [
      yield Util.decrypt(params.user_id),
      User.id
    ];

    if (_.has(params, 'profile')) {
      createFields.push('profile');
      paramFields.push(params.profile);
    }

    return yield db.query(`
      INSERT INTO o_teacher(${createFields.join(', ')})
      VALUES(${_.times(createFields.length, _.constant('?')).join(', ')})
    `, paramFields
    );
  },
  update: function * (params, id) {
    let updateFields = [];
    let updateValues = [];
    let pickFields = [
      'status'
    ];
    let fields = _.pick(params, pickFields);
    let otherDetails = _.omit(params, pickFields);

    yield _.each(fields, function(field, key) {
      updateFields.push(`${key} = ?`);
      updateValues.push(field);
    });

    // Add `profile` field
    if (!_.isEmpty(otherDetails)) {
      updateFields.push('profile = ?');
      updateValues.push(JSON.stringify(otherDetails));
    }

    // Add `updated_by` field
    updateFields.push('updated_by = ?');
    updateValues.push(User.id);

    // Add Id
    updateValues.push(id);

    return yield db.query(`
      UPDATE o_teacher SET ${updateFields.join(', ')}
      WHERE id = ?
    `, updateValues);
  }
};
