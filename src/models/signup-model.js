/* globals Util, SchoolModel */
import _ from 'lodash';
import db from '../lib/mysql';
import Role from '../lib/role';
import AccountType from '../lib/account-type';

export let SignupModel = {
  createSchool: function * (params) {
    let pickFieldsUser = [
      'username',
      'password',
      'email',
      'first_name',
      'last_name',
      'country_id'
    ];
    let pickFieldsSchool = [
      'name',
      'num_students'
    ];

    // Create User
    let createFieldsUser = [
      'username',
      'password',
      'email',
      'role',
      'first_name',
      'last_name',
      'country_id',
      'added_by'
    ];
    let paramsUser = _.pick(params, pickFieldsUser);
    paramsUser.role = Role.SCHOOL;
    let resultUser = yield db.query(`
      INSERT INTO o_users(${createFieldsUser.join(', ')})
      VALUES(${_.times(createFieldsUser.length, _.constant('?')).join(', ')})
    `, [
      paramsUser.username,
      yield Util.bcryptHash(paramsUser.password),
      paramsUser.email,
      paramsUser.role,
      paramsUser.first_name,
      paramsUser.last_name,
      paramsUser.country_id,
      0
    ]);
    let userId = Number(resultUser.insertId);

    // Create School
    let createFieldsSchool = [
      'name',
      'user_id',
      'num_students',
      'account_type',
      'profile',
      'added_by'
    ];
    let paramsSchool = _.pick(params, pickFieldsSchool);
    paramsSchool.user_id = userId; // eslint-disable-line camelcase
    paramsSchool.account_type = AccountType.FREE_SCHOOL; // eslint-disable-line camelcase
    let otherDetails = _.omit(params, _.union(pickFieldsUser, pickFieldsSchool));
    paramsSchool.profile = JSON.stringify(otherDetails);
    let resultSchool = yield db.query(`
      INSERT INTO o_school(${createFieldsSchool.join(', ')})
      VALUES(${_.times(createFieldsSchool.length, _.constant('?')).join(', ')})
    `, [
      paramsSchool.name,
      paramsSchool.user_id,
      paramsSchool.num_students,
      paramsSchool.account_type, // eslint-disable-line camelcase
      paramsSchool.profile,
      0
    ]);
    let schoolId = Number(resultSchool.insertId);

    // Update added_by column
    yield db.query(`UPDATE o_users SET added_by = ? WHERE id = ?`, [userId, userId]);
    yield db.query(`UPDATE o_school SET added_by = ? WHERE id = ?`, [userId, schoolId]);

    return;
  },
  createUser: function * (params) {
    let pickFieldsUser = [
      'username',
      'password',
      'email',
      'first_name',
      'last_name',
      'country_id'
    ];

    // Create User
    let createFieldsUser = [
      'username',
      'password',
      'email',
      'role',
      'first_name',
      'last_name',
      'country_id',
      'added_by'
    ];
    let paramsUser = _.pick(params, pickFieldsUser);
    paramsUser.role = Role.INDIVIDUAL;
    let resultUser = yield db.query(`
      INSERT INTO o_users(${createFieldsUser.join(', ')})
      VALUES(${_.times(createFieldsUser.length, _.constant('?')).join(', ')})
    `, [
      paramsUser.username,
      yield Util.bcryptHash(paramsUser.password),
      paramsUser.email,
      paramsUser.role,
      paramsUser.first_name,
      paramsUser.last_name,
      paramsUser.country_id,
      0
    ]);
    let userId = Number(resultUser.insertId);

    // Get Wagona Academy school
    let school = yield SchoolModel.findByName('Wagona Academy');

    // Create Student
    let createFieldsStudent = [
      'user_id',
      'school_id',
      'account_type',
      'profile',
      'added_by'
    ];
    let paramsStudent = {};
    paramsStudent.user_id = userId; // eslint-disable-line camelcase
    paramsStudent.school_id = school.id; // eslint-disable-line camelcase
    paramsStudent.account_type = AccountType.FREE; // eslint-disable-line camelcase
    let otherDetails = _.omit(params, pickFieldsUser);
    paramsStudent.profile = JSON.stringify(otherDetails);
    let resultStudent = yield db.query(`
      INSERT INTO o_student(${createFieldsStudent.join(', ')})
      VALUES(${_.times(createFieldsStudent.length, _.constant('?')).join(', ')})
    `, [
      paramsStudent.user_id,
      paramsStudent.school_id,
      paramsStudent.account_type, // eslint-disable-line camelcase
      paramsStudent.profile,
      0
    ]);
    let studentId = Number(resultStudent.insertId);

    // Update added_by column
    yield db.query(`UPDATE o_users SET added_by = ? WHERE id = ?`, [userId, userId]);
    yield db.query(`UPDATE o_student SET added_by = ? WHERE id = ?`, [userId, studentId]);

    return;
  }
};
