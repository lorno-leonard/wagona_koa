/* globals User, Util */
import _ from 'lodash';
import db from '../lib/mysql';
import AccountType from '../lib/account-type';

let getFields = [
  'id',
  'user_id',
  'school_id',
  'syllabi_id',
  'class_id',
  'account_type',
  'reference',
  'verified',
  'profile',
  'status'
];

export let StudentModel = {
  find: function * (query) {
    let sqlArr = [];
    let params = [];
    let filterArr = [];
    sqlArr.push(`SELECT ${getFields.join(', ')} FROM o_student`);

    if (!_.isEmpty(query)) {
      let filterFields = _.pick(query, getFields);

      // `fields` query
      if (!_.isEmpty(query.fields)) {
        let queryFieldsArr = query.fields.split(',');
        let queryFields = _.intersection(queryFieldsArr, getFields);

        // Replace fields to display
        if (!_.isEmpty(queryFields)) {
          sqlArr[0] = `SELECT ${queryFields.join(', ')} FROM o_student`;
        }
      }

      // Set filters
      yield _.each(filterFields, function(value, field) {
        if (!_.isEmpty(value)) {
          filterArr.push(`${field} = ?`);
          params.push(value);
        }
      });
    }

    if (!_.isEmpty(filterArr)) {
      sqlArr.push('WHERE ' + filterArr.join(' AND '));
    }

    let sql = sqlArr.join(' ');
    return yield db.query(sql, params);
  },
  findOne: function * (id, query) {
    let sqlArr = [];
    let params = [];
    let filterArr = [];
    sqlArr.push(`SELECT ${getFields.join(', ')} FROM o_student`);
    filterArr.push('id = ?');
    params.push(Number(id));

    if (!_.isEmpty(query)) {
      let filterFields = _.pick(query, getFields);

      // `fields` query
      if (!_.isEmpty(query.fields)) {
        let queryFieldsArr = query.fields.split(',');
        let queryFields = _.intersection(queryFieldsArr, getFields);

        // Replace fields to display
        if (!_.isEmpty(queryFields)) {
          sqlArr[0] = `SELECT ${queryFields.join(', ')} FROM o_student`;
        }
      }

      // Set filters
      yield _.each(filterFields, function(value, field) {
        if (!_.isEmpty(value)) {
          filterArr.push(`${field} = ?`);
          params.push(value);
        }
      });
    }

    if (!_.isEmpty(filterArr)) {
      sqlArr.push('WHERE ' + filterArr.join(' AND '));
    }

    let sql = sqlArr.join(' ');
    let results = yield db.query(sql, params);
    return results.length > 0 ? _.first(results) : false;
  },
  findByUserId: function * (userId, query) {
    let sqlArr = [];
    let params = [];
    let filterArr = [];
    sqlArr.push(`SELECT ${getFields.join(', ')} FROM o_student`);
    filterArr.push('user_id = ?');
    params.push(Number(userId));

    if (!_.isEmpty(query)) {
      let filterFields = _.pick(query, getFields);

      // `fields` query
      if (!_.isEmpty(query.fields)) {
        let queryFieldsArr = query.fields.split(',');
        let queryFields = _.intersection(queryFieldsArr, getFields);

        // Replace fields to display
        if (!_.isEmpty(queryFields)) {
          sqlArr[0] = `SELECT ${queryFields.join(', ')} FROM o_student`;
        }
      }

      // Set filters
      yield _.each(filterFields, function(value, field) {
        if (!_.isEmpty(value)) {
          filterArr.push(`${field} = ?`);
          params.push(value);
        }
      });
    }

    if (!_.isEmpty(filterArr)) {
      sqlArr.push('WHERE ' + filterArr.join(' AND '));
    }

    let sql = sqlArr.join(' ');
    let results = yield db.query(sql, params);
    return results.length > 0 ? _.first(results) : false;
  },
  create: function * (params) {
    let createFields = [
      'user_id',
      'school_id',
      'account_type',
      'added_by'
    ];
    let paramFields = [
      yield Util.decrypt(params.user_id),
      yield Util.decrypt(params.school_id),
      AccountType[params.account_type],
      User.id
    ];

    if (_.has(params, 'syllabi_id')) {
      createFields.push('syllabi_id');
      paramFields.push(yield Util.decrypt(params.syllabi_id));
    }

    if (_.has(params, 'class_id')) {
      createFields.push('class_id');
      paramFields.push(yield Util.decrypt(params.class_id));
    }

    if (_.has(params, 'verified')) {
      createFields.push('verified');
      paramFields.push(params.verified);
    }

    if (_.has(params, 'profile')) {
      createFields.push('profile');
      paramFields.push(params.profile);
    }

    return yield db.query(`
      INSERT INTO o_student(${createFields.join(', ')})
      VALUES(${_.times(createFields.length, _.constant('?')).join(', ')})
    `, paramFields
    );
  },
  update: function * (params, id) {
    let updateFields = [];
    let updateValues = [];
    let pickFields = [
      'school_id',
      'syllabi_id',
      'class_id',
      'verified',
      'reference',
      'status'
    ];
    let fields = _.pick(params, pickFields);
    let otherDetails = _.omit(params, pickFields);

    yield _.map(fields, function(field, key) {
      return function * () {
        updateFields.push(`${key} = ?`);
        if (_.includes(['school_id', 'syllabi_id', 'class_id'], key)) {
          updateValues.push(yield Util.decrypt(field));
        } else {
          updateValues.push(field);
        }
        return;
      };
    });

    // Add `profile` field
    if (!_.isEmpty(otherDetails)) {
      updateFields.push('profile = ?');
      updateValues.push(JSON.stringify(otherDetails));
    }

    // Add `updated_by` field
    updateFields.push('updated_by = ?');
    updateValues.push(User.id);

    // Add Id
    updateValues.push(id);

    return yield db.query(`
      UPDATE o_student SET ${updateFields.join(', ')}
      WHERE id = ?
    `, updateValues);
  }
};
