/* globals User, Util */
import _ from 'lodash';
import db from '../lib/mysql';

let getFields = [
  'id',
  'name',
  'status'
];
let searchFields = [
  'name'
];

export let SubjectModel = {
  pager: {
    total: 0,
    numPages: 0,
    currentPage: 0
  },
  find: function * (query) {
    let sqlArr = [];
    let params = [];
    let filterArr = [];
    sqlArr.push(`SELECT ${getFields.join(', ')} FROM o_subject`);

    if (!_.isEmpty(query)) {
      let filterFields = _.pick(query, getFields);

      // `fields` query
      if (!_.isEmpty(query.fields)) {
        let queryFieldsArr = query.fields.split(',');
        let queryFields = _.intersection(queryFieldsArr, getFields);

        // Replace fields to display
        if (!_.isEmpty(queryFields)) {
          sqlArr[0] = `SELECT ${queryFields.join(', ')} FROM o_subject`;
        }
      }

      // Set filters
      yield _.each(filterFields, function(value, field) {
        if (!_.isEmpty(value)) {
          filterArr.push(`${field} = ?`);
          params.push(value);
        }
      });

      // `search` query
      if (!_.isEmpty(query.search)) {
        let filterSearchArr = [];
        yield _.each(searchFields, function(field) {
          filterSearchArr.push(`${field} LIKE '%${query.search}%'`);
        });
        filterArr.push('(' + filterSearchArr.join(' OR ') + ')');
      }
    }

    if (!_.isEmpty(filterArr)) {
      sqlArr.push('WHERE ' + filterArr.join(' AND '));
    }

    // `page` query
    if (!_.isEmpty(query) && _.has(query, 'page') && _.has(query, 'limit') && _.isNumber(Number(query.page)) && _.isNumber(Number(query.limit))) {
      let page = Number(query.page);
      let limit = Number(query.limit);
      let offset = limit * (page - 1);
      this.pager.currentPage = page;

      // Get total rows and number of pages
      let clonedSqlArr = _.clone(sqlArr);
      clonedSqlArr[0] = `SELECT COUNT(id) AS num_rows FROM o_subject`;
      let sqlCount = clonedSqlArr.join(' ');
      let resultCount = yield db.query(sqlCount, params);
      let total = Number(_.first(resultCount).num_rows);
      this.pager.total = total;
      this.pager.numPages = Math.ceil(total / limit);

      // Get records
      sqlArr.push(`LIMIT ${offset}, ${limit}`);
      let sql = sqlArr.join(' ');
      return yield db.query(sql, params);
    }

    let sql = sqlArr.join(' ');
    return yield db.query(sql, params);
  },
  findOne: function * (id, query) {
    let sqlArr = [];
    let params = [];
    let filterArr = [];
    sqlArr.push(`SELECT ${getFields.join(', ')} FROM o_subject`);
    filterArr.push('id = ?');
    params.push(Number(id));

    if (!_.isEmpty(query)) {
      let filterFields = _.pick(query, getFields);

      // `fields` query
      if (!_.isEmpty(query.fields)) {
        let queryFieldsArr = query.fields.split(',');
        let queryFields = _.intersection(queryFieldsArr, getFields);

        // Replace fields to display
        if (!_.isEmpty(queryFields)) {
          sqlArr[0] = `SELECT ${queryFields.join(', ')} FROM o_subject`;
        }
      }

      // Set filters
      yield _.each(filterFields, function(value, field) {
        if (!_.isEmpty(value)) {
          filterArr.push(`${field} = ?`);
          params.push(value);
        }
      });
    }

    if (!_.isEmpty(filterArr)) {
      sqlArr.push('WHERE ' + filterArr.join(' AND '));
    }

    let sql = sqlArr.join(' ');
    let results = yield db.query(sql, params);
    return results.length > 0 ? _.first(results) : false;
  },
  findIn: function * (ids) {
    let numberIds = _.map(ids, function(id) {
      return Number(id);
    });
    return yield db.query(
      `SELECT ${getFields.join(', ')} FROM o_subject WHERE id IN(${numberIds.join(', ')})`
    );
  },
  findByName: function * (name, id) {
    let sql = `SELECT ${getFields.join(', ')} FROM o_subject WHERE name = ?`;
    let params = [name];

    if (!_.isUndefined(id)) {
      sql += ' AND id != ?';
      params.push(Number(id));
    }

    let results = yield db.query(sql, params);
    return results.length > 0 ? _.first(results) : false;
  },
  findTopics: function * (id, query) {
    let sqlArr = [];
    let params = [];
    let filterArr = [];
    let topicFields = [
      'id',
      'name',
      'is_paid',
      'status'
    ];
    let detailedFields = _.map(topicFields, function(value) {
      return 'o_topic.' + value;
    });
    sqlArr.push(`
      SELECT ${detailedFields.join(', ')}
      FROM m_subject_topic
      LEFT JOIN o_topic ON m_subject_topic.topic_id = o_topic.id
    `);
    filterArr.push('m_subject_topic.subject_id = ?');
    params.push(Number(id));

    if (!_.isEmpty(query)) {
      let filterFields = _.pick(query, topicFields);
      let filterDetailedFields = _.mapKeys(
        filterFields,
        function(value, field) {
          return 'o_topic.' + field;
        }
      );

      // `fields` query
      if (!_.isEmpty(query.fields)) {
        let queryFieldsArr = query.fields.split(',');
        let queryFields = _.intersection(queryFieldsArr, topicFields);

        // Replace fields to display
        if (!_.isEmpty(queryFields)) {
          let queryDetailedFields = _.map(queryFields, function(value) {
            return 'o_topic.' + value;
          });
          sqlArr[0] = `
            SELECT ${queryDetailedFields.join(', ')}
            FROM m_subject_topic
            LEFT JOIN o_topic ON m_subject_topic.topic_id = o_topic.id
          `;
        }
      }

      // Set filters
      yield _.each(filterDetailedFields, function(value, field) {
        if (!_.isEmpty(value)) {
          filterArr.push(`${field} = ?`);
          params.push(value);
        }
      });
    }

    if (!_.isEmpty(filterArr)) {
      sqlArr.push('WHERE ' + filterArr.join(' AND '));
    }

    let sql = sqlArr.join(' ');
    return yield db.query(sql, params);
  },
  create: function * (params) {
    let createFields = [
      'name',
      'added_by'
    ];
    return yield db.query(`
      INSERT INTO o_subject(${createFields.join(', ')})
      VALUES(${_.times(createFields.length, _.constant('?')).join(', ')})
    `, [
      params.name,
      User.id
    ]);
  },
  createTopics: function * (params, id) {
    // Delete existing data
    yield db.query(
      `DELETE FROM m_subject_topic WHERE subject_id = ?`,
      [Number(id)]
    );

    if (!_.isEmpty(params.topics)) {
      let insertValues = yield _.map(params.topics, function(topicId) {
        return function * () {
          return `(${id}, ${yield Util.decrypt(topicId)}, ${User.id})`;
        };
      });
      return yield db.query(`
        INSERT INTO m_subject_topic(subject_id, topic_id, added_by) VALUES
        ${insertValues.join(', ')}
        `
      );
    }
  },
  update: function * (params, id) {
    let updateFields = [];
    let updateValues = [];
    let fields = _.pick(params, [
      'name',
      'status'
    ]);

    yield _.each(fields, function(field, key) {
      updateFields.push(`${key} = ?`);
      updateValues.push(field);
    });

    // Add `updated_by` field
    updateFields.push('updated_by = ?');
    updateValues.push(User.id);

    // Add Id
    updateValues.push(id);

    return yield db.query(`
      UPDATE o_subject SET ${updateFields.join(', ')}
      WHERE id = ?
    `, updateValues);
  }
};
