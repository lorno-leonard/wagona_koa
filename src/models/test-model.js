/* globals QuestionModel, User, Util */
import _ from 'lodash';
import db from '../lib/mysql';
import QuestionType from './../lib/question-type';
import moment from 'moment';

let getFields = [
  'id',
  'user_id',
  'subject_id',
  'class_id',
  'details',
  'date_ended'
];
let getTestQuestionFields = [
  'id',
  'details'
];

export let TestModel = {
  pager: {
    total: 0,
    numPages: 0,
    currentPage: 0
  },
  find: function * (query) {
    let sqlArr = [];
    let params = [];
    let filterArr = [];
    sqlArr.push(`SELECT ${getFields.join(', ')} FROM o_test`);
    if (!_.isUndefined(User)) {
      filterArr.push('user_id = ?');
      params.push(Number(User.id));
    }

    if (!_.isEmpty(query)) {
      let filterFields = _.pick(query, getFields);

      // `fields` query
      if (!_.isEmpty(query.fields)) {
        let queryFieldsArr = query.fields.split(',');
        let queryFields = _.intersection(queryFieldsArr, getFields);

        // Replace fields to display
        if (!_.isEmpty(queryFields)) {
          sqlArr[0] = `SELECT ${queryFields.join(', ')} FROM o_test`;
        }
      }

      // Set filters
      yield _.each(filterFields, function(value, field) {
        if (!_.isEmpty(value)) {
          filterArr.push(`${field} = ?`);
          params.push(value);
        }
      });
    }

    if (!_.isEmpty(filterArr)) {
      sqlArr.push('WHERE ' + filterArr.join(' AND '));
    }

    // `page` query
    if (!_.isEmpty(query) && _.has(query, 'page') && _.has(query, 'limit') && _.isNumber(Number(query.page)) && _.isNumber(Number(query.limit))) {
      let page = Number(query.page);
      let limit = Number(query.limit);
      let offset = limit * (page - 1);
      this.pager.currentPage = page;

      // Get total rows and number of pages
      let clonedSqlArr = _.clone(sqlArr);
      clonedSqlArr[0] = `SELECT COUNT(id) AS num_rows FROM o_test`;
      let sqlCount = clonedSqlArr.join(' ');
      let resultCount = yield db.query(sqlCount, params);
      let total = Number(_.first(resultCount).num_rows);
      this.pager.total = total;
      this.pager.numPages = Math.ceil(total / limit);

      // Get records
      sqlArr.push(`LIMIT ${offset}, ${limit}`);
      let sql = sqlArr.join(' ');
      return yield db.query(sql, params);
    }

    let sql = sqlArr.join(' ');
    return yield db.query(sql, params);
  },
  findOne: function * (id, query) {
    let sqlArr = [];
    let params = [];
    let filterArr = [];
    sqlArr.push(`SELECT ${getFields.join(', ')} FROM o_test`);
    filterArr.push('id = ?');
    params.push(Number(id));

    if (!_.isEmpty(query)) {
      let filterFields = _.pick(query, getFields);

      // `fields` query
      if (!_.isEmpty(query.fields)) {
        let queryFieldsArr = query.fields.split(',');
        let queryFields = _.intersection(queryFieldsArr, getFields);

        // Replace fields to display
        if (!_.isEmpty(queryFields)) {
          sqlArr[0] = `SELECT ${queryFields.join(', ')} FROM o_test`;
        }
      }

      // Set filters
      yield _.each(filterFields, function(value, field) {
        if (!_.isEmpty(value)) {
          filterArr.push(`${field} = ?`);
          params.push(value);
        }
      });
    }

    if (!_.isEmpty(filterArr)) {
      sqlArr.push('WHERE ' + filterArr.join(' AND '));
    }

    let sql = sqlArr.join(' ');
    let results = yield db.query(sql, params);
    return results.length > 0 ? _.first(results) : false;
  },
  findTestQuestions: function * (testId, query) {
    return yield db.query(`
      SELECT ${getTestQuestionFields.join(', ')}
      FROM o_test_questions
      WHERE test_id = ?
    `, [testId]);
  },
  create: function * (params) {
    let createFields = [
      'user_id',
      'subject_id',
      'date_started',
      'added_by',
      'date_added'
    ];

    let now = moment().unix() * 1000;
    let paramFields = [
      User.id,
      params.subject_id,
      moment().format('YYYY-MM-DD HH:mm:ss'),
      User.id,
      moment(now).format('YYYY-MM-DD HH:mm:ss')
    ];

    // Check if there's class_id
    if (_.has(params, 'class_id')) {
      createFields.push('class_id');
      paramFields.push(params.class_id);
    }

    // Insert Test
    let result = yield db.query(`
      INSERT INTO o_test(${createFields.join(', ')})
      VALUES(${_.times(createFields.length, _.constant('?')).join(', ')})
    `, paramFields
    );
    let insertId = result.insertId;
    let hash = yield Util.encryptAES(_.join([insertId, params.subject_id, User.id, now], '|'));

    // Get Questions
    yield db.query(`SET @rn := 0, @ng := NULL;`); // Set SQL variables
    let sqlQuestions = `
      SELECT
        *,
				@rn := if(topic_id != @ng, 1, @rn + 1) AS ng,
				@ng := topic_id
      FROM (
        SELECT
          m_topic_question.topic_id,
          o_topic.name AS topic_name,
          o_topic.is_paid AS topic_is_paid,
          m_topic_question.question_id,
          o_question.description,
          o_question.explanation,
          o_question.type,
          o_question.correct,
          o_question.is_paid,
          (
            SELECT COUNT(o_test_questions.id)
            FROM
              o_test_questions
            LEFT JOIN
              o_test ON o_test_questions.test_id = o_test.id
            WHERE
              question_id = m_topic_question.question_id
              AND o_test.date_ended IS NOT NULL
          ) AS question_taken
        FROM
          m_topic_question
        LEFT JOIN
          o_question ON m_topic_question.question_id = o_question.id
        LEFT JOIN
          o_topic ON m_topic_question.topic_id = o_topic.id
        WHERE
          m_topic_question.topic_id IN(` + params.topics.join(', ') + `)
          AND o_question.status = 1
        ORDER BY
          m_topic_question.topic_id,
          question_taken
      ) AS A
			ORDER BY
				ng,
				topic_id
      LIMIT 10
    `;
    let questions = yield db.query(sqlQuestions);

    // Insert Test Questions
    yield _.map(questions, function(question) {
      return function * () {
        let details = _.pick(question, [
          'topic_id',
          'topic_name',
          'topic_is_paid',
          'question_id',
          'description',
          'explanation',
          'type',
          'correct',
          'is_paid'
        ]);
        details.choices = yield QuestionModel.findChoices(question.question_id);
        yield _.map(details.choices, function(choice) {
          return function * () {
            // Encrypt Ids
            choice.id = yield Util.encrypt(choice.id.toString());
            choice.question_id = yield Util.encrypt(choice.question_id.toString()); // eslint-disable-line

            return choice;
          };
        });

        let createTestQuestionFields = [
          'test_id',
          'question_id',
          'topic_id',
          'details',
          'added_by'
        ];
        let paramTestQuestionFields = [
          insertId,
          question.question_id,
          question.topic_id,
          JSON.stringify(details),
          User.id
        ];
        yield db.query(`
          INSERT INTO o_test_questions(${createTestQuestionFields.join(', ')})
          VALUES(${_.times(createTestQuestionFields.length, _.constant('?')).join(', ')})
        `, paramTestQuestionFields
        );

        return question;
      };
    });

    return hash;
  },
  mark: function * (testId, questions) {
    let testDetails = {};
    let testQuestions = yield this.findTestQuestions(testId);

    // Check answers
    let numCorrect = 0;
    yield _.map(testQuestions, function(testQuestion) {
      return function * () {
        // Parse JSON
        let clonedDetails = JSON.parse(testQuestion.details);

        // Find Question answer
        let questionAnswer = _.filter(questions, {id: testQuestion.id})[0];

        // Check if answer is correct
        if (questionAnswer.type === QuestionType.CHOICE) {
          if (_.has(questionAnswer, 'selected')) {
            clonedDetails.selected = questionAnswer.selected;

            if (questionAnswer.selected === clonedDetails.correct) {
              numCorrect++;
              clonedDetails.is_correct = 1; // eslint-disable-line
            } else {
              clonedDetails.is_correct = 0; // eslint-disable-line
            }
          } else {
            clonedDetails.is_correct = 0; // eslint-disable-line
          }
        } else if (questionAnswer.type === QuestionType.FILL_IN) {
          // do something
        }

        // Update Test Question
        yield db.query(`
          UPDATE o_test_questions
          SET 
            is_correct = ?,
            details = ?,
            updated_by = ?
          WHERE id = ?
        `, [
          clonedDetails.is_correct,
          JSON.stringify(clonedDetails),
          User.id,
          testQuestion.id
        ]);

        return testQuestion;
      };
    });

    // Update Test
    testDetails.num_questions = testQuestions.length; // eslint-disable-line
    testDetails.num_correct = numCorrect; // eslint-disable-line
    yield db.query(`
      UPDATE o_test
      SET 
        details = ?,
        date_ended = ?,
        updated_by = ?
      WHERE id = ?
    `, [
      JSON.stringify(testDetails),
      moment().format('YYYY-MM-DD HH:mm:ss'),
      User.id,
      testId
    ]);
  }
};
