/* globals User, Util */
import _ from 'lodash';
import db from '../lib/mysql';

let getFields = [
  'id',
  'username',
  'password',
  'email',
  'role',
  'first_name',
  'last_name',
  'country_id',
  'status'
];
let searchFields = [
  'username',
  'email',
  'first_name',
  'last_name'
];

export let UserModel = {
  pager: {
    total: 0,
    numPages: 0,
    currentPage: 0
  },
  find: function * (query) {
    let sqlArr = [];
    let params = [];
    let filterArr = [];
    sqlArr.push(`SELECT ${getFields.join(', ')} FROM o_users`);

    if (!_.isEmpty(query)) {
      // Remove password field to be queried or filtered
      let clonedGetFields = _.clone(getFields);
      _.pull(clonedGetFields, 'password');

      let filterFields = _.pick(query, clonedGetFields);

      // `fields` query
      if (!_.isEmpty(query.fields)) {
        let queryFieldsArr = query.fields.split(',');
        let queryFields = _.intersection(queryFieldsArr, clonedGetFields);

        // Replace fields to display
        if (!_.isEmpty(queryFields)) {
          sqlArr[0] = `SELECT ${queryFields.join(', ')} FROM o_users`;
        }
      }

      // Set filters
      yield _.each(filterFields, function(value, field) {
        if (!_.isEmpty(value)) {
          filterArr.push(`${field} = ?`);
          params.push(value);
        }
      });

      // `search` query
      if (!_.isEmpty(query.search)) {
        let filterSearchArr = [];
        yield _.each(searchFields, function(field) {
          filterSearchArr.push(`${field} LIKE '%${query.search}%'`);
        });
        filterArr.push('(' + filterSearchArr.join(' OR ') + ')');
      }
    }

    if (!_.isEmpty(filterArr)) {
      sqlArr.push('WHERE ' + filterArr.join(' AND '));
    }

    // `page` query
    if (!_.isEmpty(query) && _.has(query, 'page') && _.has(query, 'limit') && _.isNumber(Number(query.page)) && _.isNumber(Number(query.limit))) {
      let page = Number(query.page);
      let limit = Number(query.limit);
      let offset = limit * (page - 1);
      this.pager.currentPage = page;

      // Get total rows and number of pages
      let clonedSqlArr = _.clone(sqlArr);
      clonedSqlArr[0] = `SELECT COUNT(id) AS num_rows FROM o_users`;
      let sqlCount = clonedSqlArr.join(' ');
      let resultCount = yield db.query(sqlCount, params);
      let total = Number(_.first(resultCount).num_rows);
      this.pager.total = total;
      this.pager.numPages = Math.ceil(total / limit);

      // Get records
      sqlArr.push(`LIMIT ${offset}, ${limit}`);
      let sql = sqlArr.join(' ');
      return yield db.query(sql, params);
    }

    let sql = sqlArr.join(' ');
    return yield db.query(sql, params);
  },
  findOne: function * (id, query) {
    let sqlArr = [];
    let params = [];
    let filterArr = [];
    sqlArr.push(`SELECT ${getFields.join(', ')} FROM o_users`);
    filterArr.push('id = ?');
    params.push(Number(id));

    if (!_.isEmpty(query)) {
      // Remove password field to be queried or filtered
      let clonedGetFields = _.clone(getFields);
      _.pull(clonedGetFields, 'password');

      let filterFields = _.pick(query, clonedGetFields);

      // `fields` query
      if (!_.isEmpty(query.fields)) {
        let queryFieldsArr = query.fields.split(',');
        let queryFields = _.intersection(queryFieldsArr, clonedGetFields);

        // Replace fields to display
        if (!_.isEmpty(queryFields)) {
          sqlArr[0] = `SELECT ${queryFields.join(', ')} FROM o_users`;
        }
      }

      // Set filters
      yield _.each(filterFields, function(value, field) {
        if (!_.isEmpty(value)) {
          filterArr.push(`${field} = ?`);
          params.push(value);
        }
      });
    }

    if (!_.isEmpty(filterArr)) {
      sqlArr.push('WHERE ' + filterArr.join(' AND '));
    }

    let sql = sqlArr.join(' ');
    let results = yield db.query(sql, params);
    return results.length > 0 ? _.first(results) : false;
  },
  findByUsername: function * (username, id) {
    let sql = `SELECT ${getFields.join(', ')} FROM o_users WHERE username = ?`;
    let params = [username];

    if (!_.isUndefined(id)) {
      sql += ' AND id != ?';
      params.push(Number(id));
    }

    let results = yield db.query(sql, params);
    return results.length > 0 ? _.first(results) : false;
  },
  findByEmail: function * (email, id) {
    let sql = `SELECT ${getFields.join(', ')} FROM o_users WHERE email = ?`;
    let params = [email];

    if (!_.isUndefined(id)) {
      sql += ' AND id != ?';
      params.push(Number(id));
    }

    let results = yield db.query(sql, params);
    return results.length > 0 ? _.first(results) : false;
  },
  findByFullname: function * (firstName, lastName, id) {
    let sql = `
      SELECT ${getFields.join(', ')} 
      FROM o_users 
      WHERE
        first_name = ?
        AND last_name = ?
    `;
    let params = [firstName, lastName];

    if (!_.isUndefined(id)) {
      sql += ' AND id != ?';
      params.push(Number(id));
    }

    let results = yield db.query(sql, params);
    return results.length > 0 ? _.first(results) : false;
  },
  create: function * (params) {
    let createFields = [
      'username',
      'password',
      'email',
      'role',
      'first_name',
      'last_name',
      'country_id',
      'added_by'
    ];
    return yield db.query(`
      INSERT INTO o_users(${createFields.join(', ')})
      VALUES(${_.times(createFields.length, _.constant('?')).join(', ')})
    `, [
      params.username,
      yield Util.bcryptHash(params.password),
      params.email,
      params.role,
      params.first_name,
      params.last_name,
      params.country_id,
      User.id || 0
    ]);
  },
  update: function * (params, id) {
    let updateFields = [];
    let updateValues = [];
    let fields = _.pick(params, [
      'username',
      'password',
      'email',
      'first_name',
      'last_name',
      'status'
    ]);

    yield _.each(fields, function(field, key) {
      if (!_.isEqual(key, 'password')) {
        updateFields.push(`${key} = ?`);
        updateValues.push(field);
      }
    });

    // Check if there's a password
    if (_.has(fields, 'password')) {
      updateFields.push('password = ?');
      updateValues.push(yield Util.bcryptHash(fields.password));
    }

    // Add `updated_by` field
    updateFields.push('updated_by = ?');
    updateValues.push(User.id);

    // Add Id
    updateValues.push(id);

    return yield db.query(`
      UPDATE o_users SET ${updateFields.join(', ')}
      WHERE id = ?
    `, updateValues);
  }
};
