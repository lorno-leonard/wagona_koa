/* globals User, Util */
import _ from 'lodash';
import db from '../lib/mysql';
import AccountType from '../lib/account-type';

let getFields = [
  'id',
  'name',
  'user_id',
  'num_students',
  'account_type',
  'reference',
  'verified',
  'profile',
  'status'
];

export let SchoolModel = {
  find: function * (query) {
    let sqlArr = [];
    let params = [];
    let filterArr = [];
    sqlArr.push(`SELECT ${getFields.join(', ')} FROM o_school`);

    if (!_.isEmpty(query)) {
      let filterFields = _.pick(query, getFields);

      // `fields` query
      if (!_.isEmpty(query.fields)) {
        let queryFieldsArr = query.fields.split(',');
        let queryFields = _.intersection(queryFieldsArr, getFields);

        // Replace fields to display
        if (!_.isEmpty(queryFields)) {
          sqlArr[0] = `SELECT ${queryFields.join(', ')} FROM o_school`;
        }
      }

      // Set filters
      yield _.each(filterFields, function(value, field) {
        if (!_.isEmpty(value)) {
          filterArr.push(`${field} = ?`);
          params.push(value);
        }
      });
    }

    if (!_.isEmpty(filterArr)) {
      sqlArr.push('WHERE ' + filterArr.join(' AND '));
    }

    let sql = sqlArr.join(' ');
    return yield db.query(sql, params);
  },
  findOne: function * (id, query) {
    let sqlArr = [];
    let params = [];
    let filterArr = [];
    sqlArr.push(`SELECT ${getFields.join(', ')} FROM o_school`);
    filterArr.push('id = ?');
    params.push(Number(id));

    if (!_.isEmpty(query)) {
      let filterFields = _.pick(query, getFields);

      // `fields` query
      if (!_.isEmpty(query.fields)) {
        let queryFieldsArr = query.fields.split(',');
        let queryFields = _.intersection(queryFieldsArr, getFields);

        // Replace fields to display
        if (!_.isEmpty(queryFields)) {
          sqlArr[0] = `SELECT ${queryFields.join(', ')} FROM o_school`;
        }
      }

      // Set filters
      yield _.each(filterFields, function(value, field) {
        if (!_.isEmpty(value)) {
          filterArr.push(`${field} = ?`);
          params.push(value);
        }
      });
    }

    if (!_.isEmpty(filterArr)) {
      sqlArr.push('WHERE ' + filterArr.join(' AND '));
    }

    let sql = sqlArr.join(' ');
    let results = yield db.query(sql, params);
    return results.length > 0 ? _.first(results) : false;
  },
  findByName: function * (name, id) {
    let sql = `SELECT ${getFields.join(', ')} FROM o_school WHERE name = ?`;
    let params = [name];

    if (!_.isUndefined(id)) {
      sql += ' AND id != ?';
      params.push(Number(id));
    }

    let results = yield db.query(sql, params);
    return results.length > 0 ? _.first(results) : false;
  },
  findSyllabus: function * (id, query) {
    let sqlArr = [];
    let params = [];
    let filterArr = [];
    let syllabiFields = [
      'id',
      'name',
      'status'
    ];
    let detailedFields = _.map(syllabiFields, function(value) {
      return 'o_syllabi.' + value;
    });
    sqlArr.push(`
      SELECT ${detailedFields.join(', ')}
      FROM m_school_syllabi
      LEFT JOIN o_syllabi ON m_school_syllabi.syllabi_id = o_syllabi.id
    `);
    filterArr.push('m_school_syllabi.school_id = ?');
    params.push(Number(id));

    if (!_.isEmpty(query)) {
      let filterFields = _.pick(query, syllabiFields);
      let filterDetailedFields = _.mapKeys(
        filterFields,
        function(value, field) {
          return 'o_syllabi.' + field;
        }
      );

      // `fields` query
      if (!_.isEmpty(query.fields)) {
        let queryFieldsArr = query.fields.split(',');
        let queryFields = _.intersection(queryFieldsArr, syllabiFields);

        // Replace fields to display
        if (!_.isEmpty(queryFields)) {
          let queryDetailedFields = _.map(queryFields, function(value) {
            return 'o_syllabi.' + value;
          });
          sqlArr[0] = `
            SELECT ${queryDetailedFields.join(', ')}
            FROM m_school_syllabi
            LEFT JOIN o_syllabi ON m_school_syllabi.syllabi_id = o_syllabi.id
          `;
        }
      }

      // Set filters
      yield _.each(filterDetailedFields, function(value, field) {
        if (!_.isEmpty(value)) {
          filterArr.push(`${field} = ?`);
          params.push(value);
        }
      });
    }

    if (!_.isEmpty(filterArr)) {
      sqlArr.push('WHERE ' + filterArr.join(' AND '));
    }

    let sql = sqlArr.join(' ');
    return yield db.query(sql, params);
  },
  create: function * (params) {
    let createFields = [
      'name',
      'user_id',
      'num_students',
      'account_type',
      'added_by'
    ];
    let paramFields = [
      params.name,
      yield Util.decrypt(params.user_id),
      params.num_students,
      AccountType[params.account_type],
      User.id
    ];

    if (_.has(params, 'verified')) {
      createFields.push('verified');
      paramFields.push(params.verified);
    }

    if (_.has(params, 'profile')) {
      createFields.push('profile');
      paramFields.push(params.profile);
    }

    return yield db.query(`
      INSERT INTO o_school(${createFields.join(', ')})
      VALUES(${_.times(createFields.length, _.constant('?')).join(', ')})
    `, paramFields
    );
  },
  createSyllabus: function * (params, id) {
    // Delete existing data
    yield db.query(
      `DELETE FROM m_school_syllabi WHERE school_id = ?`,
      [Number(id)]
    );

    let insertValues = yield _.map(params.syllabus, function(syllabiId) {
      return function * () {
        return `(${id}, ${yield Util.decrypt(syllabiId)}, ${User.id})`;
      };
    });
    return yield db.query(`
      INSERT INTO m_school_syllabi(school_id, syllabi_id, added_by) VALUES
      ${insertValues.join(' ')}
      `
    );
  },
  update: function * (params, id) {
    let updateFields = [];
    let updateValues = [];
    let pickFields = [
      'name',
      'num_students',
      'verified',
      'status'
    ];
    let fields = _.pick(params, pickFields);
    let otherDetails = _.omit(params, pickFields);

    yield _.each(fields, function(field, key) {
      updateFields.push(`${key} = ?`);
      updateValues.push(field);
    });

    // Add `profile` field
    if (!_.isEmpty(otherDetails)) {
      updateFields.push('profile = ?');
      updateValues.push(JSON.stringify(otherDetails));
    }

    // Add `updated_by` field
    updateFields.push('updated_by = ?');
    updateValues.push(User.id);

    // Add Id
    updateValues.push(id);

    return yield db.query(`
      UPDATE o_school SET ${updateFields.join(', ')}
      WHERE id = ?
    `, updateValues);
  }
};
