/* globals User, Util */
import _ from 'lodash';
import db from '../lib/mysql';
import QuestionType from '../lib/question-type';

let getFields = [
  'id',
  'description',
  'explanation',
  'type',
  'correct',
  'is_paid',
  'tags',
  'status'
];
let getChoiceFields = [
  'id',
  'question_id',
  'description',
  '`index`',
  'status'
];
let searchFields = [
  'description',
  'tags'
];

export let QuestionModel = {
  pager: {
    total: 0,
    numPages: 0,
    currentPage: 0
  },
  find: function * (query) {
    let sqlArr = [];
    let params = [];
    let filterArr = [];
    let hasChoicesFilter = false;
    sqlArr.push(`SELECT ${getFields.join(', ')} FROM o_question`);

    if (!_.isEmpty(query)) {
      let filterFields = _.pick(query, getFields);

      // `fields` query
      if (!_.isEmpty(query.fields)) {
        let queryFieldsArr = query.fields.split(',');
        let queryFields = _.intersection(queryFieldsArr, getFields);

        // Replace fields to display
        if (!_.isEmpty(queryFields)) {
          sqlArr[0] = `SELECT ${queryFields.join(', ')} FROM o_question`;
        }
      }

      // Set filters
      yield _.each(filterFields, function(value, field) {
        if (!_.isEmpty(value)) {
          filterArr.push(`${field} = ?`);

          if (_.isEqual(field, 'type')) {
            let type = _.has(QuestionType, value) ? QuestionType[value] : -1;
            params.push(type);
          } else {
            params.push(value);
          }
        }
      });

      // `choices` query
      hasChoicesFilter = _.has(query, 'choices');

      // `search` query
      if (!_.isEmpty(query.search)) {
        let filterSearchArr = [];
        yield _.each(searchFields, function(field) {
          filterSearchArr.push(`${field} LIKE '%${query.search}%'`);
        });
        filterArr.push('(' + filterSearchArr.join(' OR ') + ')');
      }
    }

    if (!_.isEmpty(filterArr)) {
      sqlArr.push('WHERE ' + filterArr.join(' AND '));
    }

    let sql;
    let results;
    // `page` query
    if (!_.isEmpty(query) && _.has(query, 'page') && _.has(query, 'limit') && _.isNumber(Number(query.page)) && _.isNumber(Number(query.limit))) {
      let page = Number(query.page);
      let limit = Number(query.limit);
      let offset = limit * (page - 1);
      this.pager.currentPage = page;

      // Get total rows and number of pages
      let clonedSqlArr = _.clone(sqlArr);
      clonedSqlArr[0] = `SELECT COUNT(id) AS num_rows FROM o_question`;
      let sqlCount = clonedSqlArr.join(' ');
      let resultCount = yield db.query(sqlCount, params);
      let total = Number(_.first(resultCount).num_rows);
      this.pager.total = total;
      this.pager.numPages = Math.ceil(total / limit);

      // Get records
      sqlArr.push(`LIMIT ${offset}, ${limit}`);
      sql = sqlArr.join(' ');
      results = yield db.query(sql, params);
    } else {
      sql = sqlArr.join(' ');
      results = yield db.query(sql, params);
    }

    if (hasChoicesFilter) {
      results = yield _.map(results, function(row) {
        return function * () {
          row.choices = yield db.query(`
            SELECT ${getChoiceFields.join(', ')} FROM o_question_choices
            WHERE question_id = ?
          `, [
            row.id
          ]);
          return row;
        };
      });
    }
    return results;
  },
  findOne: function * (id, query) {
    let sqlArr = [];
    let params = [];
    let filterArr = [];
    let hasChoicesFilter = false;
    sqlArr.push(`SELECT ${getFields.join(', ')} FROM o_question`);
    filterArr.push('id = ?');
    params.push(Number(id));

    if (!_.isEmpty(query)) {
      let filterFields = _.pick(query, getFields);

      // `fields` query
      if (!_.isEmpty(query.fields)) {
        let queryFieldsArr = query.fields.split(',');
        let queryFields = _.intersection(queryFieldsArr, getFields);

        // Replace fields to display
        if (!_.isEmpty(queryFields)) {
          sqlArr[0] = `SELECT ${queryFields.join(', ')} FROM o_question`;
        }
      }

      // Set filters
      yield _.each(filterFields, function(value, field) {
        if (!_.isEmpty(value)) {
          filterArr.push(`${field} = ?`);

          if (_.isEqual(field, 'type')) {
            let type = _.has(QuestionType, value) ? QuestionType[value] : -1;
            params.push(type);
          } else {
            params.push(value);
          }
        }
      });

      // `choices` query
      hasChoicesFilter = _.has(query, 'choices');
    }

    if (!_.isEmpty(filterArr)) {
      sqlArr.push('WHERE ' + filterArr.join(' AND '));
    }

    let sql = sqlArr.join(' ');
    let results = yield db.query(sql, params);
    if (hasChoicesFilter) {
      results = yield _.map(results, function(row) {
        return function * () {
          row.choices = yield db.query(`
            SELECT ${getChoiceFields.join(', ')} FROM o_question_choices
            WHERE question_id = ?
          `, [
            row.id
          ]);
          return row;
        };
      });
    }
    return results.length > 0 ? _.first(results) : false;
  },
  findChoices: function * (questionId, query) {
    let sqlArr = [];
    let params = [];
    let filterArr = [];
    sqlArr.push(`SELECT ${getChoiceFields.join(', ')} FROM o_question_choices`);
    filterArr.push('question_id = ?');
    params.push(Number(questionId));

    if (!_.isEmpty(query)) {
      let filterFields = _.pick(query, getChoiceFields);

      // `fields` query
      if (!_.isEmpty(query.fields)) {
        let queryFieldsArr = query.fields.split(',');
        let queryFields = _.intersection(queryFieldsArr, getChoiceFields);

        // Replace fields to display
        if (!_.isEmpty(queryFields)) {
          sqlArr[0] = `
            SELECT ${queryFields.join(', ')}
            FROM o_question_choices
          `;
        }
      }

      // Set filters
      yield _.each(filterFields, function(value, field) {
        if (!_.isEmpty(value)) {
          filterArr.push(`${field} = ?`);
          params.push(value);
        }
      });
    }

    if (!_.isEmpty(filterArr)) {
      sqlArr.push('WHERE ' + filterArr.join(' AND '));
    }

    let sql = sqlArr.join(' ');
    return yield db.query(sql, params);
  },
  findOneChoice: function * (id, query) {
    let sqlArr = [];
    let params = [];
    let filterArr = [];
    sqlArr.push(`SELECT ${getChoiceFields.join(', ')} FROM o_question_choices`);
    filterArr.push('id = ?');
    params.push(Number(id));

    if (!_.isEmpty(query)) {
      let filterFields = _.pick(query, getChoiceFields);

      // `fields` query
      if (!_.isEmpty(query.fields)) {
        let queryFieldsArr = query.fields.split(',');
        let queryFields = _.intersection(queryFieldsArr, getChoiceFields);

        // Replace fields to display
        if (!_.isEmpty(queryFields)) {
          sqlArr[0] = `
            SELECT ${queryFields.join(', ')}
            FROM o_question_choices
          `;
        }
      }

      // Set filters
      yield _.each(filterFields, function(value, field) {
        if (!_.isEmpty(value)) {
          filterArr.push(`${field} = ?`);
          params.push(value);
        }
      });
    }

    if (!_.isEmpty(filterArr)) {
      sqlArr.push('WHERE ' + filterArr.join(' AND '));
    }

    let sql = sqlArr.join(' ');
    let results = yield db.query(sql, params);
    return results.length > 0 ? _.first(results) : false;
  },
  findIn: function * (ids) {
    let numberIds = _.map(ids, function(id) {
      return Number(id);
    });
    return yield db.query(
      `SELECT ${getFields.join(', ')} FROM o_question WHERE id IN(${numberIds.join(', ')})`
    );
  },
  create: function * (params) {
    let createFields = [
      'description',
      'explanation',
      'type',
      'correct',
      'is_paid',
      'tags',
      'added_by'
    ];
    let tags = params.tags && _.isArray(params.tags) ? params.tags.join(',') : 'NULL'; // eslint-disable-line max-len
    let type = QuestionType[params.type];

    // Create question
    let results = yield db.query(`
      INSERT INTO o_question(${createFields.join(', ')})
      VALUES(${_.times(createFields.length, _.constant('?')).join(', ')})
    `, [
      params.description,
      params.explanation,
      type,
      params.correct,
      params.is_paid,
      tags,
      User.id
    ]);
    let insertId = results.insertId;

    // Create question choices
    let insertValues = yield _.map(params.choices, function(choice, key) {
      return function * () {
        return `(${insertId}, '${choice}', ${key}, ${User.id})`;
      };
    });
    let indexText = '`index`';
    yield db.query(`
      INSERT INTO o_question_choices
      (question_id, description, ${indexText}, added_by)
      VALUES
      ${insertValues.join(', ')}
      `
    );

    return results;
  },
  createChoice: function * (params) {
    let indexText = '`index`';
    let questionId = yield Util.decrypt(params.question_id.toString());
    let createFields = [
      'question_id',
      'description',
      `${indexText}`,
      'added_by'
    ];

    return yield db.query(`
      SET @indexNum = (SELECT MAX(${indexText}) + 1
      FROM o_question_choices WHERE question_id = ?);
      INSERT INTO o_question_choices(${createFields.join(', ')})
      VALUES(?, ?, @indexNum, ?);
    `, [
      questionId,
      questionId,
      params.description,
      User.id
    ]);
  },
  update: function * (params, id) {
    let updateFields = [];
    let updateValues = [];
    let fields = _.pick(params, [
      'description',
      'explanation',
      'correct',
      'is_paid',
      'tags',
      'status'
    ]);

    yield _.each(fields, function(field, key) {
      if (!_.isEqual(key, 'tags')) {
        updateFields.push(`${key} = ?`);
        updateValues.push(field);
      }
    });

    // Check if there are tags
    if (_.has(fields, 'tags')) {
      updateFields.push('tags = ?');
      updateValues.push(fields.tags.join(','));
    }

    // Add `updated_by` field
    updateFields.push('updated_by = ?');
    updateValues.push(User.id);

    // Add Id
    updateValues.push(id);

    // Check if there are choices
    if (_.has(params, 'choices')) {
      // Delete question choices
      yield db.query(`DELETE FROM o_question_choices WHERE question_id = ${id}`);

      // Create question choices
      let insertValues = yield _.map(params.choices, function(choice, key) {
        return function * () {
          return `(${id}, '${choice}', ${key}, ${User.id})`;
        };
      });
      let indexText = '`index`';
      yield db.query(`
        INSERT INTO o_question_choices
        (question_id, description, ${indexText}, added_by)
        VALUES
        ${insertValues.join(', ')}
        `
      );
    }

    return yield db.query(`
      UPDATE o_question SET ${updateFields.join(', ')}
      WHERE id = ?
    `, updateValues);
  },
  updateChoice: function * (params, id) {
    let updateFields = [];
    let updateValues = [];
    let fields = _.pick(params, [
      'description',
      'status'
    ]);

    yield _.each(fields, function(field, key) {
      updateFields.push(`${key} = ?`);
      updateValues.push(field);
    });

    // Add `updated_by` field
    updateFields.push('updated_by = ?');
    updateValues.push(User.id);

    // Add Id
    updateValues.push(id);

    return yield db.query(`
      UPDATE o_question_choices SET ${updateFields.join(', ')}
      WHERE id = ?
    `, updateValues);
  }
};
