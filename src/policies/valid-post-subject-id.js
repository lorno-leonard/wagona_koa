/* globals SubjectModel, Util, AppError */
import _ from 'lodash';

export let validPostSubjectId = function * (next) {
  let subjectId = this.request.body.subject_id;
  if (!subjectId) {
    throw new AppError('INVALID_ID', 'Subject Id is invalid.');
  }

  subjectId = yield Util.decrypt(subjectId.toString());
  if (_.isUndefined(subjectId)) {
    throw new AppError('INVALID_ID', 'Subject Id is invalid.');
  }

  let subject = yield SubjectModel.findOne(subjectId);
  if (!subject) {
    throw new AppError('INVALID_ID', 'Subject Id not found.');
  }

  yield next;
};
