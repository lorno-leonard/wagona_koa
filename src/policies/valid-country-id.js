/* globals CountryModel, AppError */

export let validCountryId = function * (next) {
  let id = this.params.id;
  let country = yield CountryModel.findOne(id);
  if (!country) {
    throw new AppError('INVALID_ID', 'Country Id not found.');
  }

  yield next;
};
