/* globals User, AppError */
import _ from 'lodash';
import Role from '../lib/role';

export let isRoleStudentIndividual = function * (next) {
  if (!_.includes([Role.STUDENT, Role.INDIVIDUAL], User.role)) {
    throw new AppError('UNAUTHORIZED', 'User role is invalid.');
  }

  yield next;
};
