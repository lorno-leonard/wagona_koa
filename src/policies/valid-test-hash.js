/* globals Util, AppError */
import _ from 'lodash';

export let validTestHash = function * (next) {
  let decrypt = yield Util.decryptAES(this.params.hash);

  if (_.isEmpty(decrypt)) {
    throw new AppError('INVALID_TEST', 'Test not found.');
  }

  yield next;
};
