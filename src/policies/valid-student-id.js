/* globals StudentModel, Util, AppError */
import _ from 'lodash';

export let validStudentId = function * (next) {
  let id = yield Util.decrypt(this.params.id.toString());
  if (_.isUndefined(id)) {
    throw new AppError('INVALID_ID', 'Student Id is invalid.');
  }

  let student = yield StudentModel.findOne(id);
  if (!student) {
    throw new AppError('INVALID_ID', 'Student Id not found.');
  }

  yield next;
};
