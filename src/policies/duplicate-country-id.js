/* globals CountryModel, AppError */

export let duplicateCountryId = function * (next) {
  let id = this.request.body.id;
  let country = yield CountryModel.findOne(id);
  if (country) {
    throw new AppError('CONFLICT', 'Country Id is already existing.');
  }

  yield next;
};
