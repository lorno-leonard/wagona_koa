/* globals UserModel, Util, AppError */
import _ from 'lodash';

export let duplicateUserUsername = function * (next) {
  let username = this.request.body.username;

  // Check if there is a username
  if (username) {
    let id;
    if (_.has(this.params, 'id')) {
      id = yield Util.decrypt(this.params.id.toString());
    }

    let user = yield UserModel.findByUsername(username, id);
    if (user) {
      throw new AppError('CONFLICT', 'Username is already existing.');
    }
  }

  yield next;
};
