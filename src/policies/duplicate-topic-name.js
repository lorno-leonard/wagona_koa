/* globals TopicModel, Util, AppError */
import _ from 'lodash';

export let duplicateTopicName = function * (next) {
  let name = this.request.body.name;

  // Check if there is a name
  if (name) {
    let id;
    if (_.has(this.params, 'id')) {
      id = yield Util.decrypt(this.params.id.toString());
    }

    let topic = yield TopicModel.findByName(name, id);
    if (topic) {
      throw new AppError('CONFLICT', 'Topic Name is already existing.');
    }
  }

  yield next;
};
