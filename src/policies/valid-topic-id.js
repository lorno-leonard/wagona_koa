/* globals TopicModel, Util, AppError */
import _ from 'lodash';

export let validTopicId = function * (next) {
  let id = yield Util.decrypt(this.params.id.toString());
  if (_.isUndefined(id)) {
    throw new AppError('INVALID_ID', 'Topic Id is invalid.');
  }

  let topic = yield TopicModel.findOne(id);
  if (!topic) {
    throw new AppError('INVALID_ID', 'Topic Id not found.');
  }

  yield next;
};
