/* globals QuestionModel, Util, AppError */
import _ from 'lodash';

export let validQuestionId = function * (next) {
  let id = yield Util.decrypt(this.params.id.toString());
  if (_.isUndefined(id)) {
    throw new AppError('INVALID_ID', 'Question Id is invalid.');
  }

  let question = yield QuestionModel.findOne(id);
  if (!question) {
    throw new AppError('INVALID_ID', 'Question Id not found.');
  }

  yield next;
};
