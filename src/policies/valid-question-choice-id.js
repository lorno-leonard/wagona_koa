/* globals QuestionModel, Util, AppError */
import _ from 'lodash';

export let validQuestionChoiceId = function * (next) {
  let id = yield Util.decrypt(this.params.id.toString());
  if (_.isUndefined(id)) {
    throw new AppError('INVALID_ID', 'Choice Id is invalid.');
  }

  let choice = yield QuestionModel.findOneChoice(id);
  if (!choice) {
    throw new AppError('INVALID_ID', 'Choice Id not found.');
  }

  yield next;
};
