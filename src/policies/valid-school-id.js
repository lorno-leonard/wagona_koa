/* globals SchoolModel, Util, AppError */
import _ from 'lodash';

export let validSchoolId = function * (next) {
  let id = yield Util.decrypt(this.params.id.toString());
  if (_.isUndefined(id)) {
    throw new AppError('INVALID_ID', 'School Id is invalid.');
  }

  let school = yield SchoolModel.findOne(id);
  if (!school) {
    throw new AppError('INVALID_ID', 'School Id not found.');
  }

  yield next;
};
