/* globals SubjectModel, Util, AppError */
import _ from 'lodash';

export let duplicateSubjectName = function * (next) {
  let name = this.request.body.name;

  // Check if there is a name
  if (name) {
    let id;
    if (_.has(this.params, 'id')) {
      id = yield Util.decrypt(this.params.id.toString());
    }

    let subject = yield SubjectModel.findByName(name, id);
    if (subject) {
      throw new AppError('CONFLICT', 'Subject Name is already existing.');
    }
  }

  yield next;
};
