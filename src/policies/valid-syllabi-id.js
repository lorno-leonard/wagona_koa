/* globals SyllabiModel, Util, AppError */
import _ from 'lodash';

export let validSyllabiId = function * (next) {
  let id = yield Util.decrypt(this.params.id.toString());
  if (_.isUndefined(id)) {
    throw new AppError('INVALID_ID', 'Syllabi Id is invalid.');
  }

  let syllabi = yield SyllabiModel.findOne(id);
  if (!syllabi) {
    throw new AppError('INVALID_ID', 'Syllabi Id not found.');
  }

  yield next;
};
