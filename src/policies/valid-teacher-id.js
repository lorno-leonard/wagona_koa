/* globals TeacherModel, Util, AppError */
import _ from 'lodash';

export let validTeacherId = function * (next) {
  let id = yield Util.decrypt(this.params.id.toString());
  if (_.isUndefined(id)) {
    throw new AppError('INVALID_ID', 'Teacher Id is invalid.');
  }

  let teacher = yield TeacherModel.findOne(id);
  if (!teacher) {
    throw new AppError('INVALID_ID', 'Teacher Id not found.');
  }

  yield next;
};
