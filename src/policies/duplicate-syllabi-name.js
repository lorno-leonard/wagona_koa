/* globals SyllabiModel, Util, AppError */
import _ from 'lodash';

export let duplicateSyllabiName = function * (next) {
  let name = this.request.body.name;

  // Check if there is a name
  if (name) {
    let id;
    if (_.has(this.params, 'id')) {
      id = yield Util.decrypt(this.params.id.toString());
    }

    let syllabi = yield SyllabiModel.findByName(name, id);
    if (syllabi) {
      throw new AppError('CONFLICT', 'Syllabi Name is already existing.');
    }
  }

  yield next;
};
