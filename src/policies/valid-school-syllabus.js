/* globals SyllabiModel, Util, AppError */
import _ from 'lodash';

export let validSchoolSyllabus = function * (next) {
  let syllabiIds = this.request.body.syllabus;
  if (!syllabiIds) {
    throw new AppError('FORBIDDEN', 'Parameter `syllabus` is required.');
  }

  if (!_.isArray(syllabiIds)) {
    throw new AppError(
      'FORBIDDEN',
      'Parameter `syllabus` should be an array.'
    );
  }

  if (_.isEmpty(syllabiIds)) {
    throw new AppError('FORBIDDEN', 'Parameter `syllabus` is empty.');
  }

  let decryptedSyllabiIds = yield _.map(syllabiIds, function(id) {
    return function * () {
      // Decrypt Syllabi Id
      return yield Util.decrypt(id.toString());
    };
  });
  let syllabus = yield SyllabiModel.findIn(decryptedSyllabiIds);
  if (!_.isEqual(syllabiIds.length, syllabus.length)) {
    throw new AppError('INVALID_ID', 'Some Syllabi Ids are not found.');
  }

  yield next;
};
