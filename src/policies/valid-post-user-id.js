/* globals UserModel, Util, AppError */
import _ from 'lodash';

export let validPostUserId = function * (next) {
  let userId = this.request.body.user_id;
  if (!userId) {
    throw new AppError('INVALID_ID', 'User Id is invalid.');
  }

  userId = yield Util.decrypt(userId.toString());
  if (_.isUndefined(userId)) {
    throw new AppError('INVALID_ID', 'User Id is invalid.');
  }

  let user = yield UserModel.findOne(userId);
  if (!user) {
    throw new AppError('INVALID_ID', 'User Id not found.');
  }

  yield next;
};
