/* globals AppError */
import _ from 'lodash';
import QuestionType from '../lib/question-type';

export let validQuestionType = function * (next) {
  let type = this.request.body.type;
  if (_.isUndefined(type) || !_.has(QuestionType, type)) {
    throw new AppError('FORBIDDEN', 'Question Type is invalid.');
  }

  yield next;
};
