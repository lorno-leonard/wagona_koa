/* globals TopicModel, Util, AppError */
import _ from 'lodash';

export let validSubjectTopics = function * (next) {
  let topicIds = this.request.body.topics;
  if (!topicIds) {
    throw new AppError('FORBIDDEN', 'Parameter `topics` is required.');
  }

  if (!_.isArray(topicIds)) {
    throw new AppError('FORBIDDEN', 'Parameter `topics` should be an array.');
  }

  if (!_.isEmpty(topicIds)) {
    let decryptedTopicIds = yield _.map(topicIds, function(id) {
      return function * () {
        // Decrypt Topic Id
        return yield Util.decrypt(id.toString());
      };
    });
    let topics = yield TopicModel.findIn(decryptedTopicIds);
    if (!_.isEqual(topicIds.length, topics.length)) {
      throw new AppError('INVALID_ID', 'Some Topic Ids are not found.');
    }
  }

  yield next;
};
