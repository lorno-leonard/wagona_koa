/* globals SchoolModel, Util, AppError */
import _ from 'lodash';

export let duplicateSchoolName = function * (next) {
  let name = this.request.body.name;

  // Check if there is a name
  if (name) {
    let id;
    if (_.has(this.params, 'id')) {
      id = yield Util.decrypt(this.params.id.toString());
    }

    let school = yield SchoolModel.findByName(name, id);
    if (school) {
      throw new AppError('CONFLICT', 'School Name is already existing.');
    }
  }

  yield next;
};
