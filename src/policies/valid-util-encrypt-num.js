/* globals AppError */
import _ from 'lodash';

export let validUtilEncryptNum = function * (next) {
  let num = this.params.num;
  if (!_.isUndefined(num) && _.isNaN(Number(num))) {
    throw new AppError('FORBIDDEN', 'Number parameter is not a number.');
  }

  yield next;
};
