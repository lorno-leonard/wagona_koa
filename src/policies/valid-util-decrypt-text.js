/* globals AppError */
import _ from 'lodash';

export let validUtilDecryptText = function * (next) {
  let text = this.params.text;
  if (!_.isUndefined(text) && !_.isNaN(Number(text))) {
    throw new AppError('FORBIDDEN', 'Text parameter is not a string.');
  }

  yield next;
};
