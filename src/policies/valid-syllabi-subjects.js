/* globals SubjectModel, Util, AppError */
import _ from 'lodash';

export let validSyllabiSubjects = function * (next) {
  let subjectIds = this.request.body.subjects;
  if (!subjectIds) {
    throw new AppError('FORBIDDEN', 'Parameter `subjects` is required.');
  }

  if (!_.isArray(subjectIds)) {
    throw new AppError('FORBIDDEN', 'Parameter `subjects` should be an array.');
  }

  if (!_.isEmpty(subjectIds)) {
    let decryptedSubjectIds = yield _.map(subjectIds, function(id) {
      return function * () {
        // Decrypt Subject Id
        return yield Util.decrypt(id.toString());
      };
    });
    let subjects = yield SubjectModel.findIn(decryptedSubjectIds);
    if (!_.isEqual(subjectIds.length, subjects.length)) {
      throw new AppError('INVALID_ID', 'Some Subject Ids are not found.');
    }
  }

  yield next;
};
