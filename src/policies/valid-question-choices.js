/* globals AppError */
import _ from 'lodash';

export let validQuestionChoices = function * (next) {
  let choices = this.request.body.choices;
  if (!choices) {
    throw new AppError('FORBIDDEN', 'Parameter `choices` is required.');
  }

  if (!_.isArray(choices)) {
    throw new AppError('FORBIDDEN', 'Parameter `choices` should be an array.');
  }

  if (_.isEmpty(choices)) {
    throw new AppError('FORBIDDEN', 'Parameter `choices` is empty.');
  }

  yield next;
};
