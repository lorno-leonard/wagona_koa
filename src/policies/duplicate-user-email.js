/* globals UserModel, Util, AppError */
import _ from 'lodash';

export let duplicateUserEmail = function * (next) {
  let email = this.request.body.email;

  // Check if there is an email
  if (email) {
    let id;
    if (_.has(this.params, 'id')) {
      id = yield Util.decrypt(this.params.id.toString());
    }

    let user = yield UserModel.findByEmail(email, id);
    if (user) {
      throw new AppError('CONFLICT', 'Email Address is already existing.');
    }
  }

  yield next;
};
