/* globals ClassModel, Util, AppError */
import _ from 'lodash';

export let validClassId = function * (next) {
  let id = yield Util.decrypt(this.params.id.toString());
  if (_.isUndefined(id)) {
    throw new AppError('INVALID_ID', 'Class Id is invalid.');
  }

  let _class = yield ClassModel.findOne(id);
  if (!_class) {
    throw new AppError('INVALID_ID', 'Class Id not found.');
  }

  yield next;
};
