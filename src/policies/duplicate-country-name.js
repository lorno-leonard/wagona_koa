/* globals CountryModel, AppError */
import _ from 'lodash';

export let duplicateCountryName = function * (next) {
  let name = this.request.body.name;

  // Check if there is a name
  if (name) {
    let id;
    if (_.has(this.params, 'id')) {
      id = this.params.id;
    }

    let country = yield CountryModel.findByName(name, id);
    if (country) {
      throw new AppError('CONFLICT', 'Country Name is already existing.');
    }
  }

  yield next;
};
