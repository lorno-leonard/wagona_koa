/* globals SubjectModel, Util, AppError */
import _ from 'lodash';

export let validSubjectId = function * (next) {
  let id = yield Util.decrypt(this.params.id.toString());
  if (_.isUndefined(id)) {
    throw new AppError('INVALID_ID', 'Subject Id is invalid.');
  }

  let subject = yield SubjectModel.findOne(id);
  if (!subject) {
    throw new AppError('INVALID_ID', 'Subject Id not found.');
  }

  yield next;
};
