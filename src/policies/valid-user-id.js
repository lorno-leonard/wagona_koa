/* globals UserModel, Util, AppError */
import _ from 'lodash';

export let validUserId = function * (next) {
  let id = yield Util.decrypt(this.params.id.toString());
  if (_.isUndefined(id)) {
    throw new AppError('INVALID_ID', 'User Id is invalid.');
  }

  let user = yield UserModel.findOne(id);
  if (!user) {
    throw new AppError('INVALID_ID', 'User Id not found.');
  }

  yield next;
};
