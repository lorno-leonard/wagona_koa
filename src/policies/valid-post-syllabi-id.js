/* globals SyllabiModel, Util, AppError */
import _ from 'lodash';

export let validPostSyllabiId = function * (next) {
  let syllabiId = this.request.body.syllabi_id;
  if (!syllabiId) {
    throw new AppError('INVALID_ID', 'Syllabi Id is invalid.');
  }

  syllabiId = yield Util.decrypt(syllabiId.toString());
  if (_.isUndefined(syllabiId)) {
    throw new AppError('INVALID_ID', 'Syllabi Id is invalid.');
  }

  let syllabi = yield SyllabiModel.findOne(syllabiId);
  if (!syllabi) {
    throw new AppError('INVALID_ID', 'Syllabi Id not found.');
  }

  yield next;
};
