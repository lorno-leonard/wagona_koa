/* globals QuestionModel, Util, AppError */
import _ from 'lodash';

export let validTopicQuestions = function * (next) {
  let questionIds = this.request.body.questions;
  if (!questionIds) {
    throw new AppError('FORBIDDEN', 'Parameter `questions` is required.');
  }

  if (!_.isArray(questionIds)) {
    throw new AppError('FORBIDDEN', 'Parameter `questions` should be an array.');
  }

  if (!_.isEmpty(questionIds)) {
    let decryptedQuestionIds = yield _.map(questionIds, function(id) {
      return function * () {
        // Decrypt Question Id
        return yield Util.decrypt(id.toString());
      };
    });
    let questions = yield QuestionModel.findIn(decryptedQuestionIds);
    if (!_.isEqual(questionIds.length, questions.length)) {
      throw new AppError('INVALID_ID', 'Some Question Ids are not found.');
    }
  }

  yield next;
};
