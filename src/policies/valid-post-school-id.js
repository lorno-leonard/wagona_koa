/* globals SchoolModel, Util, AppError */
import _ from 'lodash';

export let validPostSchoolId = function * (next) {
  let schoolId = this.request.body.school_id;
  if (!schoolId) {
    throw new AppError('INVALID_ID', 'School Id is invalid.');
  }

  schoolId = yield Util.decrypt(schoolId.toString());
  if (_.isUndefined(schoolId)) {
    throw new AppError('INVALID_ID', 'School Id is invalid.');
  }

  let school = yield SchoolModel.findOne(schoolId);
  if (!school) {
    throw new AppError('INVALID_ID', 'School Id not found.');
  }

  yield next;
};
