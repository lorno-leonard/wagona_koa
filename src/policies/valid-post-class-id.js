/* globals ClassModel, Util, AppError */
import _ from 'lodash';

export let validPostClassId = function * (next) {
  let classId = this.request.body.class_id;
  if (!classId) {
    throw new AppError('INVALID_ID', 'Class Id is invalid.');
  }

  classId = yield Util.decrypt(classId.toString());
  if (_.isUndefined(classId)) {
    throw new AppError('INVALID_ID', 'Class Id is invalid.');
  }

  let _class = yield ClassModel.findOne(classId);
  if (!_class) {
    throw new AppError('INVALID_ID', 'Class Id not found.');
  }

  yield next;
};
