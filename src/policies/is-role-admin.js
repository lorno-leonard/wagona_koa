/* globals User, AppError */
import _ from 'lodash';
import Role from '../lib/role';

export let isRoleAdmin = function * (next) {
  if (!_.isEqual(User.role, Role.ADMIN)) {
    throw new AppError('UNAUTHORIZED', 'User role is invalid.');
  }

  yield next;
};
