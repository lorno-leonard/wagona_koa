/* globals AppError */
import _ from 'lodash';

export let validUtilEncryptLength = function * (next) {
  let length = this.params.length;
  if (!_.isUndefined(length) && _.isNaN(Number(length))) {
    throw new AppError('FORBIDDEN', 'Length parameter is not a number.');
  }

  if (Number(length) < 3) {
    throw new AppError('FORBIDDEN', 'Length parameter is less than 3.');
  }

  yield next;
};
