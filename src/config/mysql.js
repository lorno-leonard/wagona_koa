let env = process.env;

export default {
  host: env.MYSQL_HOST,
  user: env.MYSQL_USER,
  password: env.MYSQL_PASSWORD,
  port: Number(env.MYSQL_PORT) || 3306,
  database: env.MYSQL_DATABASE,
  connectionLimit: Number(env.MYSQL_CONNECTION_LIMIT) || 10,
  waitForConnections: true,
  acquireTimeout: 5000,
  multipleStatements: true
};
