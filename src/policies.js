export default {
  UtilController: {
    encrypt: [
      'validUtilEncryptLength',
      'validUtilEncryptNum'
    ],
    decrypt: [
      'validUtilEncryptLength',
      'validUtilDecryptText'
    ]
  },
  UserController: {
    find: [
      'validCredentials',
      'isRoleAdmin'
    ],
    findOne: [
      'validCredentials',
      'isRoleAdmin',
      'validUserId'
    ],
    create: [
      'validCredentials',
      'isRoleAdmin',
      'duplicateUserUsername',
      'duplicateUserEmail'
    ],
    update: [
      'validCredentials',
      'isRoleAdmin',
      'validUserId',
      'duplicateUserUsername',
      'duplicateUserEmail'
    ]
  },
  CountryController: {
    find: [
      'validCredentials',
      'isRoleAdmin'
    ],
    findOne: [
      'validCredentials',
      'isRoleAdmin',
      'validCountryId'
    ],
    create: [
      'validCredentials',
      'isRoleAdmin',
      'duplicateCountryId',
      'duplicateCountryName'
    ],
    update: [
      'validCredentials',
      'isRoleAdmin',
      'validCountryId',
      'duplicateCountryId',
      'duplicateCountryName'
    ]
  },
  SyllabiController: {
    find: [
      'validCredentials',
      'isRoleAdmin'
    ],
    findOne: [
      'validCredentials',
      'isRoleAdmin',
      'validSyllabiId'
    ],
    findSubjects: [
      'validCredentials',
      'isRoleAdmin',
      'validSyllabiId'
    ],
    create: [
      'validCredentials',
      'isRoleAdmin',
      'duplicateSyllabiName'
    ],
    createSubjects: [
      'validCredentials',
      'isRoleAdmin',
      'validSyllabiId',
      'validSyllabiSubjects'
    ],
    update: [
      'validCredentials',
      'isRoleAdmin',
      'validSyllabiId',
      'duplicateSyllabiName'
    ]
  },
  SubjectController: {
    find: [
      'validCredentials',
      'isRoleAdmin'
    ],
    findOne: [
      'validCredentials',
      'isRoleAdmin',
      'validSubjectId'
    ],
    findTopics: [
      'validCredentials',
      'isRoleAdmin',
      'validSubjectId'
    ],
    create: [
      'validCredentials',
      'isRoleAdmin',
      'duplicateSubjectName'
    ],
    createTopics: [
      'validCredentials',
      'isRoleAdmin',
      'validSubjectId',
      'validPostTopics'
    ],
    update: [
      'validCredentials',
      'isRoleAdmin',
      'validSubjectId',
      'duplicateSubjectName'
    ]
  },
  TopicController: {
    find: [
      'validCredentials',
      'isRoleAdmin'
    ],
    findOne: [
      'validCredentials',
      'isRoleAdmin',
      'validTopicId'
    ],
    findQuestions: [
      'validCredentials',
      'isRoleAdmin',
      'validTopicId'
    ],
    create: [
      'validCredentials',
      'isRoleAdmin',
      'duplicateTopicName'
    ],
    createQuestions: [
      'validCredentials',
      'isRoleAdmin',
      'validTopicId',
      'validTopicQuestions'
    ],
    update: [
      'validCredentials',
      'isRoleAdmin',
      'validTopicId',
      'duplicateTopicName'
    ]
  },
  QuestionController: {
    find: [
      'validCredentials',
      'isRoleAdmin'
    ],
    findOne: [
      'validCredentials',
      'isRoleAdmin',
      'validQuestionId'
    ],
    findChoices: [
      'validCredentials',
      'isRoleAdmin',
      'validQuestionId'
    ],
    findOneChoice: [
      'validCredentials',
      'isRoleAdmin',
      'validQuestionChoiceId'
    ],
    create: [
      'validCredentials',
      'isRoleAdmin',
      'validQuestionType',
      'validQuestionChoices'
    ],
    createChoice: [
      'validCredentials',
      'isRoleAdmin',
      'validQuestionId'
    ],
    update: [
      'validCredentials',
      'isRoleAdmin',
      'validQuestionId'
    ],
    updateChoice: [
      'validCredentials',
      'isRoleAdmin',
      'validQuestionChoiceId'
    ]
  },
  SchoolController: {
    find: ['validCredentials'],
    findOne: [
      'validCredentials',
      'validSchoolId'
    ],
    findSyllabus: [
      'validCredentials',
      'validSchoolId'
    ],
    create: [
      'validCredentials',
      'validPostUserId',
      'duplicateSchoolName'
    ],
    createSyllabus: [
      'validCredentials',
      'validSchoolId',
      'validSchoolSyllabus'
    ],
    update: [
      'validCredentials',
      'validSchoolId',
      'duplicateSchoolName'
    ]
  },
  TeacherController: {
    find: ['validCredentials'],
    findOne: [
      'validCredentials',
      'validTeacherId'
    ],
    create: [
      'validCredentials',
      'validPostUserId'
    ],
    update: [
      'validCredentials',
      'validTeacherId'
    ]
  },
  ClassController: {
    find: ['validCredentials'],
    findOne: [
      'validCredentials',
      'validClassId'
    ],
    create: [
      'validCredentials',
      'validPostSchoolId',
      'validPostSyllabiId',
      'validPostSubjectId'
    ],
    update: [
      'validCredentials',
      'validClassId'
    ]
  },
  StudentController: {
    find: ['validCredentials'],
    findOne: [
      'validCredentials',
      'validStudentId'
    ],
    create: [
      'validCredentials',
      'validPostUserId',
      'validPostSchoolId',
      'validPostSyllabiId',
      'validPostClassId'
    ],
    update: [
      'validCredentials',
      'validStudentId'
    ]
  },
  SignupController: {
    createSchool: [
      'duplicateUserUsername',
      'duplicateUserEmail',
      'duplicateSchoolName'
    ],
    createUser: [
      'duplicateUserUsername',
      'duplicateUserEmail'
    ]
  },
  AccountController: {
    findSyllabi: [
      'validCredentials',
      'isRoleStudentIndividual'
    ],
    findSyllabiSubjects: [
      'validCredentials',
      'validSyllabiId',
      'isRoleStudentIndividual'
    ],
    findSubjectTests: [
      'validCredentials',
      'validSubjectId',
      'isRoleStudentIndividual'
    ],
    findSubjectTopics: [
      'validCredentials',
      'validSubjectId',
      'isRoleStudentIndividual'
    ],
    updateStudent: [
      'validCredentials',
      'validPostSyllabiId',
      'isRoleStudentIndividual'
    ]
  },
  TestController: {
    findTestQuestions: [
      'validCredentials',
      'validTestHash'
    ],
    create: [
      'validCredentials',
      'validPostSubjectId',
      'validPostTopics'
    ],
    mark: [
      'validCredentials',
      'validTestHash'
    ]
  }
};
