export default {
  // Page
  'GET /': 'PageController.index',
  'GET /login': 'PageController.login',
  'GET /register': 'PageController.register',
  'GET /pricing': 'PageController.pricing',
  'GET /account': 'PageController.account',
  'GET /account/profile': 'PageController.accountProfile',
  'GET /test/:hash': 'PageController.test',
  'GET /test/:hash/history': 'PageController.testHistory',
  'GET /admin/': 'PageController.admin',
  'GET /uploads/:file': 'PageController.uploads',
  'GET /sample-landing': 'PageController.sampleLanding',

  // Utility API
  'GET /api/util/bcrypt/:text': 'UtilController.bcrypt',
  'GET /api/util/encrypt/:num': 'UtilController.encrypt',
  'GET /api/util/encrypt/:num/:length': 'UtilController.encrypt',
  'GET /api/util/decrypt/:text': 'UtilController.decrypt',
  'GET /api/util/decrypt/:text/:length': 'UtilController.decrypt',

  // Signup API
  'GET /api/signup/country': 'SignupController.findCountry',
  'POST /api/signup/school': 'SignupController.createSchool',
  'POST /api/signup/home': 'SignupController.createUser',

  // Account API
  'GET /api/account/syllabi': 'AccountController.findSyllabi',
  'GET /api/account/syllabi/:id/subject': 'AccountController.findSyllabiSubjects',
  'GET /api/account/subject/:id/test': 'AccountController.findSubjectTests',
  'GET /api/account/subject/:id/topic': 'AccountController.findSubjectTopics',
  'PATCH /api/account/student/:id': 'AccountController.updateStudent',

  // Test API
  'POST /api/test': 'TestController.create',
  'GET /api/test/:hash/question': 'TestController.findTestQuestions',
  'POST /api/test/:hash/mark': 'TestController.mark',

  // Payment API
  'GET /api/payment/pricing': 'PaymentController.findPricing',

  // Authentication API
  'POST /api/login': 'AuthController.login',
  'POST /api/logout': 'AuthController.logout',

  // Users API
  'GET /api/users': 'UserController.find',
  'GET /api/users/:id': 'UserController.findOne',
  'POST /api/users': 'UserController.create',
  'PATCH /api/users/:id': 'UserController.update',

  // Country API
  'GET /api/country': 'CountryController.find',
  'GET /api/country/:id': 'CountryController.findOne',
  'POST /api/country': 'CountryController.create',
  'PATCH /api/country/:id': 'CountryController.update',

  // Syllabi API
  'GET /api/syllabi': 'SyllabiController.find',
  'GET /api/syllabi/:id': 'SyllabiController.findOne',
  'GET /api/syllabi/:id/subject': 'SyllabiController.findSubjects',
  'POST /api/syllabi': 'SyllabiController.create',
  'POST /api/syllabi/:id/subject': 'SyllabiController.createSubjects',
  'PATCH /api/syllabi/:id': 'SyllabiController.update',

  // Subject API
  'GET /api/subject': 'SubjectController.find',
  'GET /api/subject/:id': 'SubjectController.findOne',
  'GET /api/subject/:id/topic': 'SubjectController.findTopics',
  'POST /api/subject': 'SubjectController.create',
  'POST /api/subject/:id/topic': 'SubjectController.createTopics',
  'PATCH /api/subject/:id': 'SubjectController.update',

  // Topic API
  'GET /api/topic': 'TopicController.find',
  'GET /api/topic/:id': 'TopicController.findOne',
  'GET /api/topic/:id/question': 'TopicController.findQuestions',
  'POST /api/topic': 'TopicController.create',
  'POST /api/topic/:id/question': 'TopicController.createQuestions',
  'PATCH /api/topic/:id': 'TopicController.update',

  // Question API
  'GET /api/question': 'QuestionController.find',
  'GET /api/question/:id': 'QuestionController.findOne',
  'GET /api/question/:id/choice': 'QuestionController.findChoices',
  'GET /api/question/choice/:id': 'QuestionController.findOneChoice',
  'GET /api/choice/:id': 'QuestionController.findOneChoice',
  'POST /api/question': 'QuestionController.create',
  'POST /api/question/:id/choice': 'QuestionController.createChoice',
  'PATCH /api/question/:id': 'QuestionController.update',
  'PATCH /api/question/choice/:id': 'QuestionController.updateChoice',
  'PATCH /api/choice/:id': 'QuestionController.updateChoice',

  // School API
  'GET /api/school': 'SchoolController.find',
  'GET /api/school/:id': 'SchoolController.findOne',
  'GET /api/school/:id/syllabi': 'SchoolController.findSyllabus',
  'POST /api/school': 'SchoolController.create',
  'POST /api/school/:id/syllabi': 'SchoolController.createSyllabus',
  'PATCH /api/school/:id': 'SchoolController.update',

  // Teacher API
  'GET /api/teacher': 'TeacherController.find',
  'GET /api/teacher/:id': 'TeacherController.findOne',
  'POST /api/teacher': 'TeacherController.create',
  'PATCH /api/teacher/:id': 'TeacherController.update',

  // Class API
  'GET /api/class': 'ClassController.find',
  'GET /api/class/:id': 'ClassController.findOne',
  'POST /api/class': 'ClassController.create',
  'PATCH /api/class/:id': 'ClassController.update',

  // Student API
  'GET /api/student': 'StudentController.find',
  'GET /api/student/:id': 'StudentController.findOne',
  'POST /api/student': 'StudentController.create',
  'PATCH /api/student/:id': 'StudentController.update'
};
