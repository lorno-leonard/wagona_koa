/* globals app, Util */
/* eslint max-nested-callbacks: ["error", 6]*/
import {expect} from 'chai';
import _ from 'lodash';
import Role from '../lib/role';
import AccountType from '../lib/account-type';
import QuestionType from '../lib/question-type';

let request;
let num = 456123;
let text = 'qwerty';
let encryptedText;
let encryptedTextLength;

describe('Utility API', function() {
  this.timeout(20000);

  before(function * () {
    yield app.started;
    request = require('supertest')(app.server);
  });

  after(function * () {
    app.server.close();
  });

  describe('GET /api/util/bcrypt/:text', function() {
    it('should fail', function * () {
      let hash;

      yield request
        .get('/api/util/bcrypt/' + text)
        .expect(200)
        .expect(function(res) {
          hash = res.text;
        });

      expect(yield Util.bcryptCompare(text + '`', hash)).to.be.false; // eslint-disable-line no-unused-expressions
    });

    it('should succeed', function * () {
      let hash;

      yield request
        .get('/api/util/bcrypt/' + text)
        .expect(200)
        .expect(function(res) {
          hash = res.text;
        });

      expect(yield Util.bcryptCompare(text, hash)).to.be.true; // eslint-disable-line no-unused-expressions
    });
  });

  describe('GET /api/util/encrypt/:num', function() {
    it('should fail - passed parameter is not a number', function * () {
      yield request
        .get('/api/util/encrypt/' + text)
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('FORBIDDEN');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Number parameter is not a number.');
        });
    });

    it('should fail - different hashes compared', function * () {
      let hash;

      yield request
        .get('/api/util/encrypt/' + num)
        .expect(200)
        .expect(function(res) {
          hash = res.text;
        });

      let result = yield Util.encrypt(num + 1);
      expect(_.isEqual(result, hash)).to.be.false; // eslint-disable-line no-unused-expressions
    });

    it('should succeed', function * () {
      let hash;

      yield request
        .get('/api/util/encrypt/' + num)
        .expect(200)
        .expect(function(res) {
          hash = res.text;
        });

      let result = yield Util.encrypt(num);
      expect(_.isEqual(result, hash)).to.be.true; // eslint-disable-line no-unused-expressions
      encryptedText = hash;
    });
  });

  describe('GET /api/util/encrypt/:num/:length', function() {
    it('should fail - passed parameter is not a number', function * () {
      let length = 5;

      yield request
        .get('/api/util/encrypt/' + text + '/' + length)
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('FORBIDDEN');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Number parameter is not a number.');
        });
    });

    it('should fail - different hashes compared', function * () {
      let length = 5;
      let hash;

      yield request
        .get('/api/util/encrypt/' + num + '/' + length)
        .expect(200)
        .expect(function(res) {
          hash = res.text;
        });

      let result = yield Util.encrypt(num + 1);
      expect(_.isEqual(result, hash)).to.be.false; // eslint-disable-line no-unused-expressions
    });

    it('should fail - length parameter is not a number', function * () {
      let length = 'asdf';

      yield request
        .get('/api/util/encrypt/' + num + '/' + length)
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('FORBIDDEN');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Length parameter is not a number.');
        });
    });

    it('should fail - length parameter less than 3', function * () {
      let length = 2;

      yield request
        .get('/api/util/encrypt/' + num + '/' + length)
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('FORBIDDEN');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Length parameter is less than 3.');
        });
    });

    it('should succeed', function * () {
      let length = 5;
      let hash;

      yield request
        .get('/api/util/encrypt/' + num + '/' + length)
        .expect(200)
        .expect(function(res) {
          hash = res.text;
        });

      let result = yield Util.encrypt(num, length);
      expect(_.isEqual(result, hash)).to.be.true; // eslint-disable-line no-unused-expressions
      expect(_.isEqual(hash.length, length)).to.be.true; // eslint-disable-line no-unused-expressions
      encryptedTextLength = hash;
    });
  });

  describe('GET /api/util/decrypt/:text', function() {
    it('should fail - passed parameter is not a string', function * () {
      yield request
        .get('/api/util/decrypt/' + num)
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('FORBIDDEN');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Text parameter is not a string.');
        });
    });

    it('should fail - different hashes compared', function * () {
      let hash;

      yield request
        .get('/api/util/decrypt/' + encryptedText)
        .expect(200)
        .expect(function(res) {
          hash = res.text;
        });

      let result = yield Util.decrypt(encryptedText + '`');
      expect(_.isUndefined(result)).to.be.true; // eslint-disable-line no-unused-expressions
      expect(_.isEqual(result, hash)).to.be.false; // eslint-disable-line no-unused-expressions
    });

    it('should succeed', function * () {
      let textResult;

      yield request
        .get('/api/util/decrypt/' + encryptedText)
        .expect(200)
        .expect(function(res) {
          textResult = res.text;
        });

      let result = yield Util.decrypt(encryptedText);
      expect(_.isUndefined(result)).to.be.false; // eslint-disable-line no-unused-expressions
      expect(_.isEqual(result.toString(), textResult)).to.be.true; // eslint-disable-line no-unused-expressions
    });
  });

  describe('GET /api/util/decrypt/:text/:length', function() {
    it('should fail - passed parameter is not a string', function * () {
      let length = 5;

      yield request
        .get('/api/util/decrypt/' + num + '/' + length)
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('FORBIDDEN');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Text parameter is not a string.');
        });
    });

    it('should fail - different hashes compared', function * () {
      let length = 5;
      let hash;

      yield request
        .get('/api/util/decrypt/' + encryptedTextLength + '/' + length)
        .expect(200)
        .expect(function(res) {
          hash = res.text;
        });

      let result = yield Util.decrypt(encryptedTextLength + '`');
      expect(_.isUndefined(result)).to.be.true; // eslint-disable-line no-unused-expressions
      expect(_.isEqual(result, hash)).to.be.false; // eslint-disable-line no-unused-expressions
    });

    it('should fail - length parameter is not a number', function * () {
      let length = 'asdf';

      yield request
        .get('/api/util/decrypt/' + encryptedTextLength + '/' + length)
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('FORBIDDEN');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Length parameter is not a number.');
        });
    });

    it('should fail - length parameter less than 3', function * () {
      let length = 2;

      yield request
        .get('/api/util/decrypt/' + encryptedTextLength + '/' + length)
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('FORBIDDEN');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Length parameter is less than 3.');
        });
    });

    it('should succeed', function * () {
      let length = 5;
      let textResult;

      yield request
        .get('/api/util/decrypt/' + encryptedTextLength + '/' + length)
        .expect(200)
        .expect(function(res) {
          textResult = res.text;
        });

      let result = yield Util.decrypt(encryptedTextLength, length);
      expect(_.isUndefined(result)).to.be.false; // eslint-disable-line no-unused-expressions
      expect(_.isEqual(result.toString(), textResult)).to.be.true; // eslint-disable-line no-unused-expressions
    });
  });

  describe('findKey()', function() {
    it('should get Role keys', function * () {
      yield _.map(Role, function(value, key) {
        return function * () {
          expect(key).to.equal(yield Util.findKey(value, Role));
          return value;
        };
      });
    });

    it('should get AccountType keys', function * () {
      yield _.map(AccountType, function(value, key) {
        return function * () {
          expect(key).to.equal(yield Util.findKey(value, AccountType));
          return value;
        };
      });
    });

    it('should get QuestionType keys', function * () {
      yield _.map(QuestionType, function(value, key) {
        return function * () {
          expect(key).to.equal(yield Util.findKey(value, QuestionType));
          return value;
        };
      });
    });
  });
});
