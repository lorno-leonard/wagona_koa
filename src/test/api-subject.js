/* globals app, Util */
/* eslint max-nested-callbacks: ["error", 6]*/
import {expect} from 'chai';
import _ from 'lodash';

let request;
let token;
let tokenNotAdmin;
let subject;
let insertId;
let subjectData = {
  name: 'Subject II'
};

describe('Subject API', function() {
  this.timeout(20000);

  let admin = {
    username: process.env.ADMIN_USERNAME,
    password: process.env.ADMIN_PASSWORD
  };

  let student = {
    username: process.env.STUDENT_USERNAME,
    password: process.env.STUDENT_PASSWORD
  };

  before(function * () {
    yield app.started;
    request = require('supertest')(app.server);

    yield request
      .post('/api/login')
      .send(admin)
      .expect(200)
      .expect(function(res) {
        expect(res.body).to.have.property('token');
        token = res.body.token;
      });

    yield request
      .post('/api/login')
      .set({
        referer: 'http://localhost/'
      })
      .send(student)
      .expect(200)
      .expect(function(res) {
        expect(res.body).to.have.property('token');
        tokenNotAdmin = res.body.token;
      });
  });

  after(function * () {
    app.server.close();
  });

  describe('GET /api/subject', function() {
    it('should fail - invalid user role', function * () {
      yield request
        .get('/api/subject')
        .query({token: tokenNotAdmin})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('UNAUTHORIZED');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User role is invalid.');
        });
    });

    it('should get subjects', function * () {
      yield request
        .get('/api/subject')
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          expect(res.body.data).to.be.not.empty; // eslint-disable-line no-unused-expressions
          subject = _.first(res.body.data);
        });
    });

    it('should get subjects - with specific fields', function * () {
      let fields = [
        'name'
      ];
      yield request
        .get('/api/subject?fields=' + fields.join(','))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          expect(res.body.data).to.be.not.empty; // eslint-disable-line no-unused-expressions

          _.each(res.body.data, function(_user) {
            expect(_user).to.have.all.keys(fields);
          });
        });
    });

    it('should get subjects - using the search filter', function * () {
      yield request
        .get('/api/subject?search=' + subject.name)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          expect(res.body.data).to.be.not.empty; // eslint-disable-line no-unused-expressions
        });
    });

    it('should not get any subjects - with filters non-existent to existing subjects', function * () { // eslint-disable-line max-len
      let filters = [
        'name=Test Subject'
      ];
      yield request
        .get('/api/subject?' + filters.join('&'))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          expect(res.body.data).to.be.empty; // eslint-disable-line no-unused-expressions
        });
    });
  });

  describe('GET /api/subject/:id', function() {
    it('should fail - invalid user role', function * () {
      yield request
        .get('/api/subject/' + subject.id)
        .query({token: tokenNotAdmin})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('UNAUTHORIZED');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User role is invalid.');
        });
    });

    it('should fail - with an invalid subject id', function * () {
      let fakeId = 1;
      yield request
        .get('/api/subject/' + fakeId)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('Subject Id is invalid.');
        });
    });

    it('should get a subject', function * () {
      yield request
        .get('/api/subject/' + subject.id)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');
        });
    });

    it('should get a subject - with specific fields', function * () {
      let fields = [
        'name'
      ];
      yield request
        .get('/api/subject/' + subject.id + '?fields=' + fields.join(','))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');
          expect(res.body.data).to.have.all.keys(fields);
        });
    });

    it('should not get a subject - with filters non-existent to the subject', function * () { // eslint-disable-line max-len
      let filters = [
        'name=Test Subject'
      ];
      yield request
        .get('/api/subject/' + subject.id + '?' + filters.join('&'))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.false; // eslint-disable-line no-unused-expressions
        });
    });
  });

  describe('GET /api/subject/:id/topic', function() {
    it('should fail - invalid user role', function * () {
      yield request
        .get('/api/subject/' + subject.id + '/topic')
        .query({token: tokenNotAdmin})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('UNAUTHORIZED');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User role is invalid.');
        });
    });

    it('should fail - with an invalid subject id', function * () {
      let fakeId = 1;
      yield request
        .get('/api/subject/' + fakeId + '/topic')
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('Subject Id is invalid.');
        });
    });

    it('should get assigned topics of subject', function * () {
      yield request
        .get('/api/subject/' + subject.id + '/topic')
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
        });
    });

    it('should get assigned topics of subject - with specific fields', function * () { // eslint-disable-line max-len
      let fields = [
        'name',
        'is_paid'
      ];
      yield request
        .get('/api/subject/' + subject.id + '/topic?fields=' + fields.join(','))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');

          _.each(res.body.data, function(_subject) {
            expect(_subject).to.have.all.keys(fields);
          });
        });
    });

    it('should not get assigned topics of subject - with filters non-existent to the topic', function * () { // eslint-disable-line max-len
      let filters = [
        'name=Test Topic'
      ];
      yield request
        .get('/api/subject/' + subject.id + '/topic?' + filters.join('&'))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          expect(res.body.data).to.be.empty; // eslint-disable-line no-unused-expressions
        });
    });
  });

  describe('POST /api/subject', function() {
    it('should fail - invalid user role', function * () {
      yield request
        .post('/api/subject')
        .send(subjectData)
        .query({token: tokenNotAdmin})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('UNAUTHORIZED');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User role is invalid.');
        });
    });

    it('should fail - with duplicate name', function * () {
      let clonedSubjectData = _.clone(subjectData);
      clonedSubjectData.name = subject.name;
      yield request
        .post('/api/subject')
        .send(clonedSubjectData)
        .query({token: token})
        .expect(409)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('CONFLICT');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Subject Name is already existing.');
        });
    });

    it('should create a new subject', function * () {
      yield request
        .post('/api/subject')
        .send(subjectData)
        .query({token: token})
        .expect(201)
        .expect(function(res) {
          insertId = res.body.insertId;
        });
    });

    it('should be equal - saved data and passed data', function * () {
      yield request
        .get('/api/subject/' + insertId)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');

          let data = res.body.data;
          expect(data.name).to.equal(subjectData.name);
        });
    });
  });

  describe('POST /api/subject/:id/topic', function() {
    it('should fail - invalid user role', function * () {
      yield request
        .post('/api/subject/' + subject.id + '/topic')
        .query({token: tokenNotAdmin})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('UNAUTHORIZED');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User role is invalid.');
        });
    });

    it('should fail - with no `topics` parameter passed', function * () {
      yield request
        .post('/api/subject/' + subject.id + '/topic')
        .send({})
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('FORBIDDEN');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Parameter `topics` is required.');
        });
    });

    it('should fail - when `topics` parameter is not an array', function * () { // eslint-disable-line max-len
      let subjectTopicData = {
        topics: 'Lorem ipsum dolor.'
      };
      yield request
        .post('/api/subject/' + subject.id + '/topic')
        .send(subjectTopicData)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('FORBIDDEN');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Parameter `topics` should be an array.');
        });
    });

    it('should fail - when some topic ids are invalid', function * () {
      let subjectTopicData = {
        topics: [yield Util.encrypt(1)]
      };
      yield request
        .post('/api/subject/' + subject.id + '/topic')
        .send(subjectTopicData)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Some Topic Ids are not found.');
        });
    });

    it('should assign topics to subject', function * () {
      // Create a new topic
      let topicData = {
        name: 'Topic V',
        is_paid: 1 // eslint-disable-line camelcase
      };
      let topicId;
      yield request
        .post('/api/topic')
        .send(topicData)
        .query({token: token})
        .expect(201)
        .expect(function(res) {
          topicId = res.body.insertId;
        });

      let subjectTopicData = {
        topics: [topicId]
      };
      yield request
        .post('/api/subject/' + subject.id + '/topic')
        .send(subjectTopicData)
        .query({token: token})
        .expect(201);
    });
  });

  describe('PATCH /api/subject/:id', function() {
    let updateData = {
      name: 'Subject III',
      status: 0
    };

    it('should fail - invalid user role', function * () {
      yield request
        .patch('/api/subject/' + insertId)
        .send(updateData)
        .query({token: tokenNotAdmin})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('UNAUTHORIZED');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User role is invalid.');
        });
    });

    it('should fail - with an invalid subject id', function * () {
      let fakeId = 1;
      yield request
        .patch('/api/subject/' + fakeId)
        .send(updateData)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('Subject Id is invalid.');
        });
    });

    it('should fail - with duplicate name', function * () {
      let clonedUpdateData = _.clone(updateData);
      clonedUpdateData.name = subject.name;
      yield request
        .patch('/api/subject/' + insertId)
        .send(clonedUpdateData)
        .query({token: token})
        .expect(409)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('CONFLICT');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Subject Name is already existing.');
        });
    });

    it('should update subject', function * () {
      yield request
        .patch('/api/subject/' + insertId)
        .send(updateData)
        .query({token: token})
        .expect(201);
    });

    it('should be equal - saved data and passed data', function * () {
      let data;
      yield request
        .get('/api/subject/' + insertId)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');

          data = res.body.data;
          expect(data.name).to.equal(updateData.name);
          expect(data.status).to.equal(updateData.status);
        });
    });
  });
});
