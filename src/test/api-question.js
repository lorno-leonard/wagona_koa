/* globals app */
/* eslint max-nested-callbacks: ["error", 6]*/
import {expect} from 'chai';
import _ from 'lodash';

let request;
let token;
let tokenNotAdmin;
let question;
let choice;
let insertId;
let insertChoiceId;
let questionData = {
  description: '6 + 6 = ?',
  explanation: '6 + 6 = 12',
  type: 'CHOICE',
  is_paid: 0, // eslint-disable-line camelcase
  tags: ['math', 'easy'],
  correct: 2,
  choices: [
    '11',
    '9',
    '12',
    '10'
  ]
};

describe('Question API', function() {
  this.timeout(20000);

  let admin = {
    username: process.env.ADMIN_USERNAME,
    password: process.env.ADMIN_PASSWORD
  };

  let student = {
    username: process.env.STUDENT_USERNAME,
    password: process.env.STUDENT_PASSWORD
  };

  before(function * () {
    yield app.started;
    request = require('supertest')(app.server);

    yield request
      .post('/api/login')
      .send(admin)
      .expect(200)
      .expect(function(res) {
        expect(res.body).to.have.property('token');
        token = res.body.token;
      });

    yield request
      .post('/api/login')
      .set({
        referer: 'http://localhost/'
      })
      .send(student)
      .expect(200)
      .expect(function(res) {
        expect(res.body).to.have.property('token');
        tokenNotAdmin = res.body.token;
      });
  });

  after(function * () {
    app.server.close();
  });

  describe('GET /api/question', function() {
    it('should fail - invalid user role', function * () {
      yield request
        .get('/api/question')
        .query({token: tokenNotAdmin})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('UNAUTHORIZED');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User role is invalid.');
        });
    });

    it('should get questions', function * () {
      yield request
        .get('/api/question')
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          expect(res.body.data).to.be.not.empty; // eslint-disable-line no-unused-expressions
          question = _.first(res.body.data);
        });
    });

    it('should get questions - with specific fields', function * () {
      let fields = [
        'description',
        'type',
        'correct',
        'is_paid',
        'tags',
        'status'
      ];
      yield request
        .get('/api/question?fields=' + fields.join(','))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          expect(res.body.data).to.be.not.empty; // eslint-disable-line no-unused-expressions

          _.each(res.body.data, function(obj) {
            expect(obj).to.have.all.keys(fields);
          });
        });
    });

    it('should get questions - using the search filter', function * () {
      yield request
        .get('/api/question?search=' + encodeURIComponent(question.description))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          expect(res.body.data).to.be.not.empty; // eslint-disable-line no-unused-expressions
        });
    });

    it('should not get any questions - with filters non-existent to existing questions', function * () { // eslint-disable-line max-len
      let filters = [
        'type=NOT_QUESTION_TYPE'
      ];
      yield request
        .get('/api/question?' + filters.join('&'))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          expect(res.body.data).to.be.empty; // eslint-disable-line no-unused-expressions
        });
    });
  });

  describe('GET /api/question/:id', function() {
    it('should fail - invalid user role', function * () {
      yield request
        .get('/api/question/' + question.id)
        .query({token: tokenNotAdmin})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('UNAUTHORIZED');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User role is invalid.');
        });
    });

    it('should fail - with an invalid question id', function * () {
      let fakeId = 1;
      yield request
        .get('/api/question/' + fakeId)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('Question Id is invalid.');
        });
    });

    it('should get a question', function * () {
      yield request
        .get('/api/question/' + question.id)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');
        });
    });

    it('should get a question - with specific fields', function * () {
      let fields = [
        'description',
        'type',
        'correct',
        'is_paid',
        'tags',
        'status'
      ];
      yield request
        .get('/api/question/' + question.id + '?fields=' + fields.join(','))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');
          expect(res.body.data).to.have.all.keys(fields);
        });
    });

    it('should not get a question - with filters non-existent to the question', function * () { // eslint-disable-line max-len
      let filters = [
        'type=NOT_QUESTION_TYPE'
      ];
      yield request
        .get('/api/question/' + question.id + '?' + filters.join('&'))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.false; // eslint-disable-line no-unused-expressions
        });
    });
  });

  describe('GET /api/question/:id/choice', function() {
    it('should fail - invalid user role', function * () {
      yield request
        .get('/api/question/' + question.id + '/choice')
        .query({token: tokenNotAdmin})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('UNAUTHORIZED');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User role is invalid.');
        });
    });

    it('should get choices', function * () {
      yield request
        .get('/api/question/' + question.id + '/choice')
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          choice = _.first(res.body.data);
        });
    });

    it('should get choices - with specific fields', function * () {
      let fields = [
        'question_id',
        'description',
        '`index`',
        'status'
      ];
      yield request
        .get('/api/question/' + question.id + '/choice?fields=' + fields.join(',')) // eslint-disable-line max-len
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');

          let clonedFields = _.map(fields, function(field) {
            return _.isEqual(field, '`index`') ? 'index' : field;
          });
          _.each(res.body.data, function(obj) {
            expect(obj).to.have.all.keys(clonedFields);
          });
        });
    });

    it('should not get any questions - with filters non-existent to existing questions', function * () { // eslint-disable-line max-len
      let filters = [
        '`index`=999999'
      ];
      yield request
        .get('/api/question/' + question.id + '/choice?' + filters.join('&'))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          expect(res.body.data).to.be.empty; // eslint-disable-line no-unused-expressions
        });
    });
  });

  describe('GET /api/question/choice/:id', function() {
    it('should fail - invalid user role', function * () {
      yield request
        .get('/api/question/choice/' + choice.id)
        .query({token: tokenNotAdmin})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('UNAUTHORIZED');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User role is invalid.');
        });
    });

    it('should fail - with an invalid choice id', function * () {
      let fakeId = 1;
      yield request
        .get('/api/question/choice/' + fakeId)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('Choice Id is invalid.');
        });
    });

    it('should get a choice', function * () {
      yield request
        .get('/api/question/choice/' + choice.id)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');
        });
    });

    it('should get a choice - with specific fields', function * () {
      let fields = [
        'question_id',
        'description',
        '`index`',
        'status'
      ];
      yield request
        .get('/api/question/choice/' + choice.id + '?fields=' + fields.join(',')) // eslint-disable-line max-len
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');

          let clonedFields = _.map(fields, function(field) {
            return _.isEqual(field, '`index`') ? 'index' : field;
          });
          expect(res.body.data).to.have.all.keys(clonedFields);
        });
    });

    it('should not get a choice - with filters non-existent to the choice', function * () { // eslint-disable-line max-len
      let filters = [
        '`index`=999999'
      ];
      yield request
        .get('/api/question/choice/' + choice.id + '?' + filters.join('&'))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.false; // eslint-disable-line no-unused-expressions
        });
    });
  });

  describe('GET /api/choice/:id', function() {
    it('should fail - invalid user role', function * () {
      yield request
        .get('/api/choice/' + choice.id)
        .query({token: tokenNotAdmin})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('UNAUTHORIZED');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User role is invalid.');
        });
    });

    it('should fail - with an invalid choice id', function * () {
      let fakeId = 1;
      yield request
        .get('/api/choice/' + fakeId)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('Choice Id is invalid.');
        });
    });

    it('should get a choice', function * () {
      yield request
        .get('/api/choice/' + choice.id)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');
        });
    });

    it('should get a choice - with specific fields', function * () {
      let fields = [
        'question_id',
        'description',
        '`index`',
        'status'
      ];
      yield request
        .get('/api/choice/' + choice.id + '?fields=' + fields.join(','))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');

          let clonedFields = _.map(fields, function(field) {
            return _.isEqual(field, '`index`') ? 'index' : field;
          });
          expect(res.body.data).to.have.all.keys(clonedFields);
        });
    });

    it('should not get a choice - with filters non-existent to the choice', function * () { // eslint-disable-line max-len
      let filters = [
        '`index`=999999'
      ];
      yield request
        .get('/api/choice/' + choice.id + '?' + filters.join('&'))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.false; // eslint-disable-line no-unused-expressions
        });
    });
  });

  describe('POST /api/question', function() {
    it('should fail - invalid user role', function * () {
      yield request
        .post('/api/question')
        .send(questionData)
        .query({token: tokenNotAdmin})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('UNAUTHORIZED');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User role is invalid.');
        });
    });

    it('should fail - with invalid question type', function * () {
      let clonedQuestionData = _.clone(questionData);
      clonedQuestionData.type = 'NOT_QUESTION_TYPE';
      yield request
        .post('/api/question')
        .send(clonedQuestionData)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('FORBIDDEN');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Question Type is invalid.');
        });
    });

    it('should fail - with no `choices` parameter passed', function * () {
      let clonedQuestionData = _.clone(questionData);
      delete clonedQuestionData.choices;
      yield request
        .post('/api/question')
        .send(clonedQuestionData)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('FORBIDDEN');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Parameter `choices` is required.');
        });
    });

    it('should fail - when `choices` parameter is not an array', function * () { // eslint-disable-line max-len
      let clonedQuestionData = _.clone(questionData);
      clonedQuestionData.choices = _.toPlainObject(clonedQuestionData.choices);
      yield request
        .post('/api/question')
        .send(clonedQuestionData)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('FORBIDDEN');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Parameter `choices` should be an array.');
        });
    });

    it('should fail - when `choices` parameter is empty', function * () {
      let clonedQuestionData = _.clone(questionData);
      clonedQuestionData.choices = [];
      yield request
        .post('/api/question')
        .send(clonedQuestionData)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('FORBIDDEN');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Parameter `choices` is empty.');
        });
    });

    it('should create a new question', function * () {
      yield request
        .post('/api/question')
        .send(questionData)
        .query({token: token})
        .expect(201)
        .expect(function(res) {
          insertId = res.body.insertId;
        });
    });

    it('should be equal - saved data and passed data', function * () {
      yield request
        .get('/api/question/' + insertId)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');

          let data = res.body.data;
          expect(data.description).to.equal(questionData.description);
          expect(data.type).to.equal(questionData.type);
          expect(data.is_paid).to.equal(questionData.is_paid);
          expect(_.isEqual(data.tags, questionData.tags)).to.be.true; // eslint-disable-line no-unused-expressions
        });
    });
  });

  describe('POST /api/question/:id/choice', function() {
    let choiceData;

    it('should fail - invalid user role', function * () {
      yield request
        .post('/api/question/' + insertId + '/choice')
        .send(choiceData)
        .query({token: tokenNotAdmin})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('UNAUTHORIZED');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User role is invalid.');
        });
    });

    it('should fail - with an invalid question id', function * () {
      let fakeId = 1;
      yield request
        .post('/api/question/' + fakeId + '/choice')
        .send(choiceData)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('Question Id is invalid.');
        });
    });

    it('should create a new choice', function * () {
      choiceData = {
        description: '13'
      };
      yield request
        .post('/api/question/' + insertId + '/choice')
        .send(choiceData)
        .query({token: token})
        .expect(201)
        .expect(function(res) {
          insertChoiceId = res.body.insertId;
        });
    });

    it('should be equal - saved data and passed data', function * () {
      yield request
        .get('/api/question/choice/' + insertChoiceId)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');

          let data = res.body.data;
          expect(data.question_id).to.equal(insertId);
          expect(data.description).to.equal(choiceData.description);
        });
    });
  });

  describe('PATCH /api/question/:id', function() {
    let updateData = {
      description: '6 + 6 = ?',
      explanation: '6 + 6 = 12',
      is_paid: 1, // eslint-disable-line camelcase
      tags: ['math', 'easy', 'what', 'test'],
      status: 0,
      choices: [
        '11',
        '9',
        '12',
        '10'
      ]
    };

    it('should fail - invalid user role', function * () {
      yield request
        .patch('/api/question/' + insertId)
        .send(updateData)
        .query({token: tokenNotAdmin})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('UNAUTHORIZED');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User role is invalid.');
        });
    });

    it('should fail - with an invalid question id', function * () {
      let fakeId = 1;
      yield request
        .patch('/api/question/' + fakeId)
        .send(updateData)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('Question Id is invalid.');
        });
    });

    it('should update question', function * () {
      yield request
        .patch('/api/question/' + insertId)
        .send(updateData)
        .query({token: token})
        .expect(201);

      let choiceData = {
        description: '13'
      };
      yield request
        .post('/api/question/' + insertId + '/choice')
        .send(choiceData)
        .query({token: token})
        .expect(201)
        .expect(function(res) {
          insertChoiceId = res.body.insertId;
        });
    });

    it('should be equal - saved data and passed data', function * () {
      let data;
      yield request
        .get('/api/question/' + insertId)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');

          data = res.body.data;
          expect(data.description).to.equal(updateData.description);
          expect(data.explanation).to.equal(updateData.explanation);
          expect(data.is_paid).to.equal(updateData.is_paid);
          expect(_.isEqual(data.tags, updateData.tags)).to.be.true; // eslint-disable-line no-unused-expressions
          expect(data.status).to.equal(updateData.status);
        });
    });
  });

  describe('PATCH /api/question/choice/:id', function() {
    let updateData = {
      description: '14',
      status: 0
    };

    it('should fail - invalid user role', function * () {
      yield request
        .patch('/api/question/choice/' + insertChoiceId)
        .send(updateData)
        .query({token: tokenNotAdmin})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('UNAUTHORIZED');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User role is invalid.');
        });
    });

    it('should fail - with an invalid choice id', function * () {
      let fakeId = 1;
      yield request
        .patch('/api/question/choice/' + fakeId)
        .send(updateData)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('Choice Id is invalid.');
        });
    });

    it('should update choice', function * () {
      yield request
        .patch('/api/question/choice/' + insertChoiceId)
        .send(updateData)
        .query({token: token})
        .expect(201);
    });

    it('should be equal - saved data and passed data', function * () {
      let data;
      yield request
        .get('/api/question/choice/' + insertChoiceId)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');

          data = res.body.data;
          expect(data.description).to.equal(updateData.description);
          expect(data.status).to.equal(updateData.status);
        });
    });
  });

  describe('PATCH /api/choice/:id', function() {
    let updateData = {
      description: '15',
      status: 1
    };

    it('should fail - invalid user role', function * () {
      yield request
        .patch('/api/choice/' + insertChoiceId)
        .send(updateData)
        .query({token: tokenNotAdmin})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('UNAUTHORIZED');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User role is invalid.');
        });
    });

    it('should fail - with an invalid choice id', function * () {
      let fakeId = 1;
      yield request
        .patch('/api/choice/' + fakeId)
        .send(updateData)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('Choice Id is invalid.');
        });
    });

    it('should update choice', function * () {
      yield request
        .patch('/api/choice/' + insertChoiceId)
        .send(updateData)
        .query({token: token})
        .expect(201);
    });

    it('should be equal - saved data and passed data', function * () {
      let data;
      yield request
        .get('/api/choice/' + insertChoiceId)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');

          data = res.body.data;
          expect(data.description).to.equal(updateData.description);
          expect(data.status).to.equal(updateData.status);
        });
    });
  });
});
