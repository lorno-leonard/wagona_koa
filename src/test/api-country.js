/* globals app */
/* eslint max-nested-callbacks: ["error", 6]*/
import {expect} from 'chai';
import _ from 'lodash';

let request;
let token;
let tokenNotAdmin;
let country;
let countryData = {
  id: 'ZZ',
  name: 'Zionist Zone'
};

describe('Country API', function() {
  this.timeout(20000);

  let admin = {
    username: process.env.ADMIN_USERNAME,
    password: process.env.ADMIN_PASSWORD
  };

  let student = {
    username: process.env.STUDENT_USERNAME,
    password: process.env.STUDENT_PASSWORD
  };

  before(function * () {
    yield app.started;
    request = require('supertest')(app.server);

    yield request
      .post('/api/login')
      .send(admin)
      .expect(200)
      .expect(function(res) {
        expect(res.body).to.have.property('token');
        token = res.body.token;
      });

    yield request
      .post('/api/login')
      .set({
        referer: 'http://localhost/'
      })
      .send(student)
      .expect(200)
      .expect(function(res) {
        expect(res.body).to.have.property('token');
        tokenNotAdmin = res.body.token;
      });
  });

  after(function * () {
    app.server.close();
  });

  describe('GET /api/country', function() {
    it('should fail - invalid user role', function * () {
      yield request
        .get('/api/country')
        .query({token: tokenNotAdmin})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('UNAUTHORIZED');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User role is invalid.');
        });
    });

    it('should get countries', function * () {
      yield request
        .get('/api/country')
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          expect(res.body.data).to.be.not.empty; // eslint-disable-line no-unused-expressions
          country = _.first(res.body.data);
        });
    });

    it('should get countries - with specific fields', function * () {
      let fields = [
        'name'
      ];
      yield request
        .get('/api/country?fields=' + fields.join(','))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          expect(res.body.data).to.be.not.empty; // eslint-disable-line no-unused-expressions

          _.each(res.body.data, function(_user) {
            expect(_user).to.have.all.keys(fields);
          });
        });
    });

    it('should get countries - using the search filter', function * () {
      yield request
        .get('/api/country?search=' + country.name)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          expect(res.body.data).to.be.not.empty; // eslint-disable-line no-unused-expressions
        });
    });

    it('should not get any countries - with filters non-existent to existing countries', function * () { // eslint-disable-line max-len
      let filters = [
        'name=Test Country'
      ];
      yield request
        .get('/api/country?' + filters.join('&'))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          expect(res.body.data).to.be.empty; // eslint-disable-line no-unused-expressions
        });
    });
  });

  describe('GET /api/country/:id', function() {
    it('should fail - invalid user role', function * () {
      yield request
        .get('/api/country/' + country.id)
        .query({token: tokenNotAdmin})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('UNAUTHORIZED');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User role is invalid.');
        });
    });

    it('should get a country', function * () {
      yield request
        .get('/api/country/' + country.id)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');
        });
    });

    it('should get a country - with specific fields', function * () {
      let fields = [
        'name'
      ];
      yield request
        .get('/api/country/' + country.id + '?fields=' + fields.join(','))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');
          expect(res.body.data).to.have.all.keys(fields);
        });
    });

    it('should not get a country - with filters non-existent to the country', function * () { // eslint-disable-line max-len
      let filters = [
        'name=Test Country'
      ];
      yield request
        .get('/api/country/' + country.id + '?' + filters.join('&'))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.false; // eslint-disable-line no-unused-expressions
        });
    });
  });

  describe('POST /api/country', function() {
    it('should fail - invalid user role', function * () {
      yield request
        .post('/api/country')
        .send(countryData)
        .query({token: tokenNotAdmin})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('UNAUTHORIZED');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User role is invalid.');
        });
    });

    it('should fail - with duplicate id', function * () {
      let clonedCountryData = _.clone(countryData);
      clonedCountryData.id = country.id;
      yield request
        .post('/api/country')
        .send(clonedCountryData)
        .query({token: token})
        .expect(409)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('CONFLICT');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('Country Id is already existing.');
        });
    });

    it('should fail - with duplicate name', function * () {
      let clonedCountryData = _.clone(countryData);
      clonedCountryData.name = country.name;
      yield request
        .post('/api/country')
        .send(clonedCountryData)
        .query({token: token})
        .expect(409)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('CONFLICT');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Country Name is already existing.');
        });
    });

    it('should create a new country', function * () {
      yield request
        .post('/api/country')
        .send(countryData)
        .query({token: token})
        .expect(201);
    });

    it('should be equal - saved data and passed data', function * () {
      let id = countryData.id;
      yield request
        .get('/api/country/' + id)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');

          let data = res.body.data;
          expect(data.name).to.equal(countryData.name);
        });
    });
  });

  describe('PATCH /api/country/:id', function() {
    let id = countryData.id;
    let updateData = {
      name: 'Z Nation',
      status: 0
    };

    it('should fail - invalid user role', function * () {
      yield request
        .patch('/api/country/' + id)
        .send(updateData)
        .query({token: tokenNotAdmin})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('UNAUTHORIZED');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User role is invalid.');
        });
    });

    it('should fail - with duplicate id', function * () {
      let clonedUpdateData = _.clone(updateData);
      clonedUpdateData.id = country.id;
      yield request
        .patch('/api/country/' + id)
        .send(clonedUpdateData)
        .query({token: token})
        .expect(409)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('CONFLICT');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('Country Id is already existing.');
        });
    });

    it('should fail - with duplicate name', function * () {
      let clonedUpdateData = _.clone(updateData);
      clonedUpdateData.name = country.name;
      yield request
        .patch('/api/country/' + id)
        .send(clonedUpdateData)
        .query({token: token})
        .expect(409)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('CONFLICT');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Country Name is already existing.');
        });
    });

    it('should update country', function * () {
      yield request
        .patch('/api/country/' + id)
        .send(updateData)
        .query({token: token})
        .expect(201);
    });

    it('should be equal - saved data and passed data', function * () {
      let data;
      yield request
        .get('/api/country/' + id)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');

          data = res.body.data;
          expect(data.name).to.equal(updateData.name);
          expect(data.status).to.equal(updateData.status);
        });
    });
  });
});
