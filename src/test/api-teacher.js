/* globals app, Util */
/* eslint max-nested-callbacks: ["error", 6]*/
import {expect} from 'chai';
import _ from 'lodash';

let request;
let token;
let teacher;
let insertId;
let teacherData = {
  address1: 'Villa Angela Subd., Balulang, Cagayan de Oro City',
  address2: 'Corono Drive, Tibasak, Macasanding, Cagayan de Oro City',
  address3: 'Purok 1, Tawantawan, Initao',
  address4: 'Bliss, San Pedro, Initao',
  phone_number: '+631234567890', // eslint-disable-line camelcase
  telephone_number: '08888752075' // eslint-disable-line camelcase
};

describe('Teacher API', function() {
  this.timeout(20000);

  let admin = {
    username: process.env.ADMIN_USERNAME,
    password: process.env.ADMIN_PASSWORD
  };

  before(function * () {
    yield app.started;
    request = require('supertest')(app.server);

    yield request
      .post('/api/login')
      .send(admin)
      .expect(200)
      .expect(function(res) {
        expect(res.body).to.have.property('token');
        token = res.body.token;
      });
  });

  after(function * () {
    app.server.close();
  });

  describe('GET /api/teacher', function() {
    it('should get teachers', function * () {
      yield request
        .get('/api/teacher')
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          teacher = _.first(res.body.data);
        });
    });

    it('should get teachers - with specific fields', function * () {
      let fields = [
        'status'
      ];
      yield request
        .get('/api/teacher?fields=' + fields.join(','))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');

          _.each(res.body.data, function(_user) {
            expect(_user).to.have.all.keys(fields);
          });
        });
    });

    it('should not get any teachers - with filters non-existent to existing teachers', function * () { // eslint-disable-line max-len
      let filters = [
        'status=2'
      ];
      yield request
        .get('/api/teacher?' + filters.join('&'))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          expect(res.body.data).to.be.empty; // eslint-disable-line no-unused-expressions
        });
    });
  });

  describe('GET /api/teacher/:id', function() {
    it('should fail - with an invalid teacher id', function * () {
      let fakeId = 1;
      yield request
        .get('/api/teacher/' + fakeId)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('Teacher Id is invalid.');
        });
    });

    it('should get a teacher', function * () {
      yield request
        .get('/api/teacher/' + teacher.id)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');
        });
    });

    it('should get a teacher - with specific fields', function * () {
      let fields = [
        'status'
      ];
      yield request
        .get('/api/teacher/' + teacher.id + '?fields=' + fields.join(','))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');
          expect(res.body.data).to.have.all.keys(fields);
        });
    });

    it('should not get a teacher - with filters non-existent to the teacher', function * () { // eslint-disable-line max-len
      let filters = [
        'status=2'
      ];
      yield request
        .get('/api/teacher/' + teacher.id + '?' + filters.join('&'))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.false; // eslint-disable-line no-unused-expressions
        });
    });
  });

  describe('POST /api/teacher', function() {
    it('should fail - with invalid user_id', function * () {
      let clonedTeacherData = _.clone(teacherData);
      clonedTeacherData.user_id = 1; // eslint-disable-line camelcase
      yield request
        .post('/api/teacher')
        .send(clonedTeacherData)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('User Id is invalid.');
        });
    });

    it('should create a new teacher', function * () {
      teacherData.user_id = yield Util.encrypt(1012); // eslint-disable-line camelcase
      yield request
        .post('/api/teacher')
        .send(teacherData)
        .query({token: token})
        .expect(201)
        .expect(function(res) {
          insertId = res.body.insertId;
        });
    });

    it('should be equal - saved data and passed data', function * () {
      yield request
        .get('/api/teacher/' + insertId)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');

          let data = res.body.data;
          let otherDetails = _.omit(teacherData, [
            'user_id'
          ]);
          if (!_.isEmpty(otherDetails)) {
            expect(data.profile).to.deep.equal(otherDetails);
          }
        });
    });
  });

  describe('PATCH /api/teacher/:id', function() {
    let updateData = {
      status: 0,
      address1: 'Villa Angela Subd., Balulang, Cagayan de Oro City',
      address2: 'Corono Drive, Tibasak, Macasanding, Cagayan de Oro City'
    };

    it('should fail - with an invalid teacher id', function * () {
      let fakeId = 1;
      yield request
        .patch('/api/teacher/' + fakeId)
        .send(updateData)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('Teacher Id is invalid.');
        });
    });

    it('should update teacher', function * () {
      yield request
        .patch('/api/teacher/' + insertId)
        .send(updateData)
        .query({token: token})
        .expect(201);
    });

    it('should be equal - saved data and passed data', function * () {
      yield request
        .get('/api/teacher/' + insertId)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');

          let data = res.body.data;
          expect(data.status).to.equal(updateData.status);

          let otherDetails = _.omit(updateData, [
            'status'
          ]);
          if (!_.isEmpty(otherDetails)) {
            expect(data.profile).to.deep.equal(otherDetails);
          }
        });
    });
  });
});
