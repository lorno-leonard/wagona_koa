/* globals app */
/* eslint max-nested-callbacks: ["error", 6]*/
import Token from '../lib/token';
import {expect} from 'chai';
import rand from 'rand-token';
import moment from 'moment';

let request;
let token;

describe('Authentication API', function() {
  this.timeout(20000);

  let admin = {
    username: process.env.ADMIN_USERNAME,
    email: process.env.ADMIN_EMAIL,
    password: process.env.ADMIN_PASSWORD
  };

  let student = {
    username: process.env.STUDENT_USERNAME,
    email: process.env.STUDENT_EMAIL,
    password: process.env.STUDENT_PASSWORD
  };

  before(function * () {
    yield app.started;
    request = require('supertest')(app.server);
  });

  after(function * () {
    app.server.close();
  });

  describe('POST /api/login', function() {
    it('should fail - using GET method', function * () {
      yield request
        .get('/api/login')
        .expect(405);
    });

    it('should fail - with no username or password', function * () {
      yield request
        .post('/api/login')
        .send({})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('FORBIDDEN');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('Username and Password required.');
        });
    });

    it('should fail - to non existent username', function * () {
      yield request
        .post('/api/login')
        .send({
          username: 'user_' + rand.generate(6),
          password: admin.password
        })
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_USERNAME');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('Username/Email does not exist.');
        });
    });

    it('should fail - to non existent email', function * () {
      yield request
        .post('/api/login')
        .send({
          username: 'user_' + rand.generate(6) + '@wagona.com',
          password: admin.password
        })
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_USERNAME');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('Username/Email does not exist.');
        });
    });

    it('should fail - to invalid password', function * () {
      yield request
        .post('/api/login')
        .send({
          username: admin.username,
          password: admin.password + rand.generate(6)
        })
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_PASSWORD');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('Invalid password.');
        });
    });

    it('should be able to login - with username used', function * () {
      yield request
        .post('/api/login')
        .send(admin)
        .expect(200)
        .expect(function(res) {
          expect(res.body).to.be.an('object');
          expect(res.body).to.have.property('token');
          token = res.body.token;
        });
    });

    it('should be able to login - with email used', function * () {
      yield request
        .post('/api/login')
        .send({
          username: admin.email,
          password: admin.password
        })
        .expect(200)
        .expect(function(res) {
          expect(res.body).to.be.an('object');
          expect(res.body).to.have.property('token');
        });
    });

    it('token should expire within a year - with remember_me', function * () {
      let tokenRememberMe = null;
      yield request
        .post('/api/login')
        .send({
          username: admin.email,
          password: admin.password,
          remember_me: true // eslint-disable-line camelcase
        })
        .expect(200)
        .expect(function(res) {
          expect(res.body).to.be.an('object');
          expect(res.body).to.have.property('token');
          tokenRememberMe = res.body.token;
        });

      // Validate token to expire within a year
      let payload = yield Token.decode(tokenRememberMe);
      let now = moment().unix();
      let yearFromNow = moment().add({years: 1}).unix();
      expect(payload.expire).to.not.equal(now);
      expect(payload.expire).to.be.above(now);
      expect(now).to.be.below(payload.expire);
      expect(payload.expire).to.not.equal(yearFromNow);
      expect(payload.expire).to.be.above(yearFromNow);
      expect(yearFromNow).to.be.below(payload.expire);
    });

    it('token should expire within a day - with no remember_me', function * () {
      let tokenRememberMe = null;
      yield request
        .post('/api/login')
        .send({
          username: admin.email,
          password: admin.password
        })
        .expect(200)
        .expect(function(res) {
          expect(res.body).to.be.an('object');
          expect(res.body).to.have.property('token');
          tokenRememberMe = res.body.token;
        });

      // Validate token to expire within a day
      let payload = yield Token.decode(tokenRememberMe);
      let now = moment().unix();
      let dayFromNow = moment().add({days: 1}).unix();
      expect(payload.expire).to.not.equal(now);
      expect(payload.expire).to.be.above(now);
      expect(now).to.be.below(payload.expire);
      expect(payload.expire).to.not.equal(dayFromNow);
      expect(payload.expire).to.be.above(dayFromNow);
      expect(dayFromNow).to.be.below(payload.expire);
    });

    it('should fail - accessing main page using admin account', function * () {
      yield request
        .post('/api/login')
        .set({
          referer: 'http://localhost/'
        })
        .send(admin)
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('FORBIDDEN');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('Account forbidden to access this page.');
        });
    });

    it('should fail - accessing admin page using non-admin account', function * () {
      yield request
        .post('/api/login')
        .set({
          referer: 'http://localhost/admin'
        })
        .send(student)
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('FORBIDDEN');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('Account forbidden to access this page.');
        });
    });
  });

  describe('POST /api/logout', function() {
    it('should fail - using GET method', function * () {
      yield request
        .get('/api/logout')
        .expect(405);
    });

    it('should be able to logout', function * () {
      yield request
        .post('/api/logout')
        .query({token: token})
        .expect(200);
    });
  });
});
