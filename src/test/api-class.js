/* globals app, Util */
/* eslint max-nested-callbacks: ["error", 6]*/
import {expect} from 'chai';
import _ from 'lodash';

let request;
let token;
let _class;
let insertId;
let classData = {
  name: 'Class II'
};

describe('Class API', function() {
  this.timeout(20000);

  let admin = {
    username: process.env.ADMIN_USERNAME,
    password: process.env.ADMIN_PASSWORD
  };

  before(function * () {
    yield app.started;
    request = require('supertest')(app.server);

    yield request
      .post('/api/login')
      .send(admin)
      .expect(200)
      .expect(function(res) {
        expect(res.body).to.have.property('token');
        token = res.body.token;
      });
  });

  after(function * () {
    app.server.close();
  });

  describe('GET /api/class', function() {
    it('should get classes', function * () {
      yield request
        .get('/api/class')
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          _class = _.first(res.body.data);
        });
    });

    it('should get classes - with specific fields', function * () {
      let fields = [
        'name',
        'status'
      ];
      yield request
        .get('/api/class?fields=' + fields.join(','))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');

          _.each(res.body.data, function(obj) {
            expect(obj).to.have.all.keys(fields);
          });
        });
    });

    it('should not get any classes - with filters non-existent to existing classes', function * () { // eslint-disable-line max-len
      let filters = [
        'name=Test Class'
      ];
      yield request
        .get('/api/class?' + filters.join('&'))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          expect(res.body.data).to.be.empty; // eslint-disable-line no-unused-expressions
        });
    });
  });

  describe('GET /api/class/:id', function() {
    it('should fail - with an invalid class id', function * () {
      let fakeId = 1;
      yield request
        .get('/api/class/' + fakeId)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('Class Id is invalid.');
        });
    });

    it('should get a class', function * () {
      yield request
        .get('/api/class/' + _class.id)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');
        });
    });

    it('should get a class - with specific fields', function * () {
      let fields = [
        'name',
        'status'
      ];
      yield request
        .get('/api/class/' + _class.id + '?fields=' + fields.join(','))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');
          expect(res.body.data).to.have.all.keys(fields);
        });
    });

    it('should not get a class - with filters non-existent to the class', function * () { // eslint-disable-line max-len
      let filters = [
        'name=Test Class'
      ];
      yield request
        .get('/api/class/' + _class.id + '?' + filters.join('&'))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.false; // eslint-disable-line no-unused-expressions
        });
    });
  });

  describe('POST /api/class', function() {
    it('should fail - with invalid school_id', function * () {
      classData.school_id = yield Util.encrypt(1000); // eslint-disable-line camelcase
      classData.syllabi_id = yield Util.encrypt(1000); // eslint-disable-line camelcase
      classData.subject_id = yield Util.encrypt(1000); // eslint-disable-line camelcase
      let clonedClassData = _.clone(classData);
      clonedClassData.school_id = 1; // eslint-disable-line camelcase
      yield request
        .post('/api/class')
        .send(clonedClassData)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('School Id is invalid.');
        });
    });

    it('should fail - with invalid syllabi_id', function * () {
      let clonedClassData = _.clone(classData);
      clonedClassData.syllabi_id = 1; // eslint-disable-line camelcase
      yield request
        .post('/api/class')
        .send(clonedClassData)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Syllabi Id is invalid.');
        });
    });

    it('should fail - with invalid subject_id', function * () {
      let clonedClassData = _.clone(classData);
      clonedClassData.subject_id = 1; // eslint-disable-line camelcase
      yield request
        .post('/api/class')
        .send(clonedClassData)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Subject Id is invalid.');
        });
    });

    it('should create a new class', function * () {
      yield request
        .post('/api/class')
        .send(classData)
        .query({token: token})
        .expect(201)
        .expect(function(res) {
          insertId = res.body.insertId;
        });
    });

    it('should be equal - saved data and passed data', function * () {
      yield request
        .get('/api/class/' + insertId)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');

          let data = res.body.data;
          expect(data.name).to.equal(classData.name);
          expect(data.school_id).to.equal(classData.school_id);
          expect(data.syllabi_id).to.equal(classData.syllabi_id);
          expect(data.subject_id).to.equal(classData.subject_id);
        });
    });
  });

  describe('PATCH /api/class/:id', function() {
    let updateData = {
      name: 'Class III',
      status: 0
    };

    it('should fail - with an invalid class id', function * () {
      let fakeId = 1;
      yield request
        .patch('/api/class/' + fakeId)
        .send(updateData)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('Class Id is invalid.');
        });
    });

    it('should update class', function * () {
      yield request
        .patch('/api/class/' + insertId)
        .send(updateData)
        .query({token: token})
        .expect(201);
    });

    it('should be equal - saved data and passed data', function * () {
      yield request
        .get('/api/class/' + insertId)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');

          let data = res.body.data;
          expect(data.name).to.equal(updateData.name);
          expect(data.status).to.equal(updateData.status);
        });
    });
  });
});
