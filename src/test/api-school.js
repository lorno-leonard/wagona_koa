/* globals app, Util */
/* eslint max-nested-callbacks: ["error", 6]*/
import {expect} from 'chai';
import _ from 'lodash';

let request;
let token;
let school;
let insertId;
let insertProfileId;
let schoolData = {
  name: 'School II',
  num_students: 200, // eslint-disable-line camelcase
  account_type: 'MAIN_SCHOOL', // eslint-disable-line camelcase
  verified: 1
};
let schoolProfileData = {
  name: 'School III',
  num_students: 1000, // eslint-disable-line camelcase
  account_type: 'MAIN_SCHOOL', // eslint-disable-line camelcase
  verified: 1,
  address1: 'Villa Angela Subd., Balulang, Cagayan de Oro City',
  address2: 'Corono Drive, Tibasak, Macasanding, Cagayan de Oro City',
  address3: 'Purok 1, Tawantawan, Initao',
  address4: 'Bliss, San Pedro, Initao',
  cellphone_number: '+631234567890', // eslint-disable-line camelcase
  telephone_number: '08888752075' // eslint-disable-line camelcase
};

describe('School API', function() {
  this.timeout(20000);

  let admin = {
    username: process.env.ADMIN_USERNAME,
    password: process.env.ADMIN_PASSWORD
  };

  before(function * () {
    yield app.started;
    request = require('supertest')(app.server);

    yield request
      .post('/api/login')
      .send(admin)
      .expect(200)
      .expect(function(res) {
        expect(res.body).to.have.property('token');
        token = res.body.token;
      });
  });

  after(function * () {
    app.server.close();
  });

  describe('GET /api/school', function() {
    it('should get schools', function * () {
      yield request
        .get('/api/school')
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          school = _.first(res.body.data);
        });
    });

    it('should get schools - with specific fields', function * () {
      let fields = [
        'name',
        'user_id',
        'num_students',
        'account_type'
      ];
      yield request
        .get('/api/school?fields=' + fields.join(','))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');

          _.each(res.body.data, function(_user) {
            expect(_user).to.have.all.keys(fields);
          });
        });
    });

    it('should not get any schools - with filters non-existent to existing schools', function * () { // eslint-disable-line max-len
      let filters = [
        'name=Test School'
      ];
      yield request
        .get('/api/school?' + filters.join('&'))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          expect(res.body.data).to.be.empty; // eslint-disable-line no-unused-expressions
        });
    });
  });

  describe('GET /api/school/:id', function() {
    it('should fail - with an invalid school id', function * () {
      let fakeId = 1;
      yield request
        .get('/api/school/' + fakeId)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('School Id is invalid.');
        });
    });

    it('should get a school', function * () {
      yield request
        .get('/api/school/' + school.id)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');
        });
    });

    it('should get a school - with specific fields', function * () {
      let fields = [
        'name',
        'user_id',
        'num_students',
        'account_type'
      ];
      yield request
        .get('/api/school/' + school.id + '?fields=' + fields.join(','))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');
          expect(res.body.data).to.have.all.keys(fields);
        });
    });

    it('should not get a school - with filters non-existent to the school', function * () { // eslint-disable-line max-len
      let filters = [
        'name=Test School'
      ];
      yield request
        .get('/api/school/' + school.id + '?' + filters.join('&'))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.false; // eslint-disable-line no-unused-expressions
        });
    });
  });

  describe('GET /api/school/:id/syllabi', function() {
    it('should fail - with an invalid school id', function * () {
      let fakeId = 1;
      yield request
        .get('/api/school/' + fakeId + '/syllabi')
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('School Id is invalid.');
        });
    });

    it('should get assigned syllabus of school', function * () {
      yield request
        .get('/api/school/' + school.id + '/syllabi')
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
        });
    });

    it('should get assigned syllabus of school - with specific fields', function * () { // eslint-disable-line max-len
      let fields = [
        'name',
        'status'
      ];
      yield request
        .get('/api/school/' + school.id + '/syllabi?fields=' + fields.join(','))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');

          _.each(res.body.data, function(_school) {
            expect(_school).to.have.all.keys(fields);
          });
        });
    });

    it('should not get assigned syllabus of school - with filters non-existent to the syllabi', function * () { // eslint-disable-line max-len
      let filters = [
        'name=Test Syllabi'
      ];
      yield request
        .get('/api/school/' + school.id + '/syllabi?' + filters.join('&'))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          expect(res.body.data).to.be.empty; // eslint-disable-line no-unused-expressions
        });
    });
  });

  describe('POST /api/school', function() {
    it('should fail - with invalid user_id', function * () {
      let clonedSchoolData = _.clone(schoolData);
      clonedSchoolData.user_id = 1; // eslint-disable-line camelcase
      yield request
        .post('/api/school')
        .send(clonedSchoolData)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('User Id is invalid.');
        });
    });

    it('should fail - with duplicate name', function * () {
      schoolData.user_id = yield Util.encrypt(1003); // eslint-disable-line camelcase
      schoolProfileData.user_id = yield Util.encrypt(1004); // eslint-disable-line camelcase
      let clonedSchoolData = _.clone(schoolData);
      clonedSchoolData.name = school.name;
      yield request
        .post('/api/school')
        .send(clonedSchoolData)
        .query({token: token})
        .expect(409)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('CONFLICT');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('School Name is already existing.');
        });
    });

    it('should create a new school', function * () {
      yield request
        .post('/api/school')
        .send(schoolData)
        .query({token: token})
        .expect(201)
        .expect(function(res) {
          insertId = res.body.insertId;
        });
    });

    it('should be equal - saved data and passed data', function * () {
      yield request
        .get('/api/school/' + insertId)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');

          let data = res.body.data;
          expect(data.name).to.equal(schoolData.name);
          expect(data.num_students).to.equal(schoolData.num_students);
          expect(data.account_type).to.equal(schoolData.account_type);
          expect(data.verified).to.equal(schoolData.verified);
          expect(data.user_id).to.equal(schoolData.user_id);
        });
    });

    it('should create a new school - with profile', function * () {
      yield request
        .post('/api/school')
        .send(schoolProfileData)
        .query({token: token})
        .expect(201)
        .expect(function(res) {
          insertProfileId = res.body.insertId;
        });
    });

    it('should be equal - saved data and passed data with profile', function * () { // eslint-disable-line max-len
      yield request
        .get('/api/school/' + insertProfileId)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');

          let data = res.body.data;
          expect(data.name).to.equal(schoolProfileData.name);
          expect(data.num_students).to.equal(schoolProfileData.num_students);
          expect(data.account_type).to.equal(schoolProfileData.account_type);
          expect(data.verified).to.equal(schoolProfileData.verified);
          expect(data.user_id).to.equal(schoolProfileData.user_id);

          let otherDetails = _.omit(schoolProfileData, [
            'name',
            'user_id',
            'num_students',
            'account_type',
            'verified'
          ]);
          expect(data.profile).to.deep.equal(otherDetails);
        });
    });
  });

  describe('POST /api/school/:id/syllabi', function() {
    it('should fail - with no `syllabus` parameter passed', function * () {
      yield request
        .post('/api/school/' + school.id + '/syllabi')
        .send({})
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('FORBIDDEN');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Parameter `syllabus` is required.');
        });
    });

    it('should fail - when `syllabus` parameter is not an array', function * () { // eslint-disable-line max-len
      let schoolSyllabiData = {
        syllabus: 'Lorem ipsum dolor.'
      };
      yield request
        .post('/api/school/' + school.id + '/syllabi')
        .send(schoolSyllabiData)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('FORBIDDEN');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Parameter `syllabus` should be an array.');
        });
    });

    it('should fail - when `syllabus` parameter is empty', function * () {
      let schoolSyllabiData = {
        syllabus: []
      };
      yield request
        .post('/api/school/' + school.id + '/syllabi')
        .send(schoolSyllabiData)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('FORBIDDEN');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Parameter `syllabus` is empty.');
        });
    });

    it('should fail - when some syllabi ids are invalid', function * () {
      let schoolSyllabiData = {
        syllabus: [yield Util.encrypt(1)]
      };
      yield request
        .post('/api/school/' + school.id + '/syllabi')
        .send(schoolSyllabiData)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Some Syllabi Ids are not found.');
        });
    });

    it('should assign syllabus to school', function * () {
      // Create a new syllabi
      let syllabiData = {
        name: 'Syllabi IV'
      };
      let syllabiId;
      yield request
        .post('/api/syllabi')
        .send(syllabiData)
        .query({token: token})
        .expect(201)
        .expect(function(res) {
          syllabiId = res.body.insertId;
        });

      let schoolSyllabiData = {
        syllabus: [syllabiId]
      };
      yield request
        .post('/api/school/' + school.id + '/syllabi')
        .send(schoolSyllabiData)
        .query({token: token})
        .expect(201);
    });
  });

  describe('PATCH /api/school/:id', function() {
    let updateData = {
      name: 'School IV',
      num_students: 500, // eslint-disable-line camelcase
      verified: 0,
      status: 0,
      address1: 'Villa Angela Subd., Balulang, Cagayan de Oro City',
      address2: 'Corono Drive, Tibasak, Macasanding, Cagayan de Oro City'
    };

    it('should fail - with an invalid school id', function * () {
      let fakeId = 1;
      yield request
        .patch('/api/school/' + fakeId)
        .send(updateData)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('School Id is invalid.');
        });
    });

    it('should fail - with duplicate name', function * () {
      let clonedUpdateData = _.clone(updateData);
      clonedUpdateData.name = school.name;
      yield request
        .patch('/api/school/' + insertId)
        .send(clonedUpdateData)
        .query({token: token})
        .expect(409)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('CONFLICT');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('School Name is already existing.');
        });
    });

    it('should update school', function * () {
      yield request
        .patch('/api/school/' + insertId)
        .send(updateData)
        .query({token: token})
        .expect(201);
    });

    it('should be equal - saved data and passed data', function * () {
      yield request
        .get('/api/school/' + insertId)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');

          let data = res.body.data;
          expect(data.name).to.equal(updateData.name);
          expect(data.num_students).to.equal(updateData.num_students);
          expect(data.verified).to.equal(updateData.verified);
          expect(data.status).to.equal(updateData.status);

          let otherDetails = _.omit(updateData, [
            'name',
            'num_students',
            'verified',
            'status'
          ]);
          expect(data.profile).to.deep.equal(otherDetails);
        });
    });
  });
});
