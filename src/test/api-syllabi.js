/* globals app, Util */
/* eslint max-nested-callbacks: ["error", 6]*/
import {expect} from 'chai';
import _ from 'lodash';

let request;
let token;
let tokenNotAdmin;
let syllabi;
let insertId;
let syllabiData = {
  name: 'Syllabi II'
};

describe('Syllabi API', function() {
  this.timeout(20000);

  let admin = {
    username: process.env.ADMIN_USERNAME,
    password: process.env.ADMIN_PASSWORD
  };

  let student = {
    username: process.env.STUDENT_USERNAME,
    password: process.env.STUDENT_PASSWORD
  };

  before(function * () {
    yield app.started;
    request = require('supertest')(app.server);

    yield request
      .post('/api/login')
      .send(admin)
      .expect(200)
      .expect(function(res) {
        expect(res.body).to.have.property('token');
        token = res.body.token;
      });

    yield request
      .post('/api/login')
      .set({
        referer: 'http://localhost/'
      })
      .send(student)
      .expect(200)
      .expect(function(res) {
        expect(res.body).to.have.property('token');
        tokenNotAdmin = res.body.token;
      });
  });

  after(function * () {
    app.server.close();
  });

  describe('GET /api/syllabi', function() {
    it('should fail - invalid user role', function * () {
      yield request
        .get('/api/syllabi')
        .query({token: tokenNotAdmin})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('UNAUTHORIZED');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User role is invalid.');
        });
    });

    it('should get syllabus', function * () {
      yield request
        .get('/api/syllabi')
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          expect(res.body.data).to.be.not.empty; // eslint-disable-line no-unused-expressions
          syllabi = _.first(res.body.data);
        });
    });

    it('should get syllabus - with specific fields', function * () {
      let fields = [
        'name'
      ];
      yield request
        .get('/api/syllabi?fields=' + fields.join(','))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          expect(res.body.data).to.be.not.empty; // eslint-disable-line no-unused-expressions

          _.each(res.body.data, function(_user) {
            expect(_user).to.have.all.keys(fields);
          });
        });
    });

    it('should get syllabus - using the search filter', function * () {
      yield request
        .get('/api/syllabi?search=' + syllabi.name)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          expect(res.body.data).to.be.not.empty; // eslint-disable-line no-unused-expressions
        });
    });

    it('should not get any syllabus - with filters non-existent to existing syllabus', function * () { // eslint-disable-line max-len
      let filters = [
        'name=Test Syllabi'
      ];
      yield request
        .get('/api/syllabi?' + filters.join('&'))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          expect(res.body.data).to.be.empty; // eslint-disable-line no-unused-expressions
        });
    });
  });

  describe('GET /api/syllabi/:id', function() {
    it('should fail - invalid user role', function * () {
      yield request
        .get('/api/syllabi/' + syllabi.id)
        .query({token: tokenNotAdmin})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('UNAUTHORIZED');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User role is invalid.');
        });
    });

    it('should fail - with an invalid syllabi id', function * () {
      let fakeId = 1;
      yield request
        .get('/api/syllabi/' + fakeId)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('Syllabi Id is invalid.');
        });
    });

    it('should get a syllabi', function * () {
      yield request
        .get('/api/syllabi/' + syllabi.id)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');
        });
    });

    it('should get a syllabi - with specific fields', function * () {
      let fields = [
        'name'
      ];
      yield request
        .get('/api/syllabi/' + syllabi.id + '?fields=' + fields.join(','))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');
          expect(res.body.data).to.have.all.keys(fields);
        });
    });

    it('should not get a syllabi - with filters non-existent to the syllabi', function * () { // eslint-disable-line max-len
      let filters = [
        'name=Test Syllabi'
      ];
      yield request
        .get('/api/syllabi/' + syllabi.id + '?' + filters.join('&'))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.false; // eslint-disable-line no-unused-expressions
        });
    });
  });

  describe('GET /api/syllabi/:id/subject', function() {
    it('should fail - invalid user role', function * () {
      yield request
        .get('/api/syllabi/' + syllabi.id + '/subject')
        .query({token: tokenNotAdmin})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('UNAUTHORIZED');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User role is invalid.');
        });
    });

    it('should fail - with an invalid syllabi id', function * () {
      let fakeId = 1;
      yield request
        .get('/api/syllabi/' + fakeId + '/subject')
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('Syllabi Id is invalid.');
        });
    });

    it('should get assigned subjects of syllabi', function * () {
      yield request
        .get('/api/syllabi/' + syllabi.id + '/subject')
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
        });
    });

    it('should get assigned subjects of syllabi - with specific fields', function * () { // eslint-disable-line max-len
      let fields = [
        'name'
      ];
      yield request
        .get('/api/syllabi/' + syllabi.id + '/subject?fields=' + fields.join(',')) // eslint-disable-line max-len
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');

          _.each(res.body.data, function(_subject) {
            expect(_subject).to.have.all.keys(fields);
          });
        });
    });

    it('should not get assigned subjects of syllabi - with filters non-existent to the subject', function * () { // eslint-disable-line max-len
      let filters = [
        'name=Test Subject'
      ];
      yield request
        .get('/api/syllabi/' + syllabi.id + '/subject?' + filters.join('&'))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          expect(res.body.data).to.be.empty; // eslint-disable-line no-unused-expressions
        });
    });
  });

  describe('POST /api/syllabi', function() {
    it('should fail - invalid user role', function * () {
      yield request
        .post('/api/syllabi')
        .send(syllabiData)
        .query({token: tokenNotAdmin})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('UNAUTHORIZED');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User role is invalid.');
        });
    });

    it('should fail - with duplicate name', function * () {
      let clonedSyllabiData = _.clone(syllabiData);
      clonedSyllabiData.name = syllabi.name;
      yield request
        .post('/api/syllabi')
        .send(clonedSyllabiData)
        .query({token: token})
        .expect(409)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('CONFLICT');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Syllabi Name is already existing.');
        });
    });

    it('should create a new syllabi', function * () {
      yield request
        .post('/api/syllabi')
        .send(syllabiData)
        .query({token: token})
        .expect(201)
        .expect(function(res) {
          insertId = res.body.insertId;
        });
    });

    it('should be equal - saved data and passed data', function * () {
      yield request
        .get('/api/syllabi/' + insertId)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');

          let data = res.body.data;
          expect(data.name).to.equal(syllabiData.name);
        });
    });
  });

  describe('POST /api/syllabi/:id/subject', function() {
    it('should fail - invalid user role', function * () {
      yield request
        .post('/api/syllabi/' + syllabi.id + '/subject')
        .query({token: tokenNotAdmin})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('UNAUTHORIZED');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User role is invalid.');
        });
    });

    it('should fail - with no `subjects` parameter passed', function * () {
      yield request
        .post('/api/syllabi/' + syllabi.id + '/subject')
        .send({})
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('FORBIDDEN');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Parameter `subjects` is required.');
        });
    });

    it('should fail - when `subjects` parameter is not an array', function * () { // eslint-disable-line max-len
      let syllabiSubjectData = {
        subjects: 'Lorem ipsum dolor.'
      };
      yield request
        .post('/api/syllabi/' + syllabi.id + '/subject')
        .send(syllabiSubjectData)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('FORBIDDEN');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Parameter `subjects` should be an array.');
        });
    });

    it('should fail - when some subject ids are invalid', function * () {
      let syllabiSubjectData = {
        subjects: [yield Util.encrypt(1)]
      };
      yield request
        .post('/api/syllabi/' + syllabi.id + '/subject')
        .send(syllabiSubjectData)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Some Subject Ids are not found.');
        });
    });

    it('should assign subjects to syllabi', function * () {
      // Create a new subject
      let subjectData = {
        name: 'Subject V'
      };
      let subjectId;
      yield request
        .post('/api/subject')
        .send(subjectData)
        .query({token: token})
        .expect(201)
        .expect(function(res) {
          subjectId = res.body.insertId;
        });

      let syllabiSubjectData = {
        subjects: [subjectId]
      };
      yield request
        .post('/api/syllabi/' + syllabi.id + '/subject')
        .send(syllabiSubjectData)
        .query({token: token})
        .expect(201);
    });
  });

  describe('PATCH /api/syllabi/:id', function() {
    let updateData = {
      name: 'Syllabi III',
      status: 0
    };

    it('should fail - invalid user role', function * () {
      yield request
        .patch('/api/syllabi/' + insertId)
        .send(updateData)
        .query({token: tokenNotAdmin})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('UNAUTHORIZED');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User role is invalid.');
        });
    });

    it('should fail - with an invalid syllabi id', function * () {
      let fakeId = 1;
      yield request
        .patch('/api/syllabi/' + fakeId)
        .send(updateData)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('Syllabi Id is invalid.');
        });
    });

    it('should fail - with duplicate name', function * () {
      let clonedUpdateData = _.clone(updateData);
      clonedUpdateData.name = syllabi.name;
      yield request
        .patch('/api/syllabi/' + insertId)
        .send(clonedUpdateData)
        .query({token: token})
        .expect(409)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('CONFLICT');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Syllabi Name is already existing.');
        });
    });

    it('should update syllabi', function * () {
      yield request
        .patch('/api/syllabi/' + insertId)
        .send(updateData)
        .query({token: token})
        .expect(201);
    });

    it('should be equal - saved data and passed data', function * () {
      let data;
      yield request
        .get('/api/syllabi/' + insertId)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');

          data = res.body.data;
          expect(data.name).to.equal(updateData.name);
          expect(data.status).to.equal(updateData.status);
        });
    });
  });
});
