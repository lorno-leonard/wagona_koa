/* globals app */
/* eslint max-nested-callbacks: ["error", 6]*/

let request;

describe('Page API', function() {
  this.timeout(20000);

  before(function * () {
    yield app.started;
    request = require('supertest')(app.server);
  });

  after(function * () {
    app.server.close();
  });

  describe('GET /', function() {
    it('should return 200', function * () {
      yield request
        .get('/')
        .expect(200);
    });
  });

  describe('GET /admin', function() {
    it('should return 200', function * () {
      yield request
        .get('/admin')
        .expect(200);
    });
  });
});
