/* globals app, Util */
/* eslint max-nested-callbacks: ["error", 6]*/
import {expect} from 'chai';
import _ from 'lodash';

let request;
let token;
let tokenNotAdmin;
let topic;
let insertId;
let topicData = {
  name: 'Topic II',
  is_paid: 1 // eslint-disable-line camelcase
};

describe('Topic API', function() {
  this.timeout(20000);

  let admin = {
    username: process.env.ADMIN_USERNAME,
    password: process.env.ADMIN_PASSWORD
  };

  let student = {
    username: process.env.STUDENT_USERNAME,
    password: process.env.STUDENT_PASSWORD
  };

  before(function * () {
    yield app.started;
    request = require('supertest')(app.server);

    yield request
      .post('/api/login')
      .send(admin)
      .expect(200)
      .expect(function(res) {
        expect(res.body).to.have.property('token');
        token = res.body.token;
      });

    yield request
      .post('/api/login')
      .set({
        referer: 'http://localhost/'
      })
      .send(student)
      .expect(200)
      .expect(function(res) {
        expect(res.body).to.have.property('token');
        tokenNotAdmin = res.body.token;
      });
  });

  after(function * () {
    app.server.close();
  });

  describe('GET /api/topic', function() {
    it('should fail - invalid user role', function * () {
      yield request
        .get('/api/topic')
        .query({token: tokenNotAdmin})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('UNAUTHORIZED');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User role is invalid.');
        });
    });

    it('should get topics', function * () {
      yield request
        .get('/api/topic')
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          expect(res.body.data).to.be.not.empty; // eslint-disable-line no-unused-expressions
          topic = _.first(res.body.data);
        });
    });

    it('should get topics - with specific fields', function * () {
      let fields = [
        'name',
        'is_paid'
      ];
      yield request
        .get('/api/topic?fields=' + fields.join(','))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          expect(res.body.data).to.be.not.empty; // eslint-disable-line no-unused-expressions

          _.each(res.body.data, function(_user) {
            expect(_user).to.have.all.keys(fields);
          });
        });
    });

    it('should get topics - using the search filter', function * () {
      yield request
        .get('/api/topic?search=' + topic.name)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          expect(res.body.data).to.be.not.empty; // eslint-disable-line no-unused-expressions
        });
    });

    it('should not get any topics - with filters non-existent to existing topics', function * () { // eslint-disable-line max-len
      let filters = [
        'name=Test Topic',
        'is_paid=0'
      ];
      yield request
        .get('/api/topic?' + filters.join('&'))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          expect(res.body.data).to.be.empty; // eslint-disable-line no-unused-expressions
        });
    });
  });

  describe('GET /api/topic/:id', function() {
    it('should fail - invalid user role', function * () {
      yield request
        .get('/api/topic/' + topic.id)
        .query({token: tokenNotAdmin})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('UNAUTHORIZED');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User role is invalid.');
        });
    });

    it('should fail - with an invalid topic id', function * () {
      let fakeId = 1;
      yield request
        .get('/api/topic/' + fakeId)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('Topic Id is invalid.');
        });
    });

    it('should get a topic', function * () {
      yield request
        .get('/api/topic/' + topic.id)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');
        });
    });

    it('should get a topic - with specific fields', function * () {
      let fields = [
        'name',
        'is_paid'
      ];
      yield request
        .get('/api/topic/' + topic.id + '?fields=' + fields.join(','))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');
          expect(res.body.data).to.have.all.keys(fields);
        });
    });

    it('should not get a topic - with filters non-existent to the topic', function * () { // eslint-disable-line max-len
      let filters = [
        'name=Test Topic',
        'is_paid=0'
      ];
      yield request
        .get('/api/topic/' + topic.id + '?' + filters.join('&'))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.false; // eslint-disable-line no-unused-expressions
        });
    });
  });

  describe('GET /api/topic/:id/question', function() {
    it('should fail - invalid user role', function * () {
      yield request
        .get('/api/topic/' + topic.id + '/question')
        .query({token: tokenNotAdmin})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('UNAUTHORIZED');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User role is invalid.');
        });
    });

    it('should fail - with an invalid topic id', function * () {
      let fakeId = 1;
      yield request
        .get('/api/topic/' + fakeId + '/question')
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('Topic Id is invalid.');
        });
    });

    it('should get assigned questions of topic', function * () {
      yield request
        .get('/api/topic/' + topic.id + '/question')
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
        });
    });

    it('should get assigned questions of topic - with specific fields', function * () { // eslint-disable-line max-len
      let fields = [
        'description',
        'type',
        'correct',
        'is_paid',
        'tags',
        'status'
      ];
      yield request
        .get('/api/topic/' + topic.id + '/question?fields=' + fields.join(','))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');

          _.each(res.body.data, function(_topic) {
            expect(_topic).to.have.all.keys(fields);
          });
        });
    });

    it('should not get assigned questions of topic - with filters non-existent to the question', function * () { // eslint-disable-line max-len
      let filters = [
        'type=NOT_QUESTION_TYPE'
      ];
      yield request
        .get('/api/topic/' + topic.id + '/question?' + filters.join('&'))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          expect(res.body.data).to.be.empty; // eslint-disable-line no-unused-expressions
        });
    });
  });

  describe('POST /api/topic', function() {
    it('should fail - invalid user role', function * () {
      yield request
        .post('/api/topic')
        .send(topicData)
        .query({token: tokenNotAdmin})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('UNAUTHORIZED');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User role is invalid.');
        });
    });

    it('should fail - with duplicate name', function * () {
      let clonedTopicData = _.clone(topicData);
      clonedTopicData.name = topic.name;
      yield request
        .post('/api/topic')
        .send(clonedTopicData)
        .query({token: token})
        .expect(409)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('CONFLICT');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Topic Name is already existing.');
        });
    });

    it('should create a new topic', function * () {
      yield request
        .post('/api/topic')
        .send(topicData)
        .query({token: token})
        .expect(201)
        .expect(function(res) {
          insertId = res.body.insertId;
        });
    });

    it('should be equal - saved data and passed data', function * () {
      yield request
        .get('/api/topic/' + insertId)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');

          let data = res.body.data;
          expect(data.name).to.equal(topicData.name);
          expect(data.is_paid).to.equal(topicData.is_paid);
        });
    });
  });

  describe('POST /api/topic/:id/question', function() {
    it('should fail - invalid user role', function * () {
      yield request
        .post('/api/topic/' + topic.id + '/question')
        .query({token: tokenNotAdmin})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('UNAUTHORIZED');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User role is invalid.');
        });
    });

    it('should fail - with no `questions` parameter passed', function * () {
      yield request
        .post('/api/topic/' + topic.id + '/question')
        .send({})
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('FORBIDDEN');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Parameter `questions` is required.');
        });
    });

    it('should fail - when `questions` parameter is not an array', function * () { // eslint-disable-line max-len
      let topicQuestionData = {
        questions: 'Lorem ipsum dolor.'
      };
      yield request
        .post('/api/topic/' + topic.id + '/question')
        .send(topicQuestionData)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('FORBIDDEN');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Parameter `questions` should be an array.');
        });
    });

    it('should fail - when some topic ids are invalid', function * () {
      let topicQuestionData = {
        questions: [yield Util.encrypt(1)]
      };
      yield request
        .post('/api/topic/' + topic.id + '/question')
        .send(topicQuestionData)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Some Question Ids are not found.');
        });
    });

    it('should assign questions to topic', function * () {
      // Create a new question
      let questionData = {
        description: '10 + 6 = ?',
        explanation: '10 + 6 = 16',
        type: 'FILL_IN',
        is_paid: 0, // eslint-disable-line camelcase
        tags: ['addition', 'intermediate'],
        correct: 0,
        choices: [
          '',
          '16'
        ]
      };
      let questionId;
      yield request
        .post('/api/question')
        .send(questionData)
        .query({token: token})
        .expect(201)
        .expect(function(res) {
          questionId = res.body.insertId;
        });

      let topicQuestionData = {
        questions: [questionId]
      };
      yield request
        .post('/api/topic/' + topic.id + '/question')
        .send(topicQuestionData)
        .query({token: token})
        .expect(201);
    });
  });

  describe('PATCH /api/topic/:id', function() {
    let updateData = {
      name: 'Topic III',
      is_paid: 0, // eslint-disable-line camelcase
      status: 0
    };

    it('should fail - invalid user role', function * () {
      yield request
        .patch('/api/topic/' + insertId)
        .send(updateData)
        .query({token: tokenNotAdmin})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('UNAUTHORIZED');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User role is invalid.');
        });
    });

    it('should fail - with an invalid topic id', function * () {
      let fakeId = 1;
      yield request
        .patch('/api/topic/' + fakeId)
        .send(updateData)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('Topic Id is invalid.');
        });
    });

    it('should fail - with duplicate name', function * () {
      let clonedUpdateData = _.clone(updateData);
      clonedUpdateData.name = topic.name;
      yield request
        .patch('/api/topic/' + insertId)
        .send(clonedUpdateData)
        .query({token: token})
        .expect(409)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('CONFLICT');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Topic Name is already existing.');
        });
    });

    it('should update topic', function * () {
      yield request
        .patch('/api/topic/' + insertId)
        .send(updateData)
        .query({token: token})
        .expect(201);
    });

    it('should be equal - saved data and passed data', function * () {
      let data;
      yield request
        .get('/api/topic/' + insertId)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');

          data = res.body.data;
          expect(data.name).to.equal(updateData.name);
          expect(data.is_paid).to.equal(updateData.is_paid);
          expect(data.status).to.equal(updateData.status);
        });
    });
  });
});
