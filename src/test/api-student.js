/* globals app, Util */
/* eslint max-nested-callbacks: ["error", 6]*/
import {expect} from 'chai';
import _ from 'lodash';

let request;
let token;
let student;
let insertId;
let studentData = {
  account_type: 'PAID', // eslint-disable-line camelcase
  verified: 1,
  address1: 'Villa Angela Subd., Balulang, Cagayan de Oro City',
  address2: 'Corono Drive, Tibasak, Macasanding, Cagayan de Oro City',
  address3: 'Purok 1, Tawantawan, Initao',
  address4: 'Bliss, San Pedro, Initao',
  parent_emai: 'parent2@wagona.com', // eslint-disable-line camelcase
  parent_phone_num: '+63987321897' // eslint-disable-line camelcase
};

describe('Student API', function() {
  this.timeout(20000);

  let admin = {
    username: process.env.ADMIN_USERNAME,
    password: process.env.ADMIN_PASSWORD
  };

  before(function * () {
    yield app.started;
    request = require('supertest')(app.server);

    yield request
      .post('/api/login')
      .send(admin)
      .expect(200)
      .expect(function(res) {
        expect(res.body).to.have.property('token');
        token = res.body.token;
      });
  });

  after(function * () {
    app.server.close();
  });

  describe('GET /api/student', function() {
    it('should get students', function * () {
      yield request
        .get('/api/student')
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          student = _.first(res.body.data);
        });
    });

    it('should get students - with specific fields', function * () {
      let fields = [
        'user_id',
        'school_id',
        'syllabi_id',
        'class_id',
        'account_type',
        'status'
      ];
      yield request
        .get('/api/student?fields=' + fields.join(','))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');

          _.each(res.body.data, function(obj) {
            expect(obj).to.have.all.keys(fields);
          });
        });
    });

    it('should not get any students - with filters non-existent to existing students', function * () { // eslint-disable-line max-len
      let filters = [
        'status=2'
      ];
      yield request
        .get('/api/student?' + filters.join('&'))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          expect(res.body.data).to.be.empty; // eslint-disable-line no-unused-expressions
        });
    });
  });

  describe('GET /api/student/:id', function() {
    it('should fail - with an invalid student id', function * () {
      let fakeId = 1;
      yield request
        .get('/api/student/' + fakeId)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('Student Id is invalid.');
        });
    });

    it('should get a student', function * () {
      yield request
        .get('/api/student/' + student.id)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');
        });
    });

    it('should get a student - with specific fields', function * () {
      let fields = [
        'user_id',
        'school_id',
        'syllabi_id',
        'class_id',
        'account_type',
        'status'
      ];
      yield request
        .get('/api/student/' + student.id + '?fields=' + fields.join(','))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');
          expect(res.body.data).to.have.all.keys(fields);
        });
    });

    it('should not get a student - with filters non-existent to the student', function * () { // eslint-disable-line max-len
      let filters = [
        'status=2'
      ];
      yield request
        .get('/api/student/' + student.id + '?' + filters.join('&'))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.false; // eslint-disable-line no-unused-expressions
        });
    });
  });

  describe('POST /api/student', function() {
    it('should fail - with invalid user_id', function * () {
      studentData.user_id = yield Util.encrypt(1006); // eslint-disable-line camelcase
      studentData.school_id = yield Util.encrypt(1000); // eslint-disable-line camelcase
      studentData.syllabi_id = yield Util.encrypt(1000); // eslint-disable-line camelcase
      studentData.class_id = yield Util.encrypt(1000); // eslint-disable-line camelcase
      let clonedStudentData = _.clone(studentData);
      clonedStudentData.user_id = 1; // eslint-disable-line camelcase
      yield request
        .post('/api/student')
        .send(clonedStudentData)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('User Id is invalid.');
        });
    });

    it('should fail - with invalid school_id', function * () {
      let clonedStudentData = _.clone(studentData);
      clonedStudentData.school_id = 1; // eslint-disable-line camelcase
      yield request
        .post('/api/student')
        .send(clonedStudentData)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('School Id is invalid.');
        });
    });

    it('should fail - with invalid syllabi_id', function * () {
      let clonedStudentData = _.clone(studentData);
      clonedStudentData.syllabi_id = 1; // eslint-disable-line camelcase
      yield request
        .post('/api/student')
        .send(clonedStudentData)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Syllabi Id is invalid.');
        });
    });

    it('should fail - with invalid class_id', function * () {
      let clonedStudentData = _.clone(studentData);
      clonedStudentData.class_id = 1; // eslint-disable-line camelcase
      yield request
        .post('/api/student')
        .send(clonedStudentData)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Class Id is invalid.');
        });
    });

    it('should create a new student', function * () {
      yield request
        .post('/api/student')
        .send(studentData)
        .query({token: token})
        .expect(201)
        .expect(function(res) {
          insertId = res.body.insertId;
        });
    });

    it('should be equal - saved data and passed data', function * () {
      yield request
        .get('/api/student/' + insertId)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');

          let data = res.body.data;
          expect(data.user_id).to.equal(studentData.user_id);
          expect(data.school_id).to.equal(studentData.school_id);
          expect(data.syllabi_id).to.equal(studentData.syllabi_id);
          expect(data.class_id).to.equal(studentData.class_id);
          expect(data.account_type).to.equal(studentData.account_type);
          expect(data.verified).to.equal(studentData.verified);

          let otherDetails = _.omit(studentData, [
            'user_id',
            'school_id',
            'syllabi_id',
            'class_id',
            'account_type',
            'verified'
          ]);
          if (!_.isEmpty(otherDetails)) {
            expect(data.profile).to.deep.equal(otherDetails);
          }
        });
    });
  });

  describe('PATCH /api/student/:id', function() {
    let updateData = {
      verified: 0,
      status: 0,
      address1: 'Villa Angela Subd., Balulang, Cagayan de Oro City',
      address2: 'Corono Drive, Tibasak, Macasanding, Cagayan de Oro City'
    };

    it('should fail - with an invalid student id', function * () {
      let fakeId = 1;
      yield request
        .patch('/api/student/' + fakeId)
        .send(updateData)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('Student Id is invalid.');
        });
    });

    it('should update student', function * () {
      yield request
        .patch('/api/student/' + insertId)
        .send(updateData)
        .query({token: token})
        .expect(201);
    });

    it('should be equal - saved data and passed data', function * () {
      yield request
        .get('/api/student/' + insertId)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');

          let data = res.body.data;
          expect(data.verified).to.equal(updateData.verified);
          expect(data.status).to.equal(updateData.status);

          let otherDetails = _.omit(updateData, [
            'verified',
            'status'
          ]);
          if (!_.isEmpty(otherDetails)) {
            expect(data.profile).to.deep.equal(otherDetails);
          }
        });
    });
  });
});
