/* globals app, Util */
/* eslint max-nested-callbacks: ["error", 6]*/
import {expect} from 'chai';
import _ from 'lodash';
import Role from '../lib/role';

let request;
let token;
let tokenNotAdmin;
let user;
let insertId;
let userData = {
  username: 'lorno',
  password: '123',
  email: 'lorno@sama.com',
  role: Role.SCHOOL,
  first_name: 'Lorno', // eslint-disable-line camelcase
  last_name: 'Sama', // eslint-disable-line camelcase
  country_id: 'PH' // eslint-disable-line camelcase
};

describe('Users API', function() {
  this.timeout(20000);

  let admin = {
    username: process.env.ADMIN_USERNAME,
    password: process.env.ADMIN_PASSWORD
  };

  let student = {
    username: process.env.STUDENT_USERNAME,
    password: process.env.STUDENT_PASSWORD
  };

  before(function * () {
    yield app.started;
    request = require('supertest')(app.server);

    yield request
      .post('/api/login')
      .send(admin)
      .expect(200)
      .expect(function(res) {
        expect(res.body).to.have.property('token');
        token = res.body.token;
      });

    yield request
      .post('/api/login')
      .set({
        referer: 'http://localhost/'
      })
      .send(student)
      .expect(200)
      .expect(function(res) {
        expect(res.body).to.have.property('token');
        tokenNotAdmin = res.body.token;
      });
  });

  after(function * () {
    app.server.close();
  });

  describe('GET /api/users', function() {
    it('should fail - invalid user role', function * () {
      yield request
        .get('/api/users')
        .query({token: tokenNotAdmin})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('UNAUTHORIZED');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User role is invalid.');
        });
    });

    it('should get users', function * () {
      yield request
        .get('/api/users')
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          expect(res.body.data).to.be.not.empty; // eslint-disable-line no-unused-expressions
          user = _.first(res.body.data);
        });
    });

    it('should get users - with specific fields', function * () {
      let fields = [
        'id',
        'first_name',
        'last_name'
      ];
      yield request
        .get('/api/users?fields=' + fields.join(','))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          expect(res.body.data).to.be.not.empty; // eslint-disable-line no-unused-expressions

          _.each(res.body.data, function(_user) {
            expect(_user).to.have.all.keys(fields);
          });
        });
    });

    it('should get users - using the search filter', function * () {
      yield request
        .get('/api/users?search=' + user.username)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          expect(res.body.data).to.be.not.empty; // eslint-disable-line no-unused-expressions
        });
    });

    it('should not get any users - with filters non-existent to existing users', function * () { // eslint-disable-line max-len
      let filters = [
        'role=TEST_ROLE',
        'username=test_username'
      ];
      yield request
        .get('/api/users?' + filters.join('&'))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('array');
          expect(res.body.data).to.be.empty; // eslint-disable-line no-unused-expressions
        });
    });
  });

  describe('GET /api/users/:id', function() {
    it('should fail - invalid user role', function * () {
      yield request
        .get('/api/users/' + user.id)
        .query({token: tokenNotAdmin})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('UNAUTHORIZED');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User role is invalid.');
        });
    });

    it('should fail - with an invalid user id', function * () {
      let fakeId = 1;
      yield request
        .get('/api/users/' + fakeId)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User Id is invalid.');
        });
    });

    it('should get a user', function * () {
      yield request
        .get('/api/users/' + user.id)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');
        });
    });

    it('should get a user - with specific fields', function * () {
      let fields = [
        'id',
        'first_name',
        'last_name'
      ];
      yield request
        .get('/api/users/' + user.id + '?fields=' + fields.join(','))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');
          expect(res.body.data).to.have.all.keys(fields);
        });
    });

    it('should not get a user - with filters non-existent to the user', function * () { // eslint-disable-line max-len
      let filters = [
        'role=TEST_ROLE',
        'username=test_username'
      ];
      yield request
        .get('/api/users/' + user.id + '?' + filters.join('&'))
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.false; // eslint-disable-line no-unused-expressions
        });
    });
  });

  describe('POST /api/users', function() {
    it('should fail - invalid user role', function * () {
      yield request
        .post('/api/users')
        .send(userData)
        .query({token: tokenNotAdmin})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('UNAUTHORIZED');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User role is invalid.');
        });
    });

    it('should fail - with duplicate username', function * () {
      let clonedUserData = _.clone(userData);
      clonedUserData.username = user.username;
      yield request
        .post('/api/users')
        .send(clonedUserData)
        .query({token: token})
        .expect(409)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('CONFLICT');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('Username is already existing.');
        });
    });

    it('should fail - with duplicate email', function * () {
      let clonedUserData = _.clone(userData);
      clonedUserData.email = user.email;
      yield request
        .post('/api/users')
        .send(clonedUserData)
        .query({token: token})
        .expect(409)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('CONFLICT');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Email Address is already existing.');
        });
    });

    it('should create a new user', function * () {
      yield request
        .post('/api/users')
        .send(userData)
        .query({token: token})
        .expect(201)
        .expect(function(res) {
          insertId = res.body.insertId;
        });
    });

    it('should be equal - saved data and passed data', function * () {
      let data;
      yield request
        .get('/api/users/' + insertId)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');

          data = res.body.data;
          expect(data.username).to.equal(userData.username);
          expect(data.email).to.equal(userData.email);
          expect(data.first_name).to.equal(userData.first_name);
          expect(data.last_name).to.equal(userData.last_name);
          expect(data.country_id).to.equal(userData.country_id);
        });
      expect(data.role).to.equal(yield Util.findKey(userData.role, Role));
    });
  });

  describe('PATCH /api/users/:id', function() {
    let updateData = {
      username: 'loso',
      password: '456',
      email: 'loso@chan.com',
      first_name: 'Loso', // eslint-disable-line camelcase
      last_name: 'Chan', // eslint-disable-line camelcase
      status: 0
    };

    it('should fail - invalid user role', function * () {
      yield request
        .patch('/api/users/' + insertId)
        .send(updateData)
        .query({token: tokenNotAdmin})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('UNAUTHORIZED');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User role is invalid.');
        });
    });

    it('should fail - with an invalid user id', function * () {
      let fakeId = 1;
      yield request
        .patch('/api/users/' + fakeId)
        .send(updateData)
        .query({token: token})
        .expect(403)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('INVALID_ID');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('User Id is invalid.');
        });
    });

    it('should fail - with duplicate username', function * () {
      let clonedUpdateData = _.clone(updateData);
      clonedUpdateData.username = user.username;
      yield request
        .patch('/api/users/' + insertId)
        .send(clonedUpdateData)
        .query({token: token})
        .expect(409)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('CONFLICT');

          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('Username is already existing.');
        });
    });

    it('should fail - with duplicate email', function * () {
      let clonedUpdateData = _.clone(updateData);
      clonedUpdateData.email = user.email;
      yield request
        .patch('/api/users/' + insertId)
        .send(clonedUpdateData)
        .query({token: token})
        .expect(409)
        .expect(function(res) {
          expect(res.body).to.be.an('object');

          expect(res.body).to.have.property('code');
          expect(res.body.code).to.equal('CONFLICT');

          expect(res.body).to.have.property('message');
          expect(res.body.message)
            .to.equal('Email Address is already existing.');
        });
    });

    it('should update user', function * () {
      yield request
        .patch('/api/users/' + insertId)
        .send(updateData)
        .query({token: token})
        .expect(201);
    });

    it('should be equal - saved data and passed data', function * () {
      let data;
      yield request
        .get('/api/users/' + insertId)
        .query({token: token})
        .expect(200)
        .expect(function(res) {
          expect(res.body.data).to.be.an('object');

          data = res.body.data;
          expect(data.username).to.equal(updateData.username);
          expect(data.email).to.equal(updateData.email);
          expect(data.first_name).to.equal(updateData.first_name);
          expect(data.last_name).to.equal(updateData.last_name);
          expect(data.status).to.equal(updateData.status);
        });
    });

    it('should be able to login - based on updated data', function * () {
      yield request
        .post('/api/login')
        .set({
          referer: 'http://localhost/'
        })
        .send(_.pick(updateData, ['username', 'password']))
        .expect(200)
        .expect(function(res) {
          expect(res.body).to.be.an('object');
          expect(res.body).to.have.property('token');
        });
    });
  });
});
