import mysql from 'mysql';
import Pool from 'mysql/lib/Pool';
import Connection from 'mysql/lib/Connection';
import Promise from 'bluebird';
import config from '../config/mysql';
import debug from 'debug';

Promise.promisifyAll([Pool, Connection]);
let pool = mysql.createPool(config);
let logger = debug('mysql');

export default {
  borrow: function * () {
    return yield pool.getConnectionAsync();
  },
  query: function * (sql, attr = []) {
    let conn = yield this.borrow();
    let rows = [];

    try {
      rows = conn.queryAsync(sql, attr);
      conn.release();
      return rows;
    } catch (e) {
      conn.release();
      logger('error', e);
      return Promise.reject(e);
    }
  },
  find: function * (sql, attr) {
    return yield this.query(sql, attr);
  },
  findOne: function * (sql, attr) {
    let result = yield this.query(sql, attr);
    return result[0];
  }
};
