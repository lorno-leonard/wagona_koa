/* globals Util, UserModel */
import jwt from 'jwt-simple';
import moment from 'moment';

export default {
  create: function * (payload) {
    return jwt.encode(payload, process.env.TOKEN_SECRET);
  },
  decode: function * (token) {
    return jwt.decode(token, process.env.TOKEN_SECRET);
  },
  isValid: function * (token) {
    let payload = jwt.decode(token, process.env.TOKEN_SECRET);
    let requiredKeys = [
      'id',
      'username',
      'role',
      'expire'
    ];
    if (!Util.hasEvery(requiredKeys, payload)) {
      return false;
    }

    // Check if user is existing
    let user = yield UserModel.findOne(payload.id);
    if (!user) {
      return false;
    }

    // Check if token is expired
    let now = moment().unix();
    if (payload.expire < now) {
      return false;
    }

    return true;
  }
};
