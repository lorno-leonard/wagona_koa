export default {
  ADMIN: 0,
  DIRECTOR: 1,
  SCHOOL: 2,
  STUDENT: 3,
  INDIVIDUAL: 4,
  TEACHER: 5
};
