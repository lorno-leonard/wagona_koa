import path from 'path';
import _ from 'lodash';
import bcrypt from 'bcryptjs';
import hashId from 'hashids';
import cryptoAES from 'crypto-js/aes';
import cryptoEncUTF8 from 'crypto-js/enc-utf8';

let salt = process.env.ID_SECRET;

/**
 * Delay for the specified milliseconds
 * @param {number} time
 */
export function delay(time) {
  return new Promise(function(resolve) {
    setTimeout(resolve, time);
  });
}

/**
 * Require all files inside a directory
 * @param {string} dir
 * @param {string} namespace
 */
export function dynamicRequire(dir, namespace) {
  let normalizedPath = path.join(__dirname, dir);
  let files = require('fs').readdirSync(normalizedPath);
  _.each(files, file => {
    let mod = require(path.resolve(__dirname, dir, file));
    if (namespace) {
      global[namespace] = _.merge(global[namespace] || {}, mod);
    } else {
      _.merge(global, mod);
    }
  });
}

/**
 * Calculate bcrypt hash
 * @param {string} message
 */
export function bcryptHash(message) {
  return new Promise(function(resolve, reject) {
    bcrypt.hash(message, 10, function(err, result) {
      if (err) return reject(err);
      resolve(result);
    });
  });
}

/**
 * Compare bcrypt hash
 * @param {string} message
 * @param {string} hash
 */
export function bcryptCompare(message, hash) {
  return new Promise(function(resolve, reject) {
    bcrypt.compare(message, hash, function(err, result) {
      if (err) return reject(err);
      resolve(result);
    });
  });
}

/**
 * Encrypt string
 * @param {number} id
 * @param {number} length
 */
export function encrypt(id, length = 10) {
  let hasher = new hashId(salt, length);
  return new Promise(function(resolve) {
    resolve(hasher.encode(id));
  });
}

/**
 * Decrypt string
 * @param {number} id
 * @param {number} length
 */
export function decrypt(id, length = 10) {
  let hasher = new hashId(salt, length);
  return new Promise(function(resolve) {
    resolve(_.first(hasher.decode(id)));
  });
}

/**
 * Encrypt string - AES
 * @param {number} message
 */
export function encryptAES(message) {
  return new Promise(function(resolve) {
    let encrypt = cryptoAES.encrypt(message, salt);
    let hash = encrypt.toString().replace(/\+/g, '-').replace(/\//g, '_');
    resolve(hash);
  });
}

/**
 * Decrypt string - AES
 * @param {number} message
 */
export function decryptAES(message) {
  return new Promise(function(resolve) {
    let hash = message.replace(/-/g, '+').replace(/_/g, '/');
    let decrypt = cryptoAES.decrypt(hash, salt);
    resolve(decrypt.toString(cryptoEncUTF8));
  });
}

/**
 * Check array of keys present in object
 * @param {array} keys
 * @param {object} obj
 */
export function hasEvery(keys, obj) {
  return _.every(keys, _.partial(_.has, obj));
}

/**
 * Find key in object using value
 * @param {string} value
 * @param {object} obj
 */
export function findKey(value, obj) {
  return new Promise(function(resolve) {
    resolve(
      _.findKey(obj, function(val) {
        return _.isEqual(val, Number(value));
      })
    );
  });
}
