/* globals AppError, UserModel */
import debug from 'debug';
import _ from 'lodash';
import Token from './lib/token';

let logger = debug('http');

let statusCodes = {
  UNAUTHORIZED: 403,
  FORBIDDEN: 403,
  INVALID_USERNAME: 403,
  INVALID_PASSWORD: 403,
  INVALID_ID: 403,
  INVALID_TEST: 403,
  NOT_FOUND: 404,
  CONFLICT: 409
};

export default [
  function * httpLogger(next) {
    let request = {
      method: this.request.method,
      url: this.request.originalUrl,
      headers: this.request.headers
    };
    if (this.request.body) {
      request.body = this.request.body;
    }
    logger('request', request);
    yield next;
    let response = {
      method: this.request.method,
      url: this.request.originalUrl,
      headers: this.headers,
      status: this.status
    };
    if (this.body) {
      response.body = this.body;
    }
    logger('response', response);
  },
  function * handleErrors(next) {
    try {
      yield next;
    } catch (err) {
      if (err instanceof AppError) {
        this.body = err.toObject();
        this.status = statusCodes[err.code] || 400;
        if (err.code === 'UNAUTHORIZED') {
          this.set('WWW-Authenticate', 'Basic realm="Login"');
        }
      } else if (err.name === 'ValidationError') {
        this.status = 400;
        this.body = {
          code: 'INVALID_REQUEST',
          message: err.message,
          errors: err.errors
        };
      } else {
        console.log(err);
        this.body = {
          code: 'INTERNAL_SERVER_ERROR',
          message: err.message
        };
        this.status = 500;
      }
    }
  },
  function * setBaseUrl(next) {
    this.baseUrl = process.env.BASE_URL;
    yield next;
  },
  function * setUser(next) {
    let token = this.cookies.get('token');
    let isValidToken = _.isUndefined(token) ? false : yield Token.isValid(token);
    if (isValidToken) {
      let payload = yield Token.decode(token);
      let user = yield UserModel.findOne(payload.id);
      global.User = user;
    } else {
      global.User = undefined;
    }
    yield next;
  }
];
