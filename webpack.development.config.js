var path = require('path');
var webpack = require('webpack');
var autoprefixer = require('autoprefixer');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: {
    admin: './app/admin/index.jsx',
    main: './app/main/index.jsx',
    vendor: [
      'react',
      'jquery',
      'jquery-slimscroll',
      'parsleyjs',
      'bootstrap',
      'metismenu',
      'pace-progress',
      'bootstrap-tagsinput',
      'slick-carousel',
      'lodash'
    ]
  },
  output: {
    path: path.join(__dirname, '/public'),
    publicPath: '/',
    filename: '[name].bundle.js'
  },
  module: {
    loaders: [{
      test: /(\.js|\.jsx)$/,
      exclude: /node_modules/,
      loaders: ['react-hot', 'babel']
    }, {
      test: /(\.scss|\.css)$/,
      loader: 'style!css!postcss!sass'
      // loader: 'style!css?sourceMap&modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!postcss!sass?sourceMap'
      // loaders: [
      //   'style',
      //   'css',
      //   'postcss',
      //   'sass'
      // ]
    }, {
      test: /\.(png|jpg|gif)(\?v=\d+\.\d+\.\d+)?$/,
      loader: 'url?limit=100000'
    }, {
      test: /\.(eot|com|json|ttf|woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
      loader: 'url?limit=10000&mimetype=application/octet-stream'
    }, {
      test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
      loader: 'url?limit=10000&mimetype=image/svg+xml'
    }]
  },
  resolve: {
    extensions: ['', '.scss', '.css', '.js', '.jsx', '.json'],
    modulesDirectories: [
      'node_modules',
      path.resolve(__dirname, './node_modules')
    ]
  },
  postcss: [autoprefixer],
  devServer: {
    port: 3001,
    contentBase: './public',
    historyApiFallback: true,
    hot: true,
    inline: true,
    progress: true,
    proxy: {
      '/': {
        target: process.env.BASE_URL
      }
    }
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('development'),
      'process.env.WIRIS_PLUGIN_URL': JSON.stringify(process.env.WIRIS_PLUGIN_URL),
      'process.env.JBIMAGES_PLUGIN_URL': JSON.stringify(process.env.JBIMAGES_PLUGIN_URL),
      'process.env.BASE_URL': JSON.stringify(process.env.BASE_URL),
      'process.env.BASE_URL_DEV': JSON.stringify(process.env.BASE_URL_DEV)
    }),
    new ExtractTextPlugin('[name].bundle.css', {allChunks: true}),
    new HtmlWebpackPlugin({
      template: './views/template-main.html',
      title: 'Wagona Maths',
      filename: 'main.html',
      chunks: ['main'],
      inject: true
    }),
    new HtmlWebpackPlugin({
      template: './views/template-admin.html',
      title: 'Admin - Wagona Maths',
      filename: 'admin.html',
      chunks: ['admin'],
      inject: true
    }),
    new webpack.ProvidePlugin({
      '$': "jquery",
      'jQuery': "jquery",
      'window.jQuery': "jquery",
      'window.$': 'jquery'
    }),
    new webpack.optimize.CommonsChunkPlugin('vendor', 'vendor.bundle.js', Infinity),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()
  ]
};
