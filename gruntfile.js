module.exports = function(grunt) {
  grunt.loadNpmTasks('grunt-shell');

  grunt.initConfig({
    shell: {
      mysql: {
        command: grunt.file
          .expand([
            'sql/schema.sql',
            'sql/seed/' + process.env.APP_MODE + '.seed.sql'
          ])
          .map(
            item => `mysql -u $MYSQL_USER -h $MYSQL_HOST --password=$MYSQL_PASSWORD $MYSQL_DATABASE < ${item}` // eslint-disable-line max-len
          )
          .join(' && ')
      }
    }
  });

  grunt.registerTask('mysql', ['shell:mysql']);
};
